// Interactiveness now

(function() {
    $(':not([title=""])').tooltip();
	$.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).on('click','.delete',function(event) {
        event.preventDefault();
        var $self = $(this);
        alertify.confirm('Are you sure to delete this record?').set('onok', function(closeEvent){
            if($self.attr('href') != '#')
            {
                $.ajax({
                    url: $self.data('href'),
                    type: 'DELETE',
                    success: function(result){
                        $self.closest('tr').remove();
                        // location.reload();
                    }
                });

            }
        } );
    });
    
    // autosize($('textarea').attr('rows',2));
    $('.select2').select2();
    $('.editor').trumbowyg();
    $('.datetimepicker').datetimepicker({
        format:'d-m-Y H:i:s'
    });
    $('.datepicker').datetimepicker({
        timepicker:false,
        format:'d-m-Y'
    });
    $('.timepicker').datetimepicker({
        datepicker:false,
        format:'H:i',
        'step':30
    });
}());

function previewImg(input,previewBox) {

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#'+previewBox).attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }

}