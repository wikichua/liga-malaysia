<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \CMS\User::create(
    			array(
    					'name' => 'Convep Admin',
						'email' => 'admin@convep.com',
						'password' => '$2y$10$kTQXXg/R3wJKeav9ppIc3O3QryTaxn0.Zq5B2jZAIOk9Ay0gP/oHO',
						'isAdmin' => true,
                        'status' => 'Active',
						'usergroup_id' => 1,
    				)
    		);
        \CMS\UserGroup::create(
                array(
                        'name' => 'Super User',
                    )
            );
        \CMS\UserGroup::create(
                array(
                        'name' => 'Admin',
                    )
            );
    }
}
