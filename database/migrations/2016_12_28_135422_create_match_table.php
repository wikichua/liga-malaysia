<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seq');
            $table->integer('year');
            $table->datetime('start_date_time');
            $table->datetime('end_date_time');
            $table->integer('team_home_id');
            $table->integer('team_away_id');
            $table->string('tournament');
            $table->string('tournament_type');
            $table->string('round');
            $table->string('location');
            $table->string('state');
            $table->string('status');
            $table->string('team_home_result')->nullable();
            $table->string('team_away_result')->nullable();
            $table->integer('team_home_goal')->default(0);
            $table->integer('team_away_goal')->default(0);
            $table->text('remark')->nullable();
            $table->datetime('postponed_at')->nullable();
            $table->integer('postponed_by')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match');
    }
}
