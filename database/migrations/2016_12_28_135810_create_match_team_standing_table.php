<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchTeamStandingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_team_standing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seq');
            $table->integer('year');
            $table->string('tournament');
            $table->integer('match_id');
            $table->integer('profile_team_id');
            $table->integer('goal_scored')->default(0);
            $table->integer('goal_allow')->default(0);
            $table->integer('plus_minus')->default(0);
            $table->integer('points')->default(0);
            $table->integer('number_of_play')->default(1);
            $table->integer('number_of_win')->default(0);
            $table->integer('number_of_draw')->default(0);
            $table->integer('number_of_lose')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_team_standing');
    }
}
