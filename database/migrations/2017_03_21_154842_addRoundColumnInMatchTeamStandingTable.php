<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRoundColumnInMatchTeamStandingTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('match_team_standing', function (Blueprint $table) {
				$table->string('round');
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('match_team_standing', function (Blueprint $table) {
				//
			});
	}
}
