<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddRelatedMatchIDToMatchTable extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::table('match', function (Blueprint $table) {
				$table->integer('related_match_id')->nullable()->default(0);
			});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down() {
		Schema::table('match', function (Blueprint $table) {
				$table->dropColumn('related_match_id');
			});
	}
}
