<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfileTeamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_teams', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seq');
            $table->string('name');
            $table->string('logo');
            $table->string('nickname');
            $table->string('head_coach');
            $table->text('home_ground');
            $table->text('website_url');
            $table->text('fb_url');
            $table->text('twitter_url');
            $table->text('instagram_url');
            $table->text('tournaments');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_teams');
    }
}
