<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchPlayerStandingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_player_standing', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seq');
            $table->integer('year');
            $table->string('tournament');
            $table->integer('match_id');
            $table->integer('profile_team_id');
            $table->integer('profile_player_id');
            $table->string('action');
            $table->string('time');
            $table->text('remark');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_player_standing');
    }
}
