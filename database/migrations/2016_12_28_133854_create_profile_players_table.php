<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProfilePlayersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profile_players', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seq');
            $table->string('name');
            $table->string('profile_picture');
            $table->string('position');
            $table->string('nationality');
            $table->string('squad_number');
            $table->integer('profile_team_id');
            $table->integer('appearance');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('profile_players');
    }
}
