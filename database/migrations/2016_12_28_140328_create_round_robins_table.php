<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoundRobinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('round_robins', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seq');
            $table->integer('year');
            $table->string('name');
            $table->integer('profile_team_id');
            $table->string('tournament');
            $table->string('tournament_type');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('round_robins');
    }
}
