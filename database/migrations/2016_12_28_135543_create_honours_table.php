<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHonoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('honours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('seq');
            $table->string('name');
            $table->integer('year');
            $table->integer('profile_team_id');
            $table->integer('index_key');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('honours');
    }
}
