<?php

namespace CMS\Http\Requests;

use CMS\Http\Requests\Request;

class ProfileTeamRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth()->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'website_url'=>'url',
            'fb_url'=>'url',
            'twitter'=>'url',
            'instagram_url'=>'url',
            ];
    }
}
