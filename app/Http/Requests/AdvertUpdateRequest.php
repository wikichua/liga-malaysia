<?php

namespace CMS\Http\Requests;

use CMS\Http\Requests\Request;

class AdvertUpdateRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if(request()->get('status') == 'Draft')
        {
            return [
                'name' => 'required',
                'status' => 'required',
                'weblink_url'=>'url',

            ];
        }else{
            return [
                'name' => 'required',
                'published_at' => 'required',
                'expired_at' => 'required|after:published_at',
                'status' => 'required',
                'weblink_url'=>'url',

            ];
            
        }

    }
}
