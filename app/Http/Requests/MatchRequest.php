<?php

namespace CMS\Http\Requests;

use CMS\Http\Requests\Request;

class MatchRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return auth()->check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		return [
			// 'start_date_time' => request()->get('state') != 'Result'?'required':'',
			'location'     => request()->get('state') != 'Result'?'required':'',
			'remark'       => request()->get('isPostpond')?'required':'',
			'team_home_id' => 'required|different:team_away_id',
			'team_away_id' => 'required|different:team_home_id',
		];
	}

	public function messages() {
		return [
			'team_home_id.different' => 'Team Home must be different from Team Away',
			'team_away_id.different' => 'Team Away must be different from Team Home',
		];
	}
}
