<?php

namespace CMS\Http\Requests;

use CMS\Http\Requests\Request;

class AdvertStoreRequest extends Request {
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		if (request()->get('status') == 'Draft') {
			return [
				'image'       => 'mimes:jpeg,jpg,png',
				'name'        => 'required',
				'status'      => 'required',
				'weblink_url' => 'url',

			];
		} else {
			return [
				'image'        => 'mimes:jpeg,jpg,png',
				'name'         => 'required',
				'published_at' => 'required|before:tomorrow',
				'expired_at'   => 'required|after:published_at',
				'status'       => 'required',
				'weblink_url'  => 'url',

			];

		}

	}
}
