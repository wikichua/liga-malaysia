<?php
namespace CMS\Http\Misc;

trait GlobalTrait
{
	public function setPublishedAtAttribute($value)
    {
        $this->attributes['published_at'] = date('Y-m-d H:i:00',strtotime($value));
    }

    public function getPublishedAtAttribute($value)
    {
        return (new \Carbon\Carbon($value))->format('d-m-Y H:i:00');
    }
	
	public function setExpiredAtAttribute($value)
    {
        $this->attributes['expired_at'] = date('Y-m-d H:i:00',strtotime($value));
    }

    public function getExpiredAtAttribute($value)
    {
        return (new \Carbon\Carbon($value))->format('d-m-Y H:i:00');
    }

	public function setCreatedAtAttribute($value)
    {
        $this->attributes['created_at'] = date('Y-m-d H:i:s',strtotime($value));
    }

    public function getCreatedAtAttribute($value)
    {
        return (new \Carbon\Carbon($value))->format('d-m-Y H:i:s');
    }

	public function setUpdatedAtAttribute($value)
    {
        $this->attributes['created_at'] = date('Y-m-d H:i:s',strtotime($value));
    }

    public function getUpdatedAtAttribute($value)
    {
        return (new \Carbon\Carbon($value))->format('d-m-Y H:i:s');
    }
}