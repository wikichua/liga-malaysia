<?php
namespace CMS\Http\Misc;

use CMS\UserDevice;

use \CMS\PushRecord;

class PushNotification {
	private $title;
	private $UserDevices;
	private $message;
	private $env;
	private $module;
	private $id;
	private $custom_data;

	public function __construct($options = array(
			'title'       => 'Content Update',
			'UserDevices' => '',
			'message'     => '',
			'env'         => 'development',
			'module'      => '',
			'id'          => '',
			'custom_data' => '',
		)) {
		$this->title       = $options['title'];
		$this->UserDevices = $options['UserDevices'];
		$this->message     = $options['message'];
		$this->env         = $options['env'];
		$this->module      = $options['module'];
		$this->id          = $options['id'];
		$this->custom_data = $options['custom_data'];
	}

	private function Push_FCM($devices) {
		$title       = $this->title;
		$message     = $this->message;
		$env         = $this->env;
		$module      = $this->module;
		$id          = $this->id;
		$custom_data = $this->custom_data;

		// Queue::push(function($job) use($title, $message,$devices,$env,$protocol,$id,$custom_data)
		// {
		if ($custom_data == '') {$custom_data = array();
		}

		$apiKey = config()->get('push-notification.appNameFirebase.apiKey');
		$client = new \paragraph1\phpFCM\Client();
		$client->setApiKey($apiKey);
		$client->injectHttpClient(new \GuzzleHttp\Client());

		foreach ($devices as $deviceToken) {
			$Message = new \paragraph1\phpFCM\Message();
			$Message->addRecipient(new \paragraph1\phpFCM\Recipient\Device(str_replace(' ', '', $deviceToken)));
			$UserDevice = UserDevice::where('push_token', $deviceToken)->first();
		}

		$Notification = new \paragraph1\phpFCM\Notification($title, $message);
		if ($module != '' && $id != '') {
			// $Notification->setClickAction($module);
			$Message->setNotification($Notification)->setData(
				array_merge(
					array(
						'protocol' => $module,
						'id'       => $id,
					),
					$custom_data
				)
			);
		} else {
			$Message->setNotification($Notification);
		}

		$Response = $client->send($Message);
		$result   = $Response->getStatusCode();
		$success  = $result == 200?1:0;
		$error    = $result;
		if (count($devices) == 1) {
			PushRecord::create(array(
					'device_id' => $UserDevice->id,
					'os'        => $UserDevice->os,
					'protocol'  => $module,
					'module_id' => $id,
					'message'   => $message,
					'success'   => $success,
					'error'     => $error,
				));
		}

		// $job->delete();
		// },array(),'football_pn');
	}

	private function PushIt($UserDevice) {
		$devices[] = $UserDevice->push_token;
		try {
			$this->Push_FCM($devices);
		} catch (Exception $e) {
			if ($env == 'development') {
				dd($e->getMessage());
			}
		}
	}

	public function push() {
		$UserDevices = $this->UserDevices;
		$env         = $this->env;
		foreach ($UserDevices as $UserDevice) {
			$this->PushIt($UserDevice);
		}
	}
}