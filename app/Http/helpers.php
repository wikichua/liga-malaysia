<?php

function uuid() {
    return sprintf( '%04x%04x-%04x-%08x-%04x-%04x%04x%04x',
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
        mt_rand( 0, 0xffff ),
        mt_rand( 0, 0x0fff ) | 0x4000,
        mt_rand( 0, 0x3fff ) | 0x8000,
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function activeOnCurrentRoute($regex){
  if (preg_match('/\b'.$regex.'(\.)?\b/',Route::currentRouteName())) {
    return 'active';
  }
  return '';
}

function ACLButtonCheck($module,$action = 'Read')
{
    if(auth()->user()->isAdmin)
        return true;
    else
    {
        $ACL = \CMS\ACL::where('usergroup_id',auth()->user()->usergroup_id)->where('module',$module)->first();
        if($ACL)
        {
            $permissions = json_decode($ACL->permission,true);
            if(in_array($action,$permissions))
            {
                return true;
            }
        }
    }

    return false;
}

function ACLAllButtonsCheck($modules,$action)
{
    if(auth()->user()->isAdmin)
        return true;
    else
    {
        $ACLs = \CMS\ACL::where('usergroup_id',auth()->user()->usergroup_id)->whereIn('module',$modules)->get();
        foreach ($ACLs as $ACL) {
            if($ACL)
            {
                $permissions = json_decode($ACL->permission,true);
                if(in_array($action,$permissions))
                {
                    return true;
                }
            }
        }
    }

    return false;
}

function sortTableHeaderSnippet($field='',$fieldname = '')
{
    $fullUrla = $fullUrld = isset(parse_url(Request::fullUrl())['query'])? parse_url(Request::fullUrl())['query']:'';
    parse_str($fullUrla, $fullUrla);
    parse_str($fullUrld, $fullUrld);
    unset($fullUrld['a'],$fullUrla['d']);

    $fullUrla['a'] = $fieldname; 
    $fullUrla = http_build_query($fullUrla);
    $fullUrld['d'] = $fieldname; 
    $fullUrld = http_build_query($fullUrld);
    return $field.' <div class="btn-group btn-group-xs">
            <a href="'.Request::url().'?'.$fullUrla.'" style="position:absolute;top:-12px;left:1px;color:#66CFF6;display:block;overflow:hidden;line-height:1px;float:right"><i class="fa fa-lg fa-caret-up icon-sort"></i></a>
            <a href="'.Request::url().'?'.$fullUrld.'" style="position:absolute;top:-3px;left:1px;color:#66CFF6;display:block;overflow:hidden;line-height:1px;float:right"><i class="fa fa-lg fa-caret-down icon-sort"></i></a>
            </div>';
}

function searchTableHeaderSnippet($field='',$type='text',$options=array())
{
    if($type == 'select')
    {
        return Form::select('s-'.$field, array(''=>'None') + $options, request()->get('s-'.$field), array('class'=>"input-sm form-control select2"));
    }
    elseif($type == 'date')
    {
        return Form::text('s-'.$field, request()->get('s-'.$field), array('class'=>"input-sm form-control datepicker"));
    }
    elseif($type == 'datetime'){
        return Form::text('s-'.$field, request()->get('s-'.$field), array('class'=>"input-sm form-control datetimepicker"));
    }else{
        return Form::text('s-'.$field, request()->get('s-'.$field), array('class'=>"input-sm form-control"));
    }
}
function search_reset_buttons()
{
    return '
    <div class="btn-group btn-group-sm">
        <button type="submit" class="btn btn-info btn"><i class="fa fa-search"></i></button>
        <a href="'.Request::url().'" class="btn btn-warning btn"><i class="fa fa-times"></i></a>
    </div>';
}

function action_add_button($route)
{
    return '<a href="'.$route.'" title="Create" data-toggle="tooltip" class="btn btn-default btn-sm"><i class="fa fa-plus"></i> New</a>';
}

function action_update_button($route)
{
    return '<a href="'.$route.'" title="Update" data-toggle="tooltip" class="btn btn-default btn-sm"><i class="fa fa-pencil"></i> Update</a>';
}

function shifting_buttons($object,$route1,$route2)
{
    $str = '';
    if(isSearchOrSortExecuted())
        return '';
    if($object->p_id)
        $str .= '<a href="'.$route1.'" class="btn btn-md btn-default" data-toggle="tooltip" data-placement="auto top" title="Shift Up"><span class="fa fa-chevron-circle-up"></span></a>';
    if($object->n_id)
        $str .= '<a href="'.$route2.'" class="btn btn-md btn-default" data-toggle="tooltip" data-placement="auto bottom" title="Shift Down"><span class="fa fa-chevron-circle-down"></span></a>';
    return $str;
}

function isSearchOrSortExecuted()
{
    $hide = false;
    if(count(request()->all()))
    {
        if(request()->has('a') || request()->has('d'))
            $hide = true;
        else{
            foreach (request()->all() as $key => $value) {
                if(preg_match('/^s-/i', $key))
                {
                    $hide = true;
                    break;
                }
            }
        }
    }
    
    return $hide;
}

function uploadImage($inputKey,$existFile='',$directory='')
{
    if (request()->hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = request()->file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '.' . $originExtension;
        Image::make($File)->save($destinationPath.'/'.$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }
        return $time .'/'. $fileName;
    }else
       return $existFile;
}

function removeUploadFile($file)
{
    $uploads_path = public_path().'/uploads';
    if($file != '' && file_exists($uploads_path .'/'.$file))
    {
        chmod($uploads_path .'/'.$file, 0777);
        return unlink($uploads_path .'/'.$file);
    }
    return;
}

function uploadFile($inputKey,$existFile='',$directory='')
{
    if (request()->hasFile($inputKey))
    {
        $uploads_path = public_path().'/uploads';
        $time = !empty($directory)? $directory:time();
        $destinationPath = $uploads_path .'/'. $time;
        if(!is_dir($destinationPath))
        {
            mkdir($destinationPath, 0777);
            chmod($destinationPath, 0777);
        }

        $File = request()->file($inputKey);
        $originName = $File->getClientOriginalName();
        $originExtension = $File->getClientOriginalExtension();
        $fileName = $inputKey.'_'. (str_replace(array('.',' '),'',microtime(true))) . '.' . $originExtension;
        $result = $File->move($destinationPath,$fileName);
        chmod($destinationPath.'/'.$fileName, 0777);
        if($existFile != '' && file_exists($uploads_path .'/'.$existFile))
        {
            chmod($uploads_path.'/'.$existFile, 0777);
            unlink($uploads_path .'/'.$existFile);
        }

        return $time .'/'. $fileName;
    }

    return $existFile;
}

function imgTagShow($filename,$type='default')
{
    $nopic = array(
            'default'=>'no_image.jpg',
            'profile'=>'no-profile.gif',
        );
    $pic = $nopic[strtolower($type)];
    if (!empty(trim($filename)) && file_exists(public_path().'/uploads/'.$filename))
        $pic = $filename;
    else
        $pic = '../img/'.$pic;

    return $pic;
}

function fileTagShow($filename)
{
    if (!empty(trim($filename)) && file_exists(public_path().'/uploads/'.$filename))
    {
        return '<a href="'.asset('uploads/'.$filename).'" target="_blank" class="btn btn-link col-sm-12">'.basename($filename).'</a>';
    }
    return '';
}
