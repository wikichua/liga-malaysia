<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Requests\UserStoreRequest;
use CMS\Http\Requests\UserUpdateRequest;
use CMS\User;
use CMS\UserGroup;

class UserController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		if (auth()->user()->isAdmin) {
			$Users = User::where('id', '!=', auth()->id())->where('id', '!=', 1)->search()->sort()->paginate(25);
		} else {
			$Users = User::where('id', '!=', auth()->id())->where('isAdmin', false)->search()->sort()->paginate(25);
		}

		return view('user.index')->with(compact('Users'));
	}

	public function create() {
		if (auth()->user()->isAdmin) {
			$usergroups = UserGroup::lists('name', 'id')->toArray();
		} else {
			$usergroups = UserGroup::where('id', '!=', 1)->lists('name', 'id')->toArray();
		}

		return view('user.create')->with(compact('usergroups'));
	}

	public function store(UserStoreRequest $request) {
		$User = User::where('email', $request->get('email'))->withTrashed()->first();

		if (!$User) {
			$User = User::create(
				array(
					'name'         => $request->get('name'),
					'email'        => $request->get('email'),
					'password'     => bcrypt($request->get('password')),
					'isAdmin'      => $request->get('usergroup_id') === 1?true:false,
					'usergroup_id' => $request->get('usergroup_id', 0),
					'status'       => $request->get('status', 'Inactive'),
					'photo'        => uploadImage('photo', '', 'profile-photo'),
				)
			);
		} else {
			if ($User->deleted_at) {
				$User->restore();
				$User->name         = $request->get('name', $User->name);
				$User->email        = $request->get('email', $User->email);
				$User->password     = $request->has('password') && !empty(trim($request->get('password')))?bcrypt($request->get('password')):$User->password;
				$User->isAdmin      = $request->get('usergroup_id') === 1?true:false;
				$User->usergroup_id = $request->get('usergroup_id', $User->usergroup_id);
				$User->status       = $request->get('status', 'Inactive');
				$User->photo        = uploadImage('photo', $User->photo, 'profile-photo');
				$User->save();
				return redirect()->route('user')->with('success', 'Record Existed and Restored.');
			}
			return redirect()->route('user')->with('success', 'Record Existed.');
		}

		return redirect()->route('user')->with('success', 'Record created.');
	}

	public function edit($id) {
		$User = User::find($id);
		if (auth()->user()->isAdmin) {
			$usergroups = UserGroup::lists('name', 'id')->toArray();
		} else {
			$usergroups = UserGroup::where('id', '!=', 1)->lists('name', 'id')->toArray();
		}

		return view('user.edit')->with(compact('User', 'usergroups'));
	}

	public function update(UserUpdateRequest $request, $id) {
		$User               = User::find($id);
		$User->name         = $request->get('name', $User->name);
		$User->email        = $request->get('email', $User->email);
		$User->password     = $request->has('password') && !empty(trim($request->get('password')))?bcrypt($request->get('password')):$User->password;
		$User->isAdmin      = $request->get('usergroup_id') === 1?true:false;
		$User->usergroup_id = $request->get('usergroup_id', $User->usergroup_id);
		$User->status       = $request->get('status', 'Inactive');
		$User->photo        = uploadImage('photo', $User->photo, 'profile-photo');
		$User->save();

		return redirect()->route('user')->with('success', 'Record Updated.');
	}

	public function destroy($id) {
		return User::destroy($id);
	}

	public function switch_user($id = '0') {
		if ($id != 0) {
			session()->put('original_user_id', auth()->user()->id);
			auth()->login(User::find($id));
			return redirect()->route('dashboard')->with('success', 'You have switched to '.auth()->user()->name.'.');
		} else {
			auth()->login(User::find(session()->get('original_user_id')));
			session()->forget('original_user_id');
			return redirect()->route('dashboard')->with('success', 'You have switched back to '.auth()->user()->name.'.');
		}
	}
}
