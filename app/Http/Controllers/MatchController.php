<?php

namespace CMS\Http\Controllers;

use Carbon\Carbon;

use CMS\Http\Controllers\Controller;
use CMS\Http\Misc\PushNotification;

use CMS\Http\Requests\MatchRequest;
use CMS\Match;
use CMS\MatchFavorite;

use CMS\MatchPlayerStanding;
use CMS\PlayersInMatches;
use CMS\ProfilePlayer;
use CMS\ProfileTeam;
use Illuminate\Http\Request;

class MatchController extends Controller {
	public function __construct($fromAPI = false) {
		if ($fromAPI == false) {
			$this->middleware('auth');
		}
	}

	public function index() {
		$this->scanMatchesToStartMatch();
		$this->scanMatchesToEndMatch();

		$Teams_name = array();
		foreach (ProfileTeam::orderBy('name', 'asc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->name] = $ProfileTeam->name;
		}
		$Matchs = Match::where('start_date_time', '<>', '00-00-0000 00:00:00')->search()->sort()->orderBy('start_date_time', 'desc')->paginate(25);
		return view('match.index')->with(compact('Matchs', 'Teams_name'));
	}

	public function tbc() {
		$this->scanMatchesToStartMatch();
		$this->scanMatchesToEndMatch();

		$Teams_name = array();
		foreach (ProfileTeam::orderBy('name', 'asc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->name] = $ProfileTeam->name;
		}
		$Matches = Match::where('start_date_time', '00-00-0000 00:00:00')->search()->sort()->orderBy('tournament', 'asc')->paginate(25);
		return view('match.tbc')->with(compact('Matches', 'Teams_name'));
	}

	public function create() {
		$Teams_name         = array();
		$related_match_list = array();
		foreach (ProfileTeam::orderBy('name', 'asc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->id] = $ProfileTeam->name;
		}
		$all     = ProfileTeam::orderBy('name', 'asc')->get();
		$matches = Match::where('tournament_type', 'Cup')->whereIn('round', array('Quarter-Final-1', 'Semi-Final-1'))->get();
		foreach ($matches as $match) {
			$related_match_list[$match->id] = $match->round.'('.$match->ProfileTeamHome->name.' vs '.$match->ProfileTeamAway->name.')';
		}
		return view('match.create')->with(compact('Teams_name', 'all', 'related_match_list'));
	}

	public function store(MatchRequest $request) {
		$start_date_time = '00-00-0000 00:00:00';
		$end_date_time   = '00-00-0000 00:00:00';
		$year            = 0;
		if ($request->get('start_date_time') != '') {
			$start_date_time = new Carbon($request->get('start_date_time'));
			$end_date_time   = (new Carbon($request->get('start_date_time')))->addHours(3)->addMinutes(2);
			$year            = $start_date_time->format('Y');
		}

		$Cups            = config('fb.tournament.Cup');
		$Leagues         = config('fb.tournament.League');
		$tournament_code = $request->get('tournament');
		$tournament_type = isset($Cups[$tournament_code])?'Cup':'League';
		$tournament_name = isset($Cups[$tournament_code])?$Cups[$tournament_code]:$Leagues[$tournament_code];

		$Match = Match::create(
			array(
				'seq'              => Match::max('seq')+1,
				'year'             => $year,
				'start_date_time'  => $start_date_time,
				'end_date_time'    => $end_date_time,
				'team_home_id'     => $request->get('team_home_id'),
				'team_away_id'     => $request->get('team_away_id'),
				'tournament'       => $tournament_code,
				'tournament_type'  => $tournament_type,
				'round'            => $request->get('round'),
				'location'         => $request->get('location'),
				'state'            => 'Fixture',
				'status'           => 'Upcoming',
				'remark'           => $request->get('remark'),
				'related_match_id' => $request->get('related_match_id'),
			)
		);

		if ($request->get('start_date_time') == '') {

			return redirect()->route('match.tbc')->with('success', 'Record created.');
		}

		return redirect()->route('match', array('s-state=Fixture'))->with('success', 'Record created.');
	}

	public function edit($id) {
		$related_match_list = array();
		$isPostpond         = 0;
		$Teams_name         = array();
		foreach (ProfileTeam::orderBy('name', 'desc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->id] = $ProfileTeam->name;
		}
		$Match   = Match::find($id);
		$matches = Match::where('id', '!=', $id)->where('tournament_type', 'Cup')->whereIn('round', array('Quarter-Final-1', 'Semi-Final-1'))->get();
		foreach ($matches as $match) {
			$related_match_list[$match->id] = $match->round.'('.$match->ProfileTeamHome->name.' vs '.$match->ProfileTeamAway->name.')';
		}
		return view('match.edit')->with(compact('Match', 'Teams_name', 'isPostpond', 'related_match_list'));
	}
	public function postpond($id) {
		$related_match_list = array();
		$isPostpond         = 1;
		$Teams_name         = array();
		foreach (ProfileTeam::orderBy('name', 'desc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->id] = $ProfileTeam->name;
		}
		$Match   = Match::find($id);
		$matches = Match::where('id', '!=', $id)->where('tournament_type', 'Cup')->whereIn('round', array('Quarter-Final-1', 'Semi-Final-1'))->get();
		foreach ($matches as $match) {
			$related_match_list[$match->id] = $match->round.'('.$match->ProfileTeamHome->name.' vs '.$match->ProfileTeamAway->name.')';
		}
		return view('match.edit')->with(compact('Match', 'Teams_name', 'isPostpond', 'related_match_list'));
	}

	public function update(MatchRequest $request, $id) {
		$start_date_time = '00-00-0000 00:00:00';
		$end_date_time   = '00-00-0000 00:00:00';
		$year            = 0;
		if ($request->get('start_date_time') != '') {
			$start_date_time = new Carbon($request->get('start_date_time'));
			$end_date_time   = (new Carbon($request->get('start_date_time')))->addHours(3)->addMinutes(2);
			$year            = $start_date_time->format('Y');
		}

		$Match                  = Match::find($id);
		$Match->start_date_time = $start_date_time;
		$Match->end_date_time   = $end_date_time;
		$Match->location        = $request->get('location', $Match->location);
		$Match->remark          = $request->get('remark', $Match->remark);
		if ($start_date_time != '00-00-0000 00:00:00') {
			$pn_remark = str_replace(config('fb.postpone_message_placeholder'), array(
					$Match->ProfileTeamHome()->first()->name,
					$Match->ProfileTeamAway()->first()->name,
					$Match->start_date_time,
					$Match->location,
				), $request->get('remark', $Match->remark));
		}
		$Match->related_match_id = $request->get('related_match_id', $Match->related_match_id);
		if ($request->get('isPostpond')) {
			$Match->postponed_at = date('Y-m-d H:i:s');
			$Match->postponed_by = auth()->user()->id;
			$Match->state        = 'Fixture';
			$Match->status       = 'Upcoming';

			$Standing = New \CMS\Http\Controllers\StandingController(true);
			$Actions  = MatchPlayerStanding::where('match_id', $id)->get();
			foreach ($Actions as $Action) {
				$Standing->destroy($id, $Action->id);
			}

			$UserDevices    = array();
			$MatchFavorites = MatchFavorite::where('match_id', $id)->get();
			foreach ($MatchFavorites as $Favorite) {
				$UserDevices[] = $Favorite->DeviceUser;
			}
			if ($start_date_time != '00-00-0000 00:00:00') {

				(new PushNotification(array(
							'title'       => 'Match Rescheduled',
							'UserDevices' => $UserDevices,
							'message'     => $pn_remark,
							'env'         => env('PUSH_ENV', 'production'),
							'module'      => 'Match Fixture',
							'id'          => $id,
							'custom_data' => '',
						)))->push();
			}
		}

		$Match->save();
		if ($start_date_time != '00-00-0000 00:00:00') {
			return redirect()->route('match', array('s-state='.$Match->state))->with('success', 'Record Updated.');
		} else {
			return redirect()->route('match.tbc')->with('success', 'Record Updated.');
		}
	}

	public function destroy($id) {
		$Matches = Match::where('related_match_id', $id)->get();

		if (count($Matches)) {
			foreach ($Matches as $Match) {
				$Match->related_match_id = 0;
				$Match->save();
			}
		}

		$Standing = New \CMS\Http\Controllers\StandingController(true);
		$Actions  = MatchPlayerStanding::where('match_id', $id)->get();
		foreach ($Actions as $Action) {
			$Standing->destroy($id, $Action->id);
		}
		\CMS\MatchTeamStanding::where('match_id', $id)->delete();
		session()->flash('success', 'Record deleted.');
		return Match::destroy($id);
	}

	public function scanMatchesToStartMatch() {
		$test = Match::where('state', 'Fixture')->where('start_date_time', '<>', '00-00-0000 00:00:00')->where('start_date_time', '<=', \Carbon\Carbon::now())->update(array('state' => 'OnGoing', 'status' => 'Start'));

		return true;
	}

	public function scanMatchesToEndMatch() {
		$Matches = Match::where('state', 'OnGoing')->where('end_date_time', '<=', \Carbon\Carbon::now())->get();

		foreach ($Matches as $Match) {
			(new \CMS\Http\Controllers\StandingController(true))->end($Match->id, true);
		}

		return true;
	}

	public function players_in_matches($id) {
		$Match            = Match::find($id);
		$team_home        = ProfileTeam::find($Match->team_home_id);
		$team_away        = ProfileTeam::find($Match->team_away_id);
		$checked          = array();
		$PlayersInMatches = PlayersInMatches::where('match_id', $id)->get();
		foreach ($PlayersInMatches as $key => $value) {
			$checked[] = $value->player_id;
		}
		$home_Players = ProfilePlayer::where('profile_team_id', $team_home->id)->orderBy('name', 'asc')->get();
		$away_Players = ProfilePlayer::where('profile_team_id', $team_away->id)->orderBy('name', 'asc')->get();

		return view('match.players_in_matches')->with(compact('Match', 'team_home', 'team_away', 'home_Players', 'away_Players', 'checked'));
		;
	}

	public function players_in_matches_create(Request $request, $id) {

		$Match = Match::find($id);
		if ($request->get('away_player')) {
			foreach ($request->get('away_player') as $key => $away_player) {
				$Players = ProfilePlayer::find($away_player);

				$first            = (PlayersInMatches::where('player_id', $Players->id) == null);
				$PlayersInMatches = PlayersInMatches::updateOrCreate(['player_id' => $Players->id, 'match_id' => $id],
					array(
						'player_id'  => $Players->id,
						'team_id'    => $Match->team_away_id,
						'match_id'   => $id,
						'tournament' => $Match->tournament,
						'year'       => $Match->year,
						'match_type' => 'away',
					)
				);
				if ($first) {
					$Players->appearance = $Players->appearance+1;
					$Players->save();
				}

			}

			$away = PlayersInMatches::where('team_id', $Match->team_away_id)->where('match_type', 'away')->get();
			foreach ($away as $key => $value) {

				if (in_array($value->player_id, $request->get('away_player'))) {

				} else {
					$value->deleted_at = Carbon::now();
					$value->save();
					$Player             = ProfilePlayer::find($value->player_id);
					$Player->appearance = $Player->appearance-1;
					$Player->save();
				}

			}
		} else {
			if ($away = PlayersInMatches::where('team_id', $Match->team_away_id)->where('match_type', 'away')->get()) {
				foreach ($away as $key => $value) {
					$value->deleted_at = Carbon::now();
					$value->save();
					$Player             = ProfilePlayer::find($value->player_id);
					$Player->appearance = $Player->appearance-1;
					$Player->save();
				}
			}

		}

		if ($request->get('home_player')) {
			foreach ($request->get('home_player') as $key => $home_player) {
				$Players          = ProfilePlayer::find($home_player);
				$first            = (PlayersInMatches::where('player_id', $Players->id) == null);
				$PlayersInMatches = PlayersInMatches::updateOrCreate(['player_id' => $Players->id, 'match_id' => $id],
					array(
						'player_id'  => $Players->id,
						'team_id'    => $Players->profile_team_id,
						'match_id'   => $id,
						'tournament' => $Match->tournament,
						'year'       => $Match->year,
						'match_type' => 'home',
					)
				);
				if ($first) {
					$Players->appearance = $Players->appearance+1;
					$Players->save();
				}
			}
			$home = PlayersInMatches::where('team_id', $Match->team_home_id)->where('match_type', 'home')->get();
			foreach ($home as $key => $value) {

				if (in_array($value->player_id, $request->get('home_player'))) {

				} else {
					$value->deleted_at = Carbon::now();
					$value->save();
					$Player             = ProfilePlayer::find($value->player_id);
					$Player->appearance = $Player->appearance-1;
					$Player->save();
				}

			}
		} else {
			if ($home = PlayersInMatches::where('team_id', $Match->team_home_id)->where('match_type', 'home')->get()) {
				foreach ($home as $key => $value) {
					$value->deleted_at = Carbon::now();
					$value->save();
					$Player             = ProfilePlayer::find($value->player_id);
					$Player->appearance = $Player->appearance-1;
					$Player->save();
				}
			}

		}

		$Match     = Match::find($id);
		$team_home = ProfileTeam::find($Match->team_home_id);
		$team_away = ProfileTeam::find($Match->team_away_id);

		$home_Players = ProfilePlayer::where('profile_team_id', $team_home->id)->get();
		$away_Players = ProfilePlayer::where('profile_team_id', $team_away->id)->get();

		return redirect()->route('match.players_in_matches', $id)->with('success', 'Record created.');
	}

}
