<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Misc\ShiftingTrait;

use CMS\Http\Requests\ProfilePlayerRequest;
use CMS\ProfilePlayer;
use CMS\ProfileTeam;

class ProfilePlayerController extends Controller {
	use ShiftingTrait;

	public function __construct() {
		$this->middleware('auth');
	}

	public function index($team_id) {
		$ProfilePlayers = ProfilePlayer::where('profile_team_id', $team_id)->search()->sort()->orderBy('name', 'asc')->paginate(25);
		$ProfileTeam    = ProfileTeam::find($team_id);
		return view('profile_players.index')->with(compact('ProfilePlayers', 'team_id', 'ProfileTeam'));
	}

	public function create($team_id) {

		return view('profile_players.create')->with(compact('team_id'));
	}

	public function store(ProfilePlayerRequest $request, $team_id) {
		$ProfilePlayer = ProfilePlayer::create(
			array(
				'profile_team_id' => $team_id,
				'name'            => $request->get('name'),
				'profile_picture' => uploadImage('image', '', 'profile-player-gllery'),
				'position'        => $request->get('position'),
				'nationality'     => $request->get('nationality'),
				'squad_number'    => $request->get('squad_number'),
				'appearance'      => $request->get('appearance'),
			)
		);

		return redirect()->route('profile_players', $team_id)->with('success', 'Record created.');
	}

	public function edit($team_id, $id) {
		$ProfilePlayer = ProfilePlayer::find($id);
		return view('profile_players.edit')->with(compact('team_id', 'ProfilePlayer'));
	}

	public function update(ProfilePlayerRequest $request, $team_id, $id) {
		$ProfilePlayer = ProfilePlayer::find($id);
		if ($request->get('remove_image') == 'true') {
			removeUploadFile($ProfilePlayer->profile_picture);
			$ProfilePlayer->profile_picture = '';
		} else {
			$ProfilePlayer->profile_picture = uploadImage('image', $ProfilePlayer->profile_picture, 'profile-player-gllery');
		}

		$ProfilePlayer->name         = $request->get('name', $ProfilePlayer->name);
		$ProfilePlayer->position     = $request->get('position', $ProfilePlayer->position);
		$ProfilePlayer->nationality  = $request->get('nationality', $ProfilePlayer->nationality);
		$ProfilePlayer->squad_number = $request->get('squad_number', $ProfilePlayer->squad_number);
		$ProfilePlayer->appearance   = $request->get('appearance', $ProfilePlayer->appearance);
		$ProfilePlayer->save();

		return redirect()->route('profile_players', $team_id)->with('success', 'Record Updated.');
	}

	public function destroy($team_id, $id) {
		session()->flash('success', 'Record deleted.');
		return ProfilePlayer::destroy($id);
	}

}