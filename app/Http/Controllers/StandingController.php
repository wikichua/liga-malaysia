<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Misc\PushNotification;
use CMS\Http\Misc\ShiftingTrait;

use CMS\Match;
use CMS\MatchFavorite;
use CMS\MatchPlayerStanding;
use CMS\MatchTeamStanding;
use CMS\ProfilePlayer;
use CMS\ProfileTeam;
use CMS\UserDevice;

use Illuminate\Http\Request;

class StandingController extends Controller {
	use ShiftingTrait;

	public function __construct($fromAPI = false) {
		if ($fromAPI == false) {
			$this->middleware('auth');
		}
	}

	public function edit($match_id) {
		$Match       = Match::find($match_id);
		$TeamHome    = array();
		$TeamAway    = array();
		$HomePlayers = ProfilePlayer::where('profile_team_id', $Match->team_home_id)->lists('name', 'id')->toArray();
		$AwayPlayers = ProfilePlayer::where('profile_team_id', $Match->team_away_id)->lists('name', 'id')->toArray();

		$TeamHome[$Match->ProfileTeamHome()->first()->name] = $HomePlayers;
		$TeamHome[$Match->ProfileTeamAway()->first()->name] = $AwayPlayers;

		$TeamAway[$Match->ProfileTeamAway()->first()->name] = $AwayPlayers;
		$TeamAway[$Match->ProfileTeamHome()->first()->name] = $HomePlayers;

		$Standings = MatchPlayerStanding::where('year', $Match->year)->where('match_id', $Match->id)->where('tournament', $Match->tournament)->orderBy('time', 'asc')->search()->sort()->paginate(25);

		return view('standing.edit')->with(compact('Match', 'match_id', 'TeamHome', 'TeamAway', 'Standings'));
	}

	public function penalty_edit($match_id) {
		$Match    = Match::find($match_id);
		$TeamHome = ProfilePlayer::where('profile_team_id', $Match->team_home_id)->lists('name', 'id')->toArray();
		$TeamAway = ProfilePlayer::where('profile_team_id', $Match->team_away_id)->lists('name', 'id')->toArray();

		$Standings = MatchPlayerStanding::where('year', $Match->year)->where('match_id', $Match->id)->where('tournament', $Match->tournament)->orderBy('time', 'asc')->search()->sort()->paginate(25);
		return view('standing.penalty_edit')->with(compact('Match', 'match_id', 'TeamHome', 'TeamAway', 'Standings'));
	}

	public function penalty_update(Request $request, $match_id) {
		$Match = Match::find($match_id);

		$Match->team_home_penalty = $request->get('team_home_penalty');
		$Match->team_away_penalty = $request->get('team_away_penalty');

		$MatchTeamStanding_home = MatchTeamStanding::firstOrCreate(
			array(
				'year'            => $Match->year,
				'match_id'        => $Match->id,
				'tournament'      => $Match->tournament,
				'profile_team_id' => $Match->team_home_id,
				'round'           => $Match->round,
			)
		);
		$MatchTeamStanding_away = MatchTeamStanding::firstOrCreate(
			array(
				'year'            => $Match->year,
				'match_id'        => $Match->id,
				'tournament'      => $Match->tournament,
				'profile_team_id' => $Match->team_away_id,
				'round'           => $Match->round,
			)
		);
		$MatchTeamStanding_home->number_of_win  = 0;
		$MatchTeamStanding_away->number_of_lose = 0;
		$MatchTeamStanding_home->number_of_draw = 0;
		$MatchTeamStanding_away->number_of_draw = 0;
		$MatchTeamStanding_away->number_of_win  = 0;
		$MatchTeamStanding_home->number_of_lose = 0;
		$MatchTeamStanding_home->points         = 0;
		$MatchTeamStanding_away->points         = 0;

		$MatchTeamStanding_home->number_of_play = 1;
		$MatchTeamStanding_away->number_of_play = 1;

		$total_home_goal = $Match->team_home_goal+$Match->team_home_penalty;
		$total_away_goal = $Match->team_away_goal+$Match->team_away_penalty;

		if (in_array($Match->round, array('Quarter-Final-2', 'Semi-Final-2')) && \CMS\Match::find($Match->related_match_id)) {
			$total_home_goal += \CMS\Match::find($Match->related_match_id)->team_home_goal;
			$total_away_goal += \CMS\Match::find($Match->related_match_id)->team_away_goal;
		}

		if ($total_home_goal > $total_away_goal) {
			if ($Match->tournament_type != 'Cup') {
				$MatchTeamStanding_home->points = 3;
			}
			$MatchTeamStanding_home->number_of_win  = 1;
			$MatchTeamStanding_away->number_of_lose = 1;
			$Match->team_home_result                = 'Win';
			$Match->team_away_result                = 'Lose';
		} elseif ($total_home_goal == $total_away_goal) {
			if ($Match->tournament_type != 'Cup') {
				$MatchTeamStanding_home->points = 1;
				$MatchTeamStanding_away->points = 1;
			}
			$MatchTeamStanding_home->number_of_draw = 1;
			$MatchTeamStanding_away->number_of_draw = 1;
			$Match->team_home_result                = 'Draw';
			$Match->team_away_result                = 'Draw';
		} else {
			if ($Match->tournament_type != 'Cup') {
				$MatchTeamStanding_away->points = 3;
			}
			$MatchTeamStanding_away->number_of_win  = 1;
			$MatchTeamStanding_home->number_of_lose = 1;
			$Match->team_away_result                = 'Win';
			$Match->team_home_result                = 'Lose';
		}

		$MatchTeamStanding_away->save();
		$MatchTeamStanding_home->save();
		$Match->save();

		return redirect()->route('standing.penalty_edit', $match_id)->with('success', 'Record created.');
	}

	public function update(Request $request, $match_id) {
		$Match = Match::find($match_id);
		$save  = false;
		for ($i = 1; $i <= 6; $i++) {
			if ($request->has('profile_player_id_'.$i)) {
				$save                = true;
				$MatchPlayerStanding = MatchPlayerStanding::create(
					array(
						'seq'               => MatchPlayerStanding::max('seq')+1,
						'year'              => $Match->year,
						'match_id'          => $Match->id,
						'tournament'        => $Match->tournament,
						'profile_team_id'   => $request->get('profile_team_id_'.$i),
						'profile_player_id' => $request->get('profile_player_id_'.$i),
						'action'            => $request->get('action_'.$i),
						'time'              => $request->get('time_'.$i),
						'remark'            => $request->get('remark_'.$i),
					)
				);

				$MatchTeamStanding_home = MatchTeamStanding::firstOrCreate(
					array(
						'year'            => $Match->year,
						'match_id'        => $Match->id,
						'tournament'      => $Match->tournament,
						'profile_team_id' => $Match->team_home_id,
						'round'           => $Match->round,
					)
				);

				$MatchTeamStanding_away = MatchTeamStanding::firstOrCreate(
					array(
						'year'            => $Match->year,
						'match_id'        => $Match->id,
						'tournament'      => $Match->tournament,
						'profile_team_id' => $Match->team_away_id,
						'round'           => $Match->round,
					)
				);

				$Team     = ProfileTeam::find($request->get('profile_team_id_'.$i));
				$Player   = ProfilePlayer::find($request->get('profile_player_id_'.$i));
				$pn_title = config('fb.score_action.'.$request->get('action_'.$i)).'! ';
				$pn_msg   = '';
				switch ($request->get('action_'.$i)) {
					case 'YC':
					case 'RC':
						$pn_msg .= $Player->name.'('.$Team->name.') has obtained a '.config('fb.score_action.'.$request->get('action_'.$i)).' at '.$request->get('time_'.$i).' minutes during the match.';
						break;

					case 'OG':
						if ($request->get('match_position_'.$i) == 'HOME') {
							$Match->team_home_goal++;
							$MatchTeamStanding_home->goal_scored++;
							$MatchTeamStanding_away->goal_allow++;
							$OppTeam                              = ProfileTeam::find($Match->team_away_id);
							$MatchPlayerStanding->profile_team_id = $OppTeam->id;
							$MatchPlayerStanding->save();
						} else {
							$Match->team_away_goal++;
							$MatchTeamStanding_away->goal_scored++;
							$MatchTeamStanding_home->goal_allow++;
							$OppTeam                              = ProfileTeam::find($Match->team_home_id);
							$MatchPlayerStanding->profile_team_id = $OppTeam->id;
							$MatchPlayerStanding->save();
						}

						$MatchTeamStanding_home->plus_minus = $MatchTeamStanding_home->goal_scored-$MatchTeamStanding_home->goal_allow;
						$MatchTeamStanding_away->plus_minus = $MatchTeamStanding_away->goal_scored-$MatchTeamStanding_away->goal_allow;
						$pn_msg .= $Player->name.' ('.$OppTeam->name.') has scored a '.config('fb.score_action.'.$request->get('action_'.$i)).' at '.$request->get('time_'.$i).' minutes during the match.';
						break;

					default:
						if ($request->get('match_position_'.$i) == 'HOME') {
							$Match->team_home_goal++;
							$MatchTeamStanding_home->goal_scored++;
							$MatchTeamStanding_away->goal_allow++;
						} else {
							$Match->team_away_goal++;
							$MatchTeamStanding_away->goal_scored++;
							$MatchTeamStanding_home->goal_allow++;
						}

						$MatchTeamStanding_home->plus_minus = $MatchTeamStanding_home->goal_scored-$MatchTeamStanding_home->goal_allow;
						$MatchTeamStanding_away->plus_minus = $MatchTeamStanding_away->goal_scored-$MatchTeamStanding_away->goal_allow;
						$pn_msg .= $Player->name.' ('.$Team->name.') has scored a '.config('fb.score_action.'.$request->get('action_'.$i)).' at '.$request->get('time_'.$i).' minutes during the match.';
						break;
				}

				if ($Match->status == 'End') {
					$MatchTeamStanding_home->number_of_win  = 0;
					$MatchTeamStanding_away->number_of_lose = 0;
					$MatchTeamStanding_home->number_of_draw = 0;
					$MatchTeamStanding_away->number_of_draw = 0;
					$MatchTeamStanding_away->number_of_win  = 0;
					$MatchTeamStanding_home->number_of_lose = 0;
					$MatchTeamStanding_home->number_of_play = 1;
					$MatchTeamStanding_away->number_of_play = 1;
					$MatchTeamStanding_home->points         = 0;
					$MatchTeamStanding_away->points         = 0;

					$total_home_goal = $Match->team_home_goal+$Match->team_home_penalty;
					$total_away_goal = $Match->team_away_goal+$Match->team_away_penalty;

					if (in_array($Match->round, array('Quarter-Final-2', 'Semi-Final-2')) && \CMS\Match::find($Match->related_match_id)) {
						$total_home_goal += \CMS\Match::find($Match->related_match_id)->team_home_goal;
						$total_away_goal += \CMS\Match::find($Match->related_match_id)->team_away_goal;
					}

					if ($total_home_goal > $total_away_goal) {
						if ($Match->tournament_type != 'Cup') {
							$MatchTeamStanding_home->points = 3;
						}
						$MatchTeamStanding_home->number_of_win  = 1;
						$MatchTeamStanding_away->number_of_lose = 1;
						$Match->team_home_result                = 'Win';
						$Match->team_away_result                = 'Lose';
					} elseif ($total_home_goal == $total_away_goal) {
						if ($Match->tournament_type != 'Cup') {
							$MatchTeamStanding_home->points = 1;
							$MatchTeamStanding_away->points = 1;
						}
						$MatchTeamStanding_home->number_of_draw = 1;
						$MatchTeamStanding_away->number_of_draw = 1;
						$Match->team_home_result                = 'Draw';
						$Match->team_away_result                = 'Draw';
					} else {
						if ($Match->tournament_type != 'Cup') {
							$MatchTeamStanding_away->points = 3;
						}
						$MatchTeamStanding_away->number_of_win  = 1;
						$MatchTeamStanding_home->number_of_lose = 1;
						$Match->team_away_result                = 'Win';
						$Match->team_home_result                = 'Lose';
					}
				}

				$MatchTeamStanding_away->save();
				$MatchTeamStanding_home->save();
				$Match->save();

				$emails      = MatchFavorite::where('match_id', $match_id)->lists('email');
				$UserDevices = UserDevice::whereIn('imei', $emails)->where('push_token', '!=', '')->get();

				if ($request->get('action_'.$i) == 'GL' || $request->get('action_'.$i) == 'OG') {
					(new PushNotification(array(
								'title'       => $pn_title,
								'UserDevices' => $UserDevices,
								'message'     => $pn_msg,
								'env'         => env('PUSH_ENV', 'production'),
								'module'      => 'Match OnGoing',
								'id'          => $match_id,
								'custom_data' => array('tournament' => $Match->tournament),
							)))                                           ->push();
				}
			}
		}

		if ($save) {
			return redirect()->route('standing.edit', $match_id)->with('success', 'Score Save.');
		}

		return redirect()->route('standing.edit', $match_id)->with('error', 'Please select a player.');
	}

	public function end($match_id, $fromAPI = false) {
		$Match         = Match::find($match_id);
		$Match->state  = 'Result';
		$Match->status = 'End';

		$MatchTeamStanding_home = MatchTeamStanding::firstOrCreate(
			array(
				'year'            => $Match->year,
				'match_id'        => $Match->id,
				'tournament'      => $Match->tournament,
				'profile_team_id' => $Match->team_home_id,
				'round'           => $Match->round,
			)
		);
		$MatchTeamStanding_away = MatchTeamStanding::firstOrCreate(
			array(
				'year'            => $Match->year,
				'match_id'        => $Match->id,
				'tournament'      => $Match->tournament,
				'profile_team_id' => $Match->team_away_id,
				'round'           => $Match->round,
			)
		);
		$MatchTeamStanding_home->number_of_win  = 0;
		$MatchTeamStanding_away->number_of_lose = 0;
		$MatchTeamStanding_home->number_of_draw = 0;
		$MatchTeamStanding_away->number_of_draw = 0;
		$MatchTeamStanding_away->number_of_win  = 0;
		$MatchTeamStanding_home->number_of_lose = 0;
		$MatchTeamStanding_home->points         = 0;
		$MatchTeamStanding_away->points         = 0;

		$MatchTeamStanding_home->number_of_play = 1;
		$MatchTeamStanding_away->number_of_play = 1;

		$total_home_goal = $Match->team_home_goal+$Match->team_home_penalty;
		$total_away_goal = $Match->team_away_goal+$Match->team_away_penalty;

		if (in_array($Match->round, array('Quarter-Final-2', 'Semi-Final-2')) && \CMS\Match::find($Match->related_match_id)) {
			$total_home_goal += \CMS\Match::find($Match->related_match_id)->team_home_goal;
			$total_away_goal += \CMS\Match::find($Match->related_match_id)->team_away_goal;
		}

		if ($total_home_goal > $total_away_goal) {
			if ($Match->tournament_type != 'Cup') {
				$MatchTeamStanding_home->points = 3;
			}
			$MatchTeamStanding_home->number_of_win  = 1;
			$MatchTeamStanding_away->number_of_lose = 1;
			$Match->team_home_result                = 'Win';
			$Match->team_away_result                = 'Lose';
		} elseif ($total_home_goal == $total_away_goal) {
			if ($Match->tournament_type != 'Cup') {
				$MatchTeamStanding_home->points = 1;
				$MatchTeamStanding_away->points = 1;
			}
			$MatchTeamStanding_home->number_of_draw = 1;
			$MatchTeamStanding_away->number_of_draw = 1;
			$Match->team_home_result                = 'Draw';
			$Match->team_away_result                = 'Draw';
		} else {
			if ($Match->tournament_type != 'Cup') {
				$MatchTeamStanding_away->points = 3;
			}
			$MatchTeamStanding_away->number_of_win  = 1;
			$MatchTeamStanding_home->number_of_lose = 1;
			$Match->team_away_result                = 'Win';
			$Match->team_home_result                = 'Lose';
		}

		$MatchTeamStanding_away->save();
		$MatchTeamStanding_home->save();
		$Match->save();

		if ($fromAPI == false) {
			session()->flash('success', 'Match Ended.');
			return route('match', 's-id='.$match_id);
		}

		return true;
	}

	public function destroy($match_id, $standing_id) {
		$Match    = Match::find($match_id);
		$Standing = MatchPlayerStanding::find($standing_id);

		$MatchTeamStanding_home = MatchTeamStanding::firstOrCreate(
			array(
				'year'            => $Match->year,
				'match_id'        => $Match->id,
				'tournament'      => $Match->tournament,
				'profile_team_id' => $Match->team_home_id,
				'round'           => $Match->round,
			)
		);
		$MatchTeamStanding_away = MatchTeamStanding::firstOrCreate(
			array(
				'year'            => $Match->year,
				'match_id'        => $Match->id,
				'tournament'      => $Match->tournament,
				'profile_team_id' => $Match->team_away_id,
				'round'           => $Match->round,
			)
		);

		if ($Standing->action == 'GL') {
			if ($Standing->profile_team_id == $Match->team_home_id) {
				$Match->team_home_goal               = $Match->team_home_goal <= 0?0:$Match->team_home_goal-1;
				$MatchTeamStanding_home->goal_scored = $MatchTeamStanding_home->goal_scored <= 0?0:$MatchTeamStanding_home->goal_scored-1;
				$MatchTeamStanding_away->goal_allow  = $MatchTeamStanding_away->goal_allow <= 0?0:$MatchTeamStanding_away->goal_allow-1;
			} else {
				$Match->team_away_goal               = $Match->team_away_goal <= 0?0:$Match->team_away_goal-1;
				$MatchTeamStanding_away->goal_scored = $MatchTeamStanding_away->goal_scored <= 0?0:$MatchTeamStanding_away->goal_scored-1;
				$MatchTeamStanding_home->goal_allow  = $MatchTeamStanding_home->goal_allow <= 0?0:$MatchTeamStanding_home->goal_allow-1;
			}
			$MatchTeamStanding_home->plus_minus = $MatchTeamStanding_home->goal_scored-$MatchTeamStanding_home->goal_allow;
			$MatchTeamStanding_away->plus_minus = $MatchTeamStanding_away->goal_scored-$MatchTeamStanding_away->goal_allow;
		}

		if ($Match->status == 'End') {
			$MatchTeamStanding_home->number_of_win  = 0;
			$MatchTeamStanding_away->number_of_lose = 0;
			$MatchTeamStanding_home->number_of_draw = 0;
			$MatchTeamStanding_away->number_of_draw = 0;
			$MatchTeamStanding_away->number_of_win  = 0;
			$MatchTeamStanding_home->number_of_lose = 0;
			$MatchTeamStanding_home->points         = 0;
			$MatchTeamStanding_away->points         = 0;
			$MatchTeamStanding_home->number_of_play = 1;
			$MatchTeamStanding_away->number_of_play = 1;

			$total_home_goal = $Match->team_home_goal+$Match->team_home_penalty;
			$total_away_goal = $Match->team_away_goal+$Match->team_away_penalty;

			if (in_array($Match->round, array('Quarter-Final-2', 'Semi-Final-2')) && \CMS\Match::find($Match->related_match_id)) {
				$total_home_goal += \CMS\Match::find($Match->related_match_id)->team_home_goal;
				$total_away_goal += \CMS\Match::find($Match->related_match_id)->team_away_goal;
			}
			if ($total_home_goal > $total_away_goal) {
				if ($Match->tournament_type != 'Cup') {
					$MatchTeamStanding_home->points = 3;
				}
				$MatchTeamStanding_home->number_of_win  = 1;
				$MatchTeamStanding_away->number_of_lose = 1;
				$Match->team_home_result                = 'Win';
				$Match->team_away_result                = 'Lose';
			} elseif ($total_home_goal == $total_away_goal) {
				if ($Match->tournament_type != 'Cup') {
					$MatchTeamStanding_home->points = 1;
					$MatchTeamStanding_away->points = 1;
				}
				$MatchTeamStanding_home->number_of_draw = 1;
				$MatchTeamStanding_away->number_of_draw = 1;
				$Match->team_home_result                = 'Draw';
				$Match->team_away_result                = 'Draw';
			} else {
				if ($Match->tournament_type != 'Cup') {
					$MatchTeamStanding_away->points = 3;
				}
				$MatchTeamStanding_away->number_of_win  = 1;
				$MatchTeamStanding_home->number_of_lose = 1;
				$Match->team_away_result                = 'Win';
				$Match->team_home_result                = 'Lose';
			}
		}

		$MatchTeamStanding_away->save();
		$MatchTeamStanding_home->save();
		$Match->save();

		session()->flash('success', 'Record deleted.');
		return MatchPlayerStanding::destroy($standing_id);
	}
}