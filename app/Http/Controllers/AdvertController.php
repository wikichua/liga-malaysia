<?php

namespace CMS\Http\Controllers;

use CMS\Advert;

use CMS\Http\Controllers\Controller;

use CMS\Http\Requests\AdvertStoreRequest;
use CMS\Http\Requests\AdvertUpdateRequest;

class AdvertController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		$statuses = config('fb.advert.status');
		$Adverts  = Advert::search()->sort()->orderBy('published_at', 'asc')->paginate(25);
		return view('advert.index')->with(compact('Adverts', 'statuses'));
	}

	public function create() {
		$statuses = config('fb.advert.status');
		unset($statuses['Expired']);
		return view('advert.create')->with(compact('statuses'));
	}

	public function store(AdvertStoreRequest $request) {
		$Advert = Advert::create(
			array(
				'seq'          => Advert::max('seq')+1,
				'name'         => $request->get('name'),
				'image'        => uploadImage('image', '', 'advert'),
				'tournaments'  => $request->get('tournaments', json_encode([])),
				'status'       => $request->get('status'),
				'weblink_url'  => $request->get('weblink_url'),
				'published_at' => date('Y-m-d 00:00:00', strtotime($request->get('published_at'))),
				'expired_at'   => date('Y-m-d 23:59:59', strtotime($request->get('expired_at'))),
			)
		);

		return redirect()->route('advert')->with('success', 'Record created.');
	}

	public function edit($id) {
		$statuses = config('fb.advert.status');
		unset($statuses['Expired']);
		$Advert = Advert::find($id);
		return view('advert.edit')->with(compact('Advert', 'statuses'));
	}

	public function update(AdvertUpdateRequest $request, $id) {
		$Advert               = Advert::find($id);
		$Advert->name         = $request->get('name', $Advert->name);
		$Advert->image        = uploadImage('image', $Advert->image, 'advert');
		$Advert->tournaments  = json_encode($request->get('tournaments', (json_decode($Advert->tournaments)?json_decode($Advert->tournaments, true):array())));
		$Advert->status       = $request->get('status', $Advert->status);
		$Advert->weblink_url  = $request->get('weblink_url', $Advert->weblink_url);
		$Advert->published_at = date('Y-m-d 00:00:00', strtotime($request->get('published_at', $Advert->published_at)));
		$Advert->expired_at   = date('Y-m-d 23:59:59', strtotime($request->get('expired_at', $Advert->expired_at)));
		$Advert->save();

		return redirect()->route('advert')->with('success', 'Record Updated.');
	}

	public function destroy($id) {
		session()->flash('success', 'Record deleted.');
		return Advert::destroy($id);
	}
}