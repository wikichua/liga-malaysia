<?php

namespace CMS\Http\Controllers;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Controllers\Controller;
use CMS\Http\Misc\ShiftingTrait;
use CMS\AppVersion;

class AppVersionController extends Controller
{
    use ShiftingTrait;

    private $OSs = array('IOS'=>'IOS','APK'=>'APK');
    private $types = array('Optional'=>'Optional','Critical'=>'Critical');

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
     	$AppVersions = AppVersion::search()->sort()->orderBy('version','desc')->paginate(25);
        return view('app_version.index')->with(compact('AppVersions'));
    }

    public function create()
    {
        $OSs = $this->OSs;
        $types = $this->types;
        return view('app_version.create')->with(compact('OSs','types'));
    }

    public function store(Request $request)
    {
        $AppVersion = AppVersion::create(
                array(
                        'os' => $request->get('os'),
                        'version' => $request->get('version'),
                        'type' => $request->get('type'),
                        'description' => $request->get('description'),
                    )
            );

        return redirect()->route('app.version')->with('success','Record created.');
    }

    public function edit($id)
    {
        $AppVersion = AppVersion::find($id);
        $OSs = $this->OSs;
        $types = $this->types;
        return view('app_version.edit')->with(compact('AppVersion','OSs','types'));
    }

    public function update(Request $request, $id)
    {
        $AppVersion = AppVersion::find($id);
        $AppVersion->os = $request->get('os',$AppVersion->os);
        $AppVersion->version = $request->get('version',$AppVersion->version);
        $AppVersion->type = $request->get('type',$AppVersion->type);
        $AppVersion->description = $request->get('description',$AppVersion->description);
        $AppVersion->save();

        return back()->with('success','Record Updated.');
    }

    public function destroy($id)
    {
        session()->flash('success','Record deleted.');
        return AppVersion::destroy($id);
    }
}
