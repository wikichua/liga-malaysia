<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Requests\ProfileTeamRequest;

use CMS\ProfileTeam;

class ProfileTeamController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		$ProfileTeams = ProfileTeam::search()->sort()->orderBy('name', 'asc')->paginate(25);
		return view('profile_team.index')->with(compact('ProfileTeams'));
	}

	public function create() {
		return view('profile_team.create')->with(compact(''));
	}

	public function store(ProfileTeamRequest $request) {
		$ProfileTeam = ProfileTeam::create(
			array(
				'seq'           => ProfileTeam::max('seq')+1,
				'logo'          => uploadImage('logo', '', 'profile-team-logo'),
				'name'          => $request->get('name'),
				'tournaments'   => $request->get('tournaments'),
				'nickname'      => $request->get('nickname'),
				'head_coach'    => $request->get('head_coach'),
				'home_ground'   => $request->get('home_ground'),
				'website_url'   => $request->get('website_url'),
				'fb_url'        => $request->get('fb_url'),
				'twitter_url'   => $request->get('twitter_url'),
				'instagram_url' => $request->get('instagram_url'),
			)
		);

		return redirect()->route('profile_team')->with('success', 'Record created.');
	}

	public function edit($id) {
		$ProfileTeam = ProfileTeam::find($id);
		return view('profile_team.edit')->with(compact('ProfileTeam'));
	}

	public function update(ProfileTeamRequest $request, $id) {
		$ProfileTeam                = ProfileTeam::find($id);
		$ProfileTeam->logo          = uploadImage('logo', $ProfileTeam->logo, 'profile-team-logo');
		$ProfileTeam->name          = $request->get('name', $ProfileTeam->name);
		$ProfileTeam->nickname      = $request->get('nickname', $ProfileTeam->nickname);
		$ProfileTeam->tournaments   = $request->get('tournaments', $ProfileTeam->tournaments);
		$ProfileTeam->head_coach    = $request->get('head_coach', $ProfileTeam->head_coach);
		$ProfileTeam->home_ground   = $request->get('home_ground', $ProfileTeam->home_ground);
		$ProfileTeam->website_url   = $request->get('website_url', $ProfileTeam->website_url);
		$ProfileTeam->fb_url        = $request->get('fb_url', $ProfileTeam->fb_url);
		$ProfileTeam->twitter_url   = $request->get('twitter_url', $ProfileTeam->twitter_url);
		$ProfileTeam->instagram_url = $request->get('instagram_url', $ProfileTeam->instagram_url);
		$ProfileTeam->save();

		return redirect()->route('profile_team')->with('success', 'Record Updated.');
	}
	public function shift($team_id, $id, $shift_id) {
		$this->shifting(new TeamGallery, $id, $shift_id);

		return back();
	}

	public function destroy($id) {
		session()->flash('success', 'Record deleted.');
		return ProfileTeam::destroy($id);
	}
}