<?php

namespace CMS\Http\Controllers;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Requests\ACLRequest;
use CMS\Http\Controllers\Controller;
use CMS\AuditTrail;

class AuditTrailController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function index()
    {
        $AuditTrails = AuditTrail::search()->sort()->orderBy('created_at','desc')->paginate(25);
        return view('audittrail.index')->with(compact('AuditTrails'));
    }

    public function show($id)
    {
        $AuditTrail = AuditTrail::find($id);

        // $AuditTrail->new_data = $this->refined($AuditTrail->new_data);
        // $AuditTrail->old_data = $this->refined($AuditTrail->old_data);
        
        return view('audittrail.show')->with(compact('AuditTrail'));
    }

    private function refined($data)
    {
        $string = '';
        if(!is_array($data))
        {
            $data = json_decode($data,true);
        }
        if(is_array($data) && count($data))
        {
            $string .= '<ul>';
            foreach ($data as $key => $value) {
                $string .= '<li>';
                if(is_array($value) || json_decode($value))
                {
                    $string .= ucwords(str_replace('_', ' ', $key)) .' : '. $this->refined($value);
                }else{
                    $string .= ucwords(str_replace('_', ' ', $key)) .' : '. (trim($value)==''? 'N/A':$value);
                }
                $string .= '</li>';
            }
            $string .= '</ul>';
        }
        return $string;
    }
}
