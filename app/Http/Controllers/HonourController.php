<?php

namespace CMS\Http\Controllers;

use CMS\Honour;

use CMS\Http\Controllers\Controller;
use CMS\Http\Misc\ShiftingTrait;

use CMS\Http\Requests\HonourRequest;
use CMS\ProfileTeam;

class HonourController extends Controller {
	use ShiftingTrait;

	public function __construct() {
		$this->middleware('auth');
	}

	public function index($team_id) {

		$this->reorganizeSEQ(Honour::where('profile_team_id', $team_id));
		$Honours = Honour::where('profile_team_id', $team_id)->search()->sort()->orderBy('year', 'desc')->paginate(25);
		$this->index_shift($Honours, Honour::where('profile_team_id', $team_id));
		$ProfileTeam = ProfileTeam::find($team_id);

		return view('honour.index')->with(compact('Honours', 'team_id', 'ProfileTeam'));
	}

	public function create($team_id) {

		return view('honour.create')->with(compact('team_id'));
	}

	public function store(HonourRequest $request, $team_id) {
		$Honour = Honour::create(
			array(
				'seq'             => Honour::where('profile_team_id', $team_id)->max('seq')+1,
				'profile_team_id' => $team_id,
				'name'            => $request->get('name'),
				'year'            => $request->get('year'),

			)
		);

		return redirect()->route('honour', $team_id)->with('success', 'Record created.');
	}

	public function edit($team_id, $id) {
		$Honour = Honour::find($id);

		return view('honour.edit')->with(compact('team_id', 'Honour'));
	}

	public function update(HonourRequest $request, $team_id, $id) {
		$Honour       = Honour::find($id);
		$Honour->year = $request->get('year', $Honour->year);
		$Honour->name = $request->get('name', $Honour->name);
		$Honour->save();

		return redirect()->route('honour', $team_id)->with('success', 'Record Updated.');
	}

	public function shift($team_id, $id, $shift_id) {
		$this->shifting(Honour::where('profile_team_id', $team_id), $id, $shift_id);

		return back();
	}

	public function destroy($team_id, $id) {
		session()->flash('success', 'Record deleted.');
		return Honour::destroy($id);
	}
}