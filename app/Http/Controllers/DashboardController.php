<?php

namespace CMS\Http\Controllers;

// use Analytics;
use Carbon\Carbon;
use CMS\Http\Controllers\Controller;

use CMS\Match;
use CMS\ProfileTeam;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
// use Spatie\Analytics\Period;

class DashboardController extends Controller {
	function __construct() {
		$this->middleware('auth');
	}

	public function index(Request $request) {
		// $GA_CHART_START_DATE = $request->get('ga_from', Carbon::now()->subDays(8)->format('d-m-Y'));
		// $GA_CHART_END_DATE   = $request->get('ga_to', Carbon::now()->subDays(1)->format('d-m-Y'));
		// $Period              = Period::create(Carbon::createFromFormat('d-m-Y', $GA_CHART_START_DATE), Carbon::createFromFormat('d-m-Y', $GA_CHART_END_DATE));

		// $users    = $this->getLast7Days()['users'];
		// $sessions = $this->getLast7Days()['sessions'];
		$Matchs   = Match::where('start_date_time', '>=', Carbon::now()->format('d-m-Y 00:00:00'))->where('start_date_time', '>=', Carbon::now()->addDays(1)->format('d-m-Y 00:00:00'))->get();
		$today    = Carbon::now()->format('d-m-Y');

		$Teams_name = array();
		foreach (ProfileTeam::orderBy('name', 'asc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->name] = $ProfileTeam->name;
		}

		$TodayMatchs = array();
		foreach ($Matchs as $match) {
			$start_date_time = substr($match->start_date_time, 0, 10);
			if ($start_date_time == $today) {
				$TodayMatchs[] = $match;
			}
		}

		// $pv       = array();
		// $pageview = array();
		// $Pageview = Analytics::fetchTotalVisitorsAndPageViews($Period, 20);
		// foreach ($Pageview as $key => $value) {
		// 	$value['date'] = $value['date']->toDateTimeString();
		// 	$pageview[]    = $value;
		// }
		// $pv['sessions'] = json_encode($pageview);
		// // dd($pv);
		// if (ACLButtonCheck('DASHBOARD', $action = 'Read')) {
		 	// return view('dashboard')->with(compact('sessions', 'pv', 'users', 'TodayMatchs', 'Teams_name', 'GA_CHART_START_DATE', 'GA_CHART_END_DATE'));
		// }

		// return view('maintenance')->with(compact('TodayMatchs', 'Teams_name', 'GA_CHART_START_DATE', 'GA_CHART_END_DATE'));
		return view('dashboard')->with(compact('TodayMatchs', 'Teams_name'));

	}

	public function getLast7Days()//$startDate,$endDate
	{
		// $endDate = Carbon::createFromFormat('d-m-Y',$endDate);
		// $startDate = Carbon::createFromFormat('d-m-Y',$startDate);
		$endDate   = Carbon::today();
		$startDate = Carbon::today()->subDays(7);
		$Period    = Period::create($startDate, $endDate);
		$users     = $this->getUser($Period);
		$sessions  = $this->getSession($Period);

		return compact('users', 'sessions');
	}

	// public function getLast30Days()
	// {
	//     $endDate = Carbon::today();
	//     $startDate = Carbon::today()->subDays(30);

	//     $users = $this->getUser($startDate,$endDate);
	//     $sessions = $this->getSession($startDate,$endDate);

	//     return compact('users','sessions');
	// }

	public function getUser($period) {
		$GAUsers = Analytics::performQuery($period, 'ga:users');

		$users = $GAUsers->rows[0][0];

		return compact('users');
	}

	public function getSession($period) {
		$groupBy = 'date';
		$answer  = Analytics::performQuery($period, 'ga:sessions', ['dimensions' => 'ga:'.$groupBy]);
		if (is_null($answer->rows)) {
			$getSessions = new Collection([]);
		} else {
			foreach ($answer->rows as $dateRow) {
				$sessionData[] = [$groupBy => Carbon::createFromFormat(($groupBy == 'yearMonth'?'Ym':'Ymd'), $dateRow[0]), 'sessions' => $dateRow[1]];
			}
			$getSessions = new Collection($sessionData);
		}

		$sessions = array();
		foreach ($getSessions as $Sessions) {
			$sessions[] = array(
				'date'  => $Sessions['date']->format('j M Y'),
				'count' => $Sessions['sessions'],
			);
		}

		$sessions = json_encode($sessions)?json_encode($sessions, true):array();
		return compact('sessions');
	}
}
