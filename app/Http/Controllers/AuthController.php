<?php

namespace CMS\Http\Controllers;

use Illuminate\Http\Request;

use CMS\Http\Requests;
use CMS\Http\Requests\AuthRequest;
use CMS\Http\Requests\ProfileRequest;
use CMS\Http\Controllers\Controller;
use Mail;
use CMS\User;
use Auth, Session;

class AuthController extends Controller
{
    public function index()
    {
        return view('login');
    }

    public function login(AuthRequest $request)
    {
        if (Auth::attempt(['email' => $request->get('email'), 'password' => $request->get('password'), 'status' => 'Active']))
        {
            return redirect()->intended(route('dashboard'))->with('message','Welcome to ' . config('cms.brand').' portal.');
        }else{
            return back()->withInput();
        }
    }

    public function logout()
    {
        Auth::logout();
        Session::flush();
        return redirect()->route('auth');
    }

    public function profile()
    {
        return view('profile');
    }

    public function profile_update(ProfileRequest $request)
    {
        $User = User::find(auth()->user()->id);
        $User->name = $request->get('name',$User->name);
        $User->email = $request->get('email',$User->email);
        $User->password = $request->has('password') && !empty(trim($request->get('password')))? bcrypt($request->get('password')):$User->password;
        $User->photo = uploadImage('photo',$User->photo,'profile-photo');
        $User->save();

        return back()->with('success','Record Updated.');
    }


    public function change_password_email()
    {

        return view('change_password');
    }
    public function send_email(Request $request)
    {
        if($User=User::where('email',$request->get('email'))->first()){

            $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';    
            $randstring = '';    
            for ($i = 0; $i < 10; $i++) {        
                $randstring = $randstring."".$characters[rand(0, strlen($characters))];    
            }

            $dates=['username'=> $User->name,'link'=>route('change_password',array($User->email,$randstring)),'password'=>$randstring];

            Mail::send('email.change_poassword', $dates, function ($message) use ($User) {
            $message->from('support@convep.com', 'football');
            $message->to($User->email, $User->name)->subject('Football Malaysia - Password Reset');
            });
        return  back()->with('success','Record Updated.');

        }
        return  back()->with('success','try again');
    }

    public function change_password($email,$password)
    {
        $User=User::where('email',$email)->first();
        $User->password=bcrypt($password);
        $User->save();
        return redirect()->route('auth');
    }


}
