<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Requests\VideoRequest;
use CMS\Video;

class VideoController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		$tournaments = array();
		foreach (config('fb.tournament') as $tour) {
			$tournaments = array_merge($tournaments, $tour);
		}
		$Videos = Video::search()->sort()->orderBy('published_at', 'asc')->paginate(25);
		return view('video.index')->with(compact('Videos', 'tournaments'));
	}

	public function create() {
		return view('video.create');
	}

	public function store(VideoRequest $request) {
		$Video = Video::create(
			array(
				'seq'          => Video::max('seq')+1,
				'name'         => $request->get('name'),
				'image'        => uploadImage('image', '', 'video'),
				'description'  => $request->get('description'),
				'published_at' => $request->get('published_at'),
				'tournament'   => $request->get('tournament'),
				'url'          => $request->get('url'),
			)
		);

		return redirect()->route('video')->with('success', 'Record created.');
	}

	public function edit($id) {
		$Video = Video::find($id);
		return view('video.edit')->with(compact('Video'));
	}

	public function update(VideoRequest $request, $id) {
		$Video               = Video::find($id);
		$Video->name         = $request->get('name', $Video->name);
		$Video->image        = uploadImage('image', $Video->image, 'video');
		$Video->description  = $request->get('description', $Video->description);
		$Video->published_at = $request->get('published_at', $Video->published_at);
		$Video->tournament   = $request->get('tournament', $Video->tournament);
		$Video->url          = $request->get('url', $Video->url);
		$Video->save();

		return redirect()->route('video')->with('success', 'Record Updated.');
	}

	public function destroy($id) {
		session()->flash('success', 'Record deleted.');
		return Video::destroy($id);
	}
}