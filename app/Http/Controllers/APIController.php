<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\UserDevice;
use Illuminate\Http\Request;

use Response;
use Validator;

class APIController extends Controller {
	private $Request, $Input;

	public function __construct(Request $Request) {
		$this->middleware('auth', array('only' => array('index')));

		$this->Input   = $Request;
		$this->Request = json_decode($Request->get('data'))?json_decode($Request->get('data'), true):array();
		if ($Request->has('debug') && $Request->get('debug') == 1) {
			dd($Request->all());
		}
	}

	private function validation($Request) {
		$Validator = Validator::make($this->Request, $Request->rules(), $Request->messages());
		if ($Validator->fails()) {
			return array(
				'status'  => 'failed',
				'message' => $Validator->errors()->all(),
			);
		} else {
			return true;
		}
	}

	private function response($module, $status = array(), $datas = array()) {
		$datas  = json_decode(json_encode($datas), true);
		$return = ['AppData' => [
				'module'           => $module,
				'settingIndex'     => date('Y-m-d H:i:s'),
				'portal_url'       => route('dashboard'),
				'download_url'     => route('dashboard'),
				'web_url'          => route('dashboard'),
				'totalRecords'     => count($datas),
				'last_updated_at'  => count($datas)?$this->get_last_updated_at($datas):date('Y-m-d H:i:s'),
				'version'          => '',
				'status'           => isset($status['status'])?$status['status']:'success',
				'message'          => isset($status['message'])?$status['message']:'',
			]
		];

		$return['Data'] = array();
		if (count($datas) > 0) {
			$this->resize_generate_url_for_images_or_files($datas);
			$return['Data'] = $datas;
		}

		return Response::make(str_replace(': null', ': ""', json_encode($return, JSON_PRETTY_PRINT)))->header('Content-Type', "application/json");
	}

	private function get_last_updated_at($datas) {
		$last_updated_at = array();

		foreach ($datas as $key => $value) {
			if (is_array($value)) {
				$last_updated_at[] = $this->get_last_updated_at($datas[$key]);
			} else {
				if ($key == 'updated_at') {
					$last_updated_at[] = $value;
				}
			}
		}

		if (count($last_updated_at)) {
			$time = max(array_map('strtotime', $last_updated_at));
			return date("Y-m-d H:i:s", $time);
		} else {
			return 0;
		}
	}

	private function resize_generate_url_for_images_or_files(&$datas) {
		foreach ($datas as $key => $value) {
			if (is_array($value)) {
				$this->resize_generate_url_for_images_or_files($datas[$key]);
			} else {
				if (strlen($value) <= 255) {

					if (preg_match('/([^\/]+)(?=\.\w+$)?([^\/]+?(\.gif|\.png|\.jpg|\.bmp|\.jpeg))/i', $value) && file_exists(public_path().'/uploads/'.$value)) {
						$datas[$key] = config('app.url').'/uploads/'.$value;
					} elseif (preg_match('/([^\/]+)(?=\.\w+$)?([^\/]+?(\.pdf|\.docs|\.xls|\.ppt))/i', $value) && file_exists(public_path().'/uploads/'.$value)) {
						$datas[$key] = config('app.url').'/uploads/'.$value;
					}
				}
			}
		}
	}

	public function index($api = 'pt') {
		$sample  = $file_keys  = $links  = array();
		$links[] = route('api', array('pt'));
		$links[] = route('api', array('news'));
		$links[] = route('api', array('video'));
		$links[] = route('api', array('team'));
		$links[] = route('api', array('advert'));
		$links[] = route('api', array('matches'));
		$links[] = route('api', array('match_fixture'));
		$links[] = route('api', array('match_ongoing'));
		$links[] = route('api', array('match_result'));
		$links[] = route('api', array('group_standings'));
		$links[] = route('api', array('list_favorite'));
		$links[] = route('api', array('add_favorite'));
		$links[] = route('api', array('delete_favorite'));
		$links[] = route('api', array('group_stage'));
		$links[] = route('api', array('standings'));
		$links[] = route('api', array('top_scores'));
		$links[] = route('api', array('players_standings'));

		$api_token  = session()->get('api_token', 'IOS:1234567890');
		$imei       = session()->get('imei', 'IOS:1234567890');
		$push_token = session()->get('push_token', '1');
		$match_id   = session()->get('sess_match_id', '0');

		switch (strtolower($api)) {

			case 'pt':
				$sample = array(
					'api_token'   => $api_token,
					'imei'        => $imei,
					'push_token'  => $push_token,
					'width'       => '100',
					'height'      => '100',
					'os_version'  => '1.0',
					'app_version' => '1.0',
				);
				break;
			case 'news':
			case 'video':
			case 'advert':
			case 'team':
			case 'group_stage':
			case 'matches':
			case 'match_fixture':
			case 'match_ongoing':
			case 'match_result':
			case 'group_standings':
			case 'standings':
			case 'top_scores':
				$sample = array(
					// 'api_token' => $api_token,
				);
				break;
			case 'players_standings':
				$sample = array(
					'api_token' => $api_token,
					'match_id'  => '',
				);
				break;
			case 'list_favorite':
				$sample = array(
					'api_token' => $api_token,
				);
				break;
			case 'add_favorite':
			case 'delete_favorite':
				$sample = array(
					'api_token' => $api_token,
					'match_id'  => $match_id,
				);
				break;
		}
		return view('api.index')->with(compact('api', 'sample', 'file_keys', 'links'));
	}

	public function login() {
		$status = array();
		$datas  = array();

		session()->put('api_token', $this->Request['api_token']);
		session()->put('imei', $this->Request['imei']);
		session()->put('push_token', $this->Request['push_token']);

		$os         = explode(':', $this->Request['imei'])[0];
		$imei       = explode(':', $this->Request['imei'])[1];
		$UserDevice = UserDevice::firstOrCreate(array(
				'imei'        => $imei,
				'os'          => $os,
				'width'       => trim($this->Request['width']),
				'height'      => trim($this->Request['height']),
				'os_version'  => trim($this->Request['os_version']),
				'app_version' => trim($this->Request['app_version']),
			));
		$UserDevice->push_token = trim($this->Request['push_token']);
		$UserDevice->api_token  = trim($this->Request['api_token']);
		$UserDevice->save();

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function pt() {
		$datas  = array();
		$status = array();

		$os         = explode(':', $this->Request['imei'])[0];
		$imei       = explode(':', $this->Request['imei'])[1];
		$UserDevice = UserDevice::firstOrCreate(array(
				'imei' => $imei,
				'os'   => $os,
			));
		$UserDevice->push_token  = trim($this->Request['push_token']);
		$UserDevice->width       = trim($this->Request['width']);
		$UserDevice->height      = trim($this->Request['height']);
		$UserDevice->os_version  = trim($this->Request['os_version']);
		$UserDevice->app_version = trim($this->Request['app_version']);
		$UserDevice->save();

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function logout() {
		$datas  = array();
		$status = array();

		UserDevice::where('api_token', $this->Request['api_token'])->forceDelete();

		session()->forget('api_token');
		session()->forget('imei');
		session()->forget('push_token');

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function news() {
		$datas   = array();
		$status  = array();
		$Cups    = config('fb.tournament.Cup');
		$Leagues = config('fb.tournament.League');
		$Newses  = \CMS\News::where('published_at', '<=', date('Y-m-d H:i:s'))->orderBy('published_at', 'desc')->get();

		foreach ($Newses as $News) {
			$News->tournament_code = $News->tournament;
			$News->tournament_type = isset($Cups[$News->tournament])?'Cup':'Leagues';
			$News->tournament_name = isset($Cups[$News->tournament])?$Cups[$News->tournament]:$Leagues[$News->tournament];
		}

		return $this->response(__FUNCTION__, $status, $datas = $Newses);
	}

	public function video() {
		$datas   = array();
		$status  = array();
		$Cups    = config('fb.tournament.Cup');
		$Leagues = config('fb.tournament.League');
		$Videos  = \CMS\Video::where('published_at', '<=', date('Y-m-d H:i:s'))->orderBy('published_at', 'asc')->get();

		foreach ($Videos as $Video) {
			$Video->tournament_code = $Video->tournament;
			$Video->tournament_type = isset($Cups[$Video->tournament])?'Cup':'Leagues';
			$Video->tournament_name = isset($Cups[$Video->tournament])?$Cups[$Video->tournament]:$Leagues[$Video->tournament];
		}

		return $this->response(__FUNCTION__, $status, $datas = $Videos);
	}

	public function team() {
		$datas           = array();
		$status          = array();
		$Cups            = config('fb.tournament.Cup');
		$Leagues         = config('fb.tournament.League');
		$all_tournaments = array_merge($Cups, $Leagues);
		$position        = config('fb.position');
		$years           = \CMS\MatchTeamStanding::distinct()->lists('year');
		$ProfileTeams    = \CMS\ProfileTeam::orderBy('name', 'asc')->get();
		foreach ($ProfileTeams as $ProfileTeam) {
			$arr = array();
			$ts  = array();
			foreach ($years as $year) {
				$arr[] = \CMS\MatchTeamStanding::where('year', $year)
					->where('profile_team_id', $ProfileTeam->id)->orderBy('tournament', 'desc')
				                                             ->select('year', 'tournament', \DB::raw('sum(goal_scored) as goal_scored,sum(goal_allow) as goal_allow,sum(plus_minus) as plus_minus,sum(points) as points,sum(number_of_play) as number_of_play,sum(number_of_win) as number_of_win,sum(number_of_draw) as number_of_draw,sum(number_of_lose) as number_of_lose'))->groupBy('year')->groupBy('tournament')->get();
			}

			foreach ($arr as $a) {
				foreach ($a as $b) {

					if ($b->tournament_name != '') {
						$b->tournament_name = isset($Cups[$b->tournament])?$Cups[$b->tournament]:$Leagues[$b->tournament];
					}
					$ts[] = $b;

				}
			}
			$ProfileTeam->team_standings = $ts;

			foreach ($ProfileTeam->ProfilePlayer as $ProfilePlayer) {
				$ProfilePlayer->position = isset($position[$ProfilePlayer->position])?$position[$ProfilePlayer->position]:'';
				if ($ProfilePlayer->nationality != '') {
					$ProfilePlayer->nationality = (is_numeric($ProfilePlayer->nationality))?config('fb.countries')[$ProfilePlayer->nationality]:$ProfilePlayer->nationality;
				}

				$ARR = array();
				foreach ($years as $year) {
					$MPS = array();
					foreach (array_keys($all_tournaments) as $each_tournament) {
						$arr     = array();
						$MPS_chk = \CMS\MatchPlayerStanding::where('year', $year)
							->where('tournament', $each_tournament)
							->where('profile_team_id', $ProfileTeam->id)
							->where('profile_player_id', $ProfilePlayer->id)
							->groupBy('year');

						if ($MPS_chk->count()) {
							$MPS[][$MPS_chk->first()->tournament] = $MPS_chk;
						}

						foreach ($MPS as $mps) {
							foreach ($mps as $tournament => $M) {
								$GL    = clone$M;
								$YC    = clone$M;
								$RC    = clone$M;
								$arr[] = array(
									'year'            => $year,
									'tournament'      => $tournament,
									'tournament_name' => isset($Cups[$tournament])?$Cups[$tournament]:$Leagues[$tournament],
									'goal'            => $GL->where('action', 'GL')->count(),
									'yellow_card'     => $YC->where('action', 'YC')->count(),
									'red_card'        => $RC->where('action', 'RC')->count(),
								);
							}
						}
					}
					$ARR[] = $arr;
				}
				$ProfilePlayer->player_standings = $ARR;

			}
			$ProfileTeam->TeamGallery = \CMS\TeamGallery::where('profile_team_id', $ProfileTeam->id)->orderBy('seq', 'desc')->get();

			$ProfileTeam->Honour = \CMS\Honour::where('profile_team_id', $ProfileTeam->id)->orderBy('seq', 'desc')->get();
			foreach ($ProfileTeam->Honour as $Honour) {
				if ($Honour->name != '') {
					$Honour->name = config('fb.honours')[$Honour->name];
				}
			}

		}

		return $this->response(__FUNCTION__, $status, $datas = $ProfileTeams);
	}

	public function advert() {
		$datas      = array();
		$status     = array();
		$Cups       = config('fb.tournament.Cup');
		$Leagues    = config('fb.tournament.League');
		$tournament =
		$Adverts    = \CMS\Advert::where('published_at', '<=', date('Y-m-d H:i:s'))
			->where('expired_at', '>', date('Y-m-d H:i:s'))
			->where('status', 'Published')
			->orderBy('published_at', 'asc')	->get();
		foreach ($Adverts as $key => $ad) {
			if ($ad->tournaments == '') {
				$ad->tournaments = [];
			} else {
				$ad->tournaments = json_decode($ad->tournaments);

			}

		}

		return $this->response(__FUNCTION__, $status, $datas = $Adverts);
	}

	public function matches() {
		$datas           = array();
		$status          = array();
		$all_tournaments = array_merge(config("fb.tournament")['League'], config("fb.tournament")['Cup']);
		$Matches         = \CMS\Match::orderBy('start_date_time', 'asc')->get();
		foreach ($Matches as $Match) {
			$Match->team_home_name = $Match->ProfileTeamHome;
			$Match->team_away_name = $Match->ProfileTeamAway;
			$Match->team_home_name = $Match->ProfileTeamHome()->first()->name;
			$Match->team_away_name = $Match->ProfileTeamAway()->first()->name;
		}

		return $this->response(__FUNCTION__, $status, $datas = $Matches);
	}

	public function match_fixture() {
		$datas   = array();
		$status  = array();
		$Matches = \CMS\Match::where('state', 'Fixture')->orderBy('start_date_time', 'asc')->get();

		foreach ($Matches as $Match) {
			$Match->team_home_name = $Match->ProfileTeamHome;
			$Match->team_away_name = $Match->ProfileTeamAway;
			$Match->team_home_name = $Match->ProfileTeamHome()->first()->name;
			$Match->team_away_name = $Match->ProfileTeamAway()->first()->name;
		}

		return $this->response(__FUNCTION__, $status, $datas = $Matches);
	}

	public function match_ongoing() {
		$datas   = array();
		$status  = array();
		$Matches = \CMS\Match::where('state', 'OnGoing')->orderBy('start_date_time', 'asc')->get();
		foreach ($Matches as $Match) {
			$Match->team_home_name = $Match->ProfileTeamHome;
			$Match->team_away_name = $Match->ProfileTeamAway;
			$Match->team_home_name = $Match->ProfileTeamHome()->first()->name;
			$Match->team_away_name = $Match->ProfileTeamAway()->first()->name;

			$matches = \CMS\MatchPlayerStanding::where('match_id', $Match->id)->orderBy('time', 'desc')->get();

			$action = array();
			foreach ($matches as $ft) {
				if ($ft->home_team()->where('id', $Match->id)->first()) {
					$ft->team_type = 'Home';
				} else {
					$ft->team_type = 'Away';
				}
				if ($ft->time < 46) {
					$ft->team_name         = $ft->team()->first()->name;
					$ft->player_name       = $ft->player()->first()->name;
					$ft->action            = config('fb.score_action')[$ft->action];
					$action['half_time'][] = $ft;
				} else if ($ft->time > 45 && $ft->time < 91) {
					$ft->team_name         = $ft->team()->first()->name;
					$ft->player_name       = $ft->player()->first()->name;
					$ft->action            = config('fb.score_action')[$ft->action];
					$action['full_time'][] = $ft;
				} else {
					$ft->team_name          = $ft->team()->first()->name;
					$ft->player_name        = $ft->player()->first()->name;
					$ft->action             = config('fb.score_action')[$ft->action];
					$action['extra_time'][] = $ft;
				}
			}

			$Match->players_standings = (!$action == [])?[$action]:$action;

		}

		return $this->response(__FUNCTION__, $status, $datas = $Matches);

	}

	public function match_result() {
		$datas   = array();
		$status  = array();
		$Matches = \CMS\Match::where('state', 'Result')->orderBy('end_date_time', 'asc')->get();
		foreach ($Matches as $Match) {
			$Match->home_accumulated_goal = 0;
			$Match->away_accumulated_goal = 0;
			if ($Match->tournament_type == 'Cup' && $Match->related_match_id != 0) {
				$Match->home_accumulated_goal = \CMS\Match::find($Match->related_match_id)->team_home_goal+$Match->team_home_goal;
				$Match->away_accumulated_goal = \CMS\Match::find($Match->related_match_id)->team_away_goal+$Match->team_away_goal;
			}

			$Match->profile_team_home            = \CMS\ProfileTeam::where('id', $Match->team_home_id)->withTrashed()->first();
			$Match->profile_team_home->standings = \CMS\MatchTeamStanding::select("goal_scored", "goal_allow", "plus_minus", "points", "number_of_play", "number_of_win", "number_of_draw", "number_of_lose")->where('match_id', $Match->id)->where('profile_team_id', $Match->team_home_id)->get();
			$Match->profile_team_home->players   = \CMS\ProfilePlayer::where('profile_team_id', $Match->team_home_id)->get();
			foreach ($Match->profile_team_home->players as $player) {
				$MPS = \CMS\MatchPlayerStanding::where('match_id', $Match->id)
				                                                         ->where('profile_team_id', $Match->team_home_id)
					->where('profile_player_id', $player->id);
				$player->player_standings = array(
					'goal'        => $MPS->where('action', 'GL')->count(),
					'yellow_card' => $MPS->where('action', 'YC')->count(),
					'red_card'    => $MPS->where('action', 'RC')->count(),
				);
			}

			$Match->profile_team_away            = \CMS\ProfileTeam::where('id', $Match->team_away_id)->withTrashed()->first();
			$Match->profile_team_away->standings = \CMS\MatchTeamStanding::select("goal_scored", "goal_allow", "plus_minus", "points", "number_of_play", "number_of_win", "number_of_draw", "number_of_lose")->where('match_id', $Match->id)->where('profile_team_id', $Match->team_away_id)->get();
			$Match->profile_team_away->players   = \CMS\ProfilePlayer::where('profile_team_id', $Match->team_away_id)->get();
			foreach ($Match->profile_team_away->players as $player) {
				$MPS = \CMS\MatchPlayerStanding::where('match_id', $Match->id)
				                                                         ->where('profile_team_id', $Match->team_away_id)
					->where('profile_player_id', $player->id);
				$player->player_standings = array(
					'goal'        => $MPS->where('action', 'GL')->count(),
					'yellow_card' => $MPS->where('action', 'YC')->count(),
					'red_card'    => $MPS->where('action', 'RC')->count(),
				);
			}

			$Match->team_home_name = $Match->ProfileTeamHome;
			$Match->team_away_name = $Match->ProfileTeamAway;
			$Match->team_home_name = $Match->ProfileTeamHome()->first()->name;
			$Match->team_away_name = $Match->ProfileTeamAway()->first()->name;

			$matches = \CMS\MatchPlayerStanding::where('match_id', $Match->id)->orderBy('time', 'desc')->get();

			$action = array();
			foreach ($matches as $ft) {
				if ($ft->home_team()->where('id', $Match->id)->first()) {
					$ft->team_type = 'Home';
				} else {
					$ft->team_type = 'Away';
				}
				if ($ft->time < 46) {
					$ft->team_name         = $ft->team()->first()->name;
					$ft->player_name       = $ft->player()->first()->name;
					$ft->action            = config('fb.score_action')[$ft->action];
					$action['half_time'][] = $ft;
				} else if ($ft->time > 45 && $ft->time < 91) {
					$ft->team_name         = $ft->team()->first()->name;
					$ft->player_name       = $ft->player()->first()->name;
					$ft->action            = config('fb.score_action')[$ft->action];
					$action['full_time'][] = $ft;
				} else {
					$ft->team_name          = $ft->team()->first()->name;
					$ft->player_name        = $ft->player()->first()->name;
					$ft->action             = config('fb.score_action')[$ft->action];
					$action['extra_time'][] = $ft;
				}
			}

			$Match->players_standings = (!$action == [])?[$action]:$action;

		}

		return $this->response(__FUNCTION__, $status, $datas = $Matches);
	}

	public function group_standings() {
		$datas       = array();
		$status      = array();
		$RoundRobins = \CMS\RoundRobin::orderBy('name', 'asc')->where('year', date("Y"))->get();

		$array = array();
		foreach ($RoundRobins as $RoundRobin) {
			$ProfileTeam = \CMS\ProfileTeam::find($RoundRobin->profile_team_id);
			$Standings   = \CMS\MatchTeamStanding::where('year', date("Y"))->where('profile_team_id', $RoundRobin->profile_team_id)->where('tournament', $RoundRobin->tournament)
				->select('year', 'round', 'tournament', \DB::raw('sum(goal_scored) as goal_scored,sum(goal_allow) as goal_allow,sum(plus_minus) as plus_minus,sum(points) as points,sum(number_of_play) as number_of_play,sum(number_of_win) as number_of_win,sum(number_of_draw) as number_of_draw,sum(number_of_lose) as number_of_lose'))	->groupBy('round')	->groupBy('tournament')	->orderBy('round', 'asc')	->get();

			foreach ($Standings as $Standing) {
				if ($Standing) {
					$Standing->round_robin     = $RoundRobin->name;
					$Standing->profile_team_id = $RoundRobin->profile_team_id;
					$Standing->team_name       = $ProfileTeam->name;
					$Standing->team_logo       = $ProfileTeam->logo;
					// $RoundRobin->team_standings = $Standings;
				}
			}
			$array[] = $Standings->toArray();
		}

		foreach ($array as $ars) {
			$datas = array_merge($datas, $ars);
		}

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function players_standings() {
		$datas  = array();
		$status = array();
		$match  = \CMS\MatchPlayerStanding::where('match_id', $this->Request['match_id'])->orderBy('profile_team_id')->orderBy('time')->get();

		foreach ($match as $ft) {
			if ($ft->home_team()->where('id', $this->Request['match_id'])->first()) {
				$ft->team_type = 'Home';
			} else {
				$ft->team_type = 'Away';
			}
			if ($ft->time < 46) {
				$ft->team_name        = $ft->team()->first()->name;
				$ft->player_name      = $ft->player()->first()->name;
				$ft->action           = config('fb.score_action')[$ft->action];
				$datas['half_time'][] = $ft;
			} else if ($ft->time > 45 && $ft->time < 91) {
				$ft->team_name        = $ft->team()->first()->name;
				$ft->player_name      = $ft->player()->first()->name;
				$ft->action           = config('fb.score_action')[$ft->action];
				$datas['full_time'][] = $ft;
			} else {
				$ft->team_name         = $ft->team()->first()->name;
				$ft->player_name       = $ft->player()->first()->name;
				$ft->action            = config('fb.score_action')[$ft->action];
				$datas['extra_time'][] = $ft;
			}
		}

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function list_favorite() {
		$datas  = array();
		$status = array();

		$Favorites = \CMS\MatchFavorite::has('Match')->where('email', str_replace(array('IOS:', 'AND:'), '', $this->Request['api_token']))->get();

		foreach ($Favorites as $Favorite) {
			$Favorite->Match->team_home_id = \CMS\ProfileTeam::where('id', $Favorite->Match->team_home_id)->first();
			$Favorite->Match->team_away_id = \CMS\ProfileTeam::where('id', $Favorite->Match->team_away_id)->first();
			// $Favorite->match               = $Favorite->Match;
		}

		$collection = collect($Favorites->toArray());
		$Data       = $collection->sortBy(function ($collects, $key) {
				return $collects['match']['start_date_time'];
			});
		foreach ($Data as $key => $value) {
			$datas[] = $value;
		}

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function add_favorite() {
		$datas  = array();
		$status = array();

		session()->put('sess_match_id', $this->Request['match_id']);

		\CMS\MatchFavorite::firstOrCreate(array(
				'email'    => str_replace(array('IOS:', 'AND:'), '', $this->Request['api_token']),
				'match_id' => $this->Request['match_id'],
			));

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function delete_favorite() {
		$datas  = array();
		$status = array();

		session()->forget('sess_match_id');

		\CMS\MatchFavorite::where('email', str_replace(array('IOS:', 'AND:'), '', $this->Request['api_token']))
		                                                                               ->where('match_id', $this->Request['match_id'])->delete();

		return $this->response(__FUNCTION__, $status, $datas);
	}

	public function group_stage() {
		$datas       = array();
		$status      = array();
		$RoundRobins = array();
		foreach (\CMS\RoundRobin::orderBy('name', 'desc')->get() as $RoundRobin) {
			$RoundRobin->ProfileTeam;
			$RoundRobins[] = $RoundRobin;
		}

		return $this->response(__FUNCTION__, $status, $datas = $RoundRobins);
	}

	public function standings() {
		return $this->team();
	}

	public function top_scores() {
		$datas  = array();
		$status = array();

		$MatchPlayerStandings = \CMS\MatchPlayerStanding::where('year', date('Y'))
			->select('profile_player_id', 'year', 'tournament', 'profile_team_id')
			->where('action', 'GL')
			->groupBy('year')
			->groupBy('tournament')
			->groupBy('profile_player_id')
			->get();

		foreach ($MatchPlayerStandings as $MatchPlayerStanding) {
			$MatchPlayerStanding->player_name        = $MatchPlayerStanding->player()->first()->name;
			$MatchPlayerStanding->player_nationality = config('fb.countries.'.$MatchPlayerStanding->player()->first()->nationality);
			$MatchPlayerStanding->team_name          = $MatchPlayerStanding->team()->first()->name;
			$MatchPlayerStanding->goal_scored        = \CMS\MatchPlayerStanding::where('action', 'GL')->where('profile_player_id', $MatchPlayerStanding->profile_player_id)->where('tournament', $MatchPlayerStanding->tournament)->where('year', $MatchPlayerStanding->year)->count().'';
		}

		$collection = collect($MatchPlayerStandings->toArray());
		$Data       = $collection->sortByDesc(function ($collects, $key) {
				return $collects['goal_scored'];
			});
		foreach ($Data as $key => $value) {
			$datas[$key] = $value;
		}
		sort($datas);

		return $this->response(__FUNCTION__, $status, $datas);
	}

}