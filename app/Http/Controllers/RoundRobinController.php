<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Misc\ShiftingTrait;

use CMS\ProfileTeam;
use CMS\RoundRobin;
use Illuminate\Http\Request;

class RoundRobinController extends Controller {
	use ShiftingTrait;

	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {

		$Teams_name = array();
		foreach (ProfileTeam::orderBy('name', 'desc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->name] = $ProfileTeam->name;
		}
		$RoundRobins = RoundRobin::search()->sort()->orderBy('name', 'desc')->paginate(25);
		return view('round_robin.index')->with(compact('RoundRobins', 'Teams_name'));
	}

	public function create() {
		$Teams_name = array();
		foreach (ProfileTeam::orderBy('name', 'desc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->id] = $ProfileTeam->name;
		}
		return view('round_robin.create')->with(compact('Teams_name'));
	}

	public function store(Request $request) {

		$RoundRobins = RoundRobin::create(
			array(
				'seq'             => RoundRobin::max('seq')+1,
				'name'            => $request->get('name'),
				'year'            => $request->get('year'),
				'tournament'      => 'MYC',
				'tournament_type' => 'Cup',
				'profile_team_id' => $request->get('team'),
			)
		);

		return redirect()->route('round_robin')->with('success', 'Record created.');
	}

	public function edit($id) {
		$Teams_name = array();
		foreach (ProfileTeam::orderBy('name', 'desc')->get() as $ProfileTeam) {
			$Teams_name[$ProfileTeam->id] = $ProfileTeam->name;
		}
		$RoundRobin = RoundRobin::find($id);
		return view('round_robin.edit')->with(compact('RoundRobin', 'Teams_name'));
	}

	public function update(Request $request, $id) {

		$RoundRobins                  = RoundRobin::find($id);
		$RoundRobins->name            = $request->get('name', $RoundRobins->name);
		$RoundRobins->year            = $request->get('year', $RoundRobins->year);
		$RoundRobins->profile_team_id = $request->get('team', $RoundRobins->team);
		$RoundRobins->save();

		return redirect()->route('round_robin')->with('success', 'Record Updated.');
	}

	public function destroy($id) {
		session()->flash('success', 'Record deleted.');
		return RoundRobin::destroy($id);
	}

}
