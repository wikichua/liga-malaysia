<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Misc\ShiftingTrait;

use CMS\Http\Requests\TeamGalleryRequest;
use CMS\ProfileTeam;
use CMS\TeamGallery;

class TeamGalleriesController extends Controller {
	use ShiftingTrait;

	public function __construct() {
		$this->middleware('auth');
	}

	public function index($team_id) {
		$this->reorganizeSEQ(TeamGallery::where('profile_team_id', $team_id));
		$TeamGallerys = TeamGallery::where('profile_team_id', $team_id)->search()->sort()->orderBy('seq', 'desc')->paginate(25);
		$this->index_shift($TeamGallerys, TeamGallery::where('profile_team_id', $team_id));
		$ProfileTeam = ProfileTeam::find($team_id);
		return view('team_galleries.index')->with(compact('TeamGallerys', 'team_id', 'ProfileTeam'));
	}

	public function create($team_id) {

		return view('team_galleries.create')->with(compact('team_id'));
	}

	public function store(TeamGalleryRequest $request, $team_id) {
		$TeamGallery = TeamGallery::create(
			array(
				'seq'             => TeamGallery::where('profile_team_id', $team_id)->max('seq')+1,
				'profile_team_id' => $team_id,
				'name'            => $request->get('name'),
				'image'           => uploadImage('image', '', 'profile-team-gllery'),
			)
		);

		return redirect()->route('team_galleries', $team_id)->with('success', 'Record created.');
	}

	public function edit($team_id, $id) {
		$TeamGallery = TeamGallery::find($id);
		return view('team_galleries.edit')->with(compact('team_id', 'TeamGallery'));
	}

	public function update(TeamGalleryRequest $request, $team_id, $id) {
		$TeamGallery        = TeamGallery::find($id);
		$TeamGallery->image = uploadImage('image', $TeamGallery->image, 'profile-team-gllery');
		$TeamGallery->name  = $request->get('name', $TeamGallery->name);
		$TeamGallery->save();

		return redirect()->route('team_galleries', $team_id)->with('success', 'Record Updated.');
	}

	public function destroy($team_id, $id) {
		session()->flash('success', 'Record deleted.');
		return TeamGallery::destroy($id);
	}

	public function shift($team_id, $id, $shift_id) {
		$this->shifting(TeamGallery::where('profile_team_id', $team_id), $id, $shift_id);

		return back();
	}
}