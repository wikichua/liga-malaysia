<?php

namespace CMS\Http\Controllers;

use CMS\Http\Controllers\Controller;

use CMS\Http\Requests\NewsRequest;
use CMS\News;

class NewsController extends Controller {
	public function __construct() {
		$this->middleware('auth');
	}

	public function index() {
		$tournaments = array();
		foreach (config('fb.tournament') as $tour) {
			$tournaments = array_merge($tournaments, $tour);
		}
		$Newses = News::search()->sort()->orderBy('published_at', 'desc')->paginate(25);
		return view('news.index')->with(compact('Newses', 'tournaments'));
	}

	public function create() {
		return view('news.create');
	}

	public function store(NewsRequest $request) {
		$News = News::create(
			array(
				'seq'          => News::max('seq')+1,
				'name'         => $request->get('name'),
				'image'        => uploadImage('image', '', 'news'),
				'description'  => $request->get('description'),
				'published_at' => $request->get('published_at'),
				'tournament'   => $request->get('tournament'),
			)
		);

		return redirect()->route('news')->with('success', 'Record created.');
	}

	public function edit($id) {
		$News = News::find($id);
		return view('news.edit')->with(compact('News'));
	}

	public function update(NewsRequest $request, $id) {
		$News               = News::find($id);
		$News->name         = $request->get('name', $News->name);
		$News->image        = uploadImage('image', $News->image, 'news');
		$News->description  = $request->get('description', $News->description);
		$News->published_at = $request->get('published_at', $News->published_at);
		$News->tournament   = $request->get('tournament', $News->tournament);
		$News->save();

		return redirect()->route('news')->with('success', 'Record Updated.');
	}

	public function destroy($id) {
		session()->flash('success', 'Record deleted.');
		return News::destroy($id);
	}
}