<?php
Route::group(['middleware'                                     => ['web']], function () {
		Route::get('/', array('as'                                   => 'auth', 'uses'                                   => 'AuthController@index'));
		Route::get('/change_password_email', array('as'              => 'change_password_email', 'uses'              => 'AuthController@change_password_email'));
		Route::put('/send_email', array('as'                         => 'send_email', 'uses'                         => 'AuthController@send_email'));
		Route::get('/change_password/{email}/{password}', array('as' => 'change_password', 'uses' => 'AuthController@change_password'));
		Route::post('/login', array('as'                             => 'login', 'uses'                             => 'AuthController@login'));
		Route::get('/logout', array('as'                             => 'logout', 'uses'                             => 'AuthController@logout'));
	});

Route::group(['middleware' => ['web', 'auth']], function () {

		Route::get('/dashboard', array('as'      => 'dashboard', 'uses'      => 'DashboardController@index'));
		Route::get('/user', array('as'           => 'user', 'uses'           => 'UserController@index'));
		Route::get('/profile', array('as'        => 'profile', 'uses'        => 'AuthController@profile'));
		Route::put('/profile/update', array('as' => 'profile.update', 'uses' => 'AuthController@profile_update'));

		Route::group(array('prefix' => 'setting'), function () {

				Route::group(array('prefix'          => 'audit'), function () {
						Route::get('', array('as'          => 'audit_trail', 'uses'          => 'AuditTrailController@index'));
						Route::get('{id}/show', array('as' => 'audit_trail.show', 'uses' => 'AuditTrailController@show'));
					});

				Route::group(array('prefix'                => 'user'), function () {
						Route::get('', array('as'                => 'user', 'uses'                => 'UserController@index'));
						Route::get('create', array('as'          => 'user.create', 'uses'          => 'UserController@create'));
						Route::post('store', array('as'          => 'user.store', 'uses'          => 'UserController@store'));
						Route::get('{id}/edit', array('as'       => 'user.edit', 'uses'       => 'UserController@edit'));
						Route::put('{id}/update', array('as'     => 'user.update', 'uses'     => 'UserController@update'));
						Route::delete('{id}/destroy', array('as' => 'user.destroy', 'uses' => 'UserController@destroy'));
						Route::get('{id}/switch', array('as'     => 'user.switch', 'uses'     => 'UserController@switch_user'));
					});

				Route::group(array('prefix'                => 'usergroup'), function () {
						Route::get('', array('as'                => 'usergroup', 'uses'                => 'UserGroupController@index'));
						Route::get('create', array('as'          => 'usergroup.create', 'uses'          => 'UserGroupController@create'));
						Route::post('store', array('as'          => 'usergroup.store', 'uses'          => 'UserGroupController@store'));
						Route::get('{id}/edit', array('as'       => 'usergroup.edit', 'uses'       => 'UserGroupController@edit'));
						Route::put('{id}/update', array('as'     => 'usergroup.update', 'uses'     => 'UserGroupController@update'));
						Route::delete('{id}/destroy', array('as' => 'usergroup.destroy', 'uses' => 'UserGroupController@destroy'));

						Route::group(array('prefix'       => 'usergroup/acl/{usergroup_id}'), function () {
								Route::get('', array('as'       => 'acl', 'uses'       => 'ACLController@index'));
								Route::get('create', array('as' => 'acl.create', 'uses' => 'ACLController@create'));
								Route::post('store', array('as' => 'acl.store', 'uses' => 'ACLController@store'));
							});
					});

				Route::group(array('prefix'                => '/app/version'), function () {
						Route::get('', array('as'                => 'app.version', 'uses'                => 'AppVersionController@index'));
						Route::get('create', array('as'          => 'app.version.create', 'uses'          => 'AppVersionController@create'));
						Route::post('store', array('as'          => 'app.version.store', 'uses'          => 'AppVersionController@store'));
						Route::get('{id}/edit', array('as'       => 'app.version.edit', 'uses'       => 'AppVersionController@edit'));
						Route::put('{id}/update', array('as'     => 'app.version.update', 'uses'     => 'AppVersionController@update'));
						Route::delete('{id}/destroy', array('as' => 'app.version.destroy', 'uses' => 'AppVersionController@destroy'));
					});

			});

		Route::group(array('prefix' => 'team'), function () {

				Route::get('', array('as'                 => 'profile_team', 'uses'                 => 'ProfileTeamController@index'));
				Route::get('/create', array('as'          => 'profile_team.create', 'uses'          => 'ProfileTeamController@create'));
				Route::post('/store', array('as'          => 'profile_team.store', 'uses'          => 'ProfileTeamController@store'));
				Route::get('/{id}/edit', array('as'       => 'profile_team.edit', 'uses'       => 'ProfileTeamController@edit'));
				Route::put('/{id}/update', array('as'     => 'profile_team.update', 'uses'     => 'ProfileTeamController@update'));
				Route::delete('/{id}/destroy', array('as' => 'profile_team.destroy', 'uses' => 'ProfileTeamController@destroy'));

				Route::group(array('prefix' => '{team_id}/player'), function () {

						Route::get('', array('as'                 => 'profile_players', 'uses'                 => 'ProfilePlayerController@index'));
						Route::get('/create', array('as'          => 'profile_players.create', 'uses'          => 'ProfilePlayerController@create'));
						Route::post('/store', array('as'          => 'profile_players.store', 'uses'          => 'ProfilePlayerController@store'));
						Route::get('/{id}/edit', array('as'       => 'profile_players.edit', 'uses'       => 'ProfilePlayerController@edit'));
						Route::put('/{id}/update', array('as'     => 'profile_players.update', 'uses'     => 'ProfilePlayerController@update'));
						Route::delete('/{id}/destroy', array('as' => 'profile_players.destroy', 'uses' => 'ProfilePlayerController@destroy'));

					});

				Route::group(array('prefix' => '{team_id}/honour'), function () {

						Route::get('', array('as'                       => 'honour', 'uses'                       => 'HonourController@index'));
						Route::get('create', array('as'                 => 'honour.create', 'uses'                 => 'HonourController@create'));
						Route::post('store', array('as'                 => 'honour.store', 'uses'                 => 'HonourController@store'));
						Route::get('{id}/edit', array('as'              => 'honour.edit', 'uses'              => 'HonourController@edit'));
						Route::put('{id}/update', array('as'            => 'honour.update', 'uses'            => 'HonourController@update'));
						Route::delete('{id}/destroy', array('as'        => 'honour.destroy', 'uses'        => 'HonourController@destroy'));
						Route::get('/{id}/shift/{shift_id}', array('as' => 'honour.shift', 'uses' => 'HonourController@shift'));

					});

				Route::group(array('prefix'                       => '{team_id}/gallery'), function () {
						Route::get('', array('as'                       => 'team_galleries', 'uses'                       => 'TeamGalleriesController@index'));
						Route::get('/create', array('as'                => 'team_galleries.create', 'uses'                => 'TeamGalleriesController@create'));
						Route::post('/store', array('as'                => 'team_galleries.store', 'uses'                => 'TeamGalleriesController@store'));
						Route::get('/{id}/edit', array('as'             => 'team_galleries.edit', 'uses'             => 'TeamGalleriesController@edit'));
						Route::put('/{id}/update', array('as'           => 'team_galleries.update', 'uses'           => 'TeamGalleriesController@update'));
						Route::delete('/{id}/destroy', array('as'       => 'team_galleries.destroy', 'uses'       => 'TeamGalleriesController@destroy'));
						Route::get('/{id}/shift/{shift_id}', array('as' => 'team_galleries.shift', 'uses' => 'TeamGalleriesController@shift'));

					});

			});

		Route::group(array('prefix' => 'news'), function () {

				Route::get('', array('as'                 => 'news', 'uses'                 => 'NewsController@index'));
				Route::get('/create', array('as'          => 'news.create', 'uses'          => 'NewsController@create'));
				Route::post('/store', array('as'          => 'news.store', 'uses'          => 'NewsController@store'));
				Route::get('/{id}/edit', array('as'       => 'news.edit', 'uses'       => 'NewsController@edit'));
				Route::put('/{id}/update', array('as'     => 'news.update', 'uses'     => 'NewsController@update'));
				Route::delete('/{id}/destroy', array('as' => 'news.destroy', 'uses' => 'NewsController@destroy'));

			});

		Route::group(array('prefix' => 'video'), function () {

				Route::get('', array('as'                 => 'video', 'uses'                 => 'VideoController@index'));
				Route::get('/create', array('as'          => 'video.create', 'uses'          => 'VideoController@create'));
				Route::post('/store', array('as'          => 'video.store', 'uses'          => 'VideoController@store'));
				Route::get('/{id}/edit', array('as'       => 'video.edit', 'uses'       => 'VideoController@edit'));
				Route::put('/{id}/update', array('as'     => 'video.update', 'uses'     => 'VideoController@update'));
				Route::delete('/{id}/destroy', array('as' => 'video.destroy', 'uses' => 'VideoController@destroy'));

			});

		Route::group(array('prefix' => 'advert'), function () {

				Route::get('', array('as'                 => 'advert', 'uses'                 => 'AdvertController@index'));
				Route::get('/create', array('as'          => 'advert.create', 'uses'          => 'AdvertController@create'));
				Route::post('/store', array('as'          => 'advert.store', 'uses'          => 'AdvertController@store'));
				Route::get('/{id}/edit', array('as'       => 'advert.edit', 'uses'       => 'AdvertController@edit'));
				Route::put('/{id}/update', array('as'     => 'advert.update', 'uses'     => 'AdvertController@update'));
				Route::delete('/{id}/destroy', array('as' => 'advert.destroy', 'uses' => 'AdvertController@destroy'));

			});

		Route::group(array('prefix' => 'match'), function () {

				Route::get('', array('as'                                 => 'match', 'uses'                                 => 'MatchController@index'));
				Route::get('/create', array('as'                          => 'match.create', 'uses'                          => 'MatchController@create'));
				Route::post('/store', array('as'                          => 'match.store', 'uses'                          => 'MatchController@store'));
				Route::get('/{id}/edit', array('as'                       => 'match.edit', 'uses'                       => 'MatchController@edit'));
				Route::get('/{id}/postpond', array('as'                   => 'match.postpond', 'uses'                   => 'MatchController@postpond'));
				Route::put('/{id}/update', array('as'                     => 'match.update', 'uses'                     => 'MatchController@update'));
				Route::get('/{id}/players_in_matches', array('as'         => 'match.players_in_matches', 'uses'         => 'MatchController@players_in_matches'));
				Route::post('/{id}/players_in_matches_create', array('as' => 'match.players_in_matches_create', 'uses' => 'MatchController@players_in_matches_create'));
				Route::delete('/{id}/destroy', array('as'                 => 'match.destroy', 'uses'                 => 'MatchController@destroy'));
				Route::get('tbc', array('as'                              => 'match.tbc', 'uses'                              => 'MatchController@tbc'));

				Route::group(array('prefix'               => '{match_id}/score'), function () {
						Route::get('', array('as'               => 'standing.edit', 'uses'               => 'StandingController@edit'));
						Route::get('penalty_edit', array('as'   => 'standing.penalty_edit', 'uses'   => 'StandingController@penalty_edit'));
						Route::put('update', array('as'         => 'standing.update', 'uses'         => 'StandingController@update'));
						Route::put('penalty_update', array('as' => 'standing.penalty_update', 'uses' => 'StandingController@penalty_update'));

						Route::put('end', array('as'              => 'standing.end', 'uses'              => 'StandingController@end'));
						Route::delete('/{id}/destroy', array('as' => 'standing.destroy', 'uses' => 'StandingController@destroy'));

					});

				Route::group(array('prefix' => 'group/stages'), function () {

						Route::get('', array('as'                 => 'round_robin', 'uses'                 => 'RoundRobinController@index'));
						Route::get('/create', array('as'          => 'round_robin.create', 'uses'          => 'RoundRobinController@create'));
						Route::post('/store', array('as'          => 'round_robin.store', 'uses'          => 'RoundRobinController@store'));
						Route::get('/{id}/edit', array('as'       => 'round_robin.edit', 'uses'       => 'RoundRobinController@edit'));
						Route::put('/{id}/update', array('as'     => 'round_robin.update', 'uses'     => 'RoundRobinController@update'));
						Route::delete('/{id}/destroy', array('as' => 'round_robin.destroy', 'uses' => 'RoundRobinController@destroy'));

					});

			});

		Route::get('/test', function () {
				$UserDevices = \CMS\UserDevice::where('push_token', '!=', '')->get();

				(new \CMS\Http\Misc\PushNotification(array(
							'title'       => 'Testing',
							'UserDevices' => $UserDevices,
							'message'     => 'Testing Convep',
							'env'         => env('PUSH_ENV', 'production'),
							'module'      => 'Match OnGoing',
							'id'          => 0,
							'custom_data' => '',
						)))->push();
				// $Ms = \CMS\Match::all();
				// foreach ($Ms as $M) {
				// 	\CMS\MatchTeamStanding::where('match_id', $M->id)->update(array('round' => $M->round));
				// }
				return 'Develop By Convep Mobilogy';
			});

	});

Route::group(['middleware'                    => ['web'], 'prefix'                    => 'api'], function () {
		Route::get('test/{api}', array('as'         => 'api', 'uses'         => 'APIController@index'));
		Route::any('/pt', array('as'                => 'api.pt', 'uses'                => 'APIController@pt'));
		Route::any('/news', array('as'              => 'api.news', 'uses'              => 'APIController@news'));
		Route::any('/video', array('as'             => 'api.video', 'uses'             => 'APIController@video'));
		Route::any('/team', array('as'              => 'api.team', 'uses'              => 'APIController@team'));
		Route::any('/advert', array('as'            => 'api.advert', 'uses'            => 'APIController@advert'));
		Route::any('/matches', array('as'           => 'api.matches', 'uses'           => 'APIController@matches'));
		Route::any('/match_fixture', array('as'     => 'api.match_fixture', 'uses'     => 'APIController@match_fixture'));
		Route::any('/match_ongoing', array('as'     => 'api.match_ongoing', 'uses'     => 'APIController@match_ongoing'));
		Route::any('/match_result', array('as'      => 'api.match_result', 'uses'      => 'APIController@match_result'));
		Route::any('/group_standings', array('as'   => 'api.group_standings', 'uses'   => 'APIController@group_standings'));
		Route::any('/list_favorite', array('as'     => 'api.list_favorite', 'uses'     => 'APIController@list_favorite'));
		Route::any('/add_favorite', array('as'      => 'api.add_favorite', 'uses'      => 'APIController@add_favorite'));
		Route::any('/delete_favorite', array('as'   => 'api.delete_favorite', 'uses'   => 'APIController@delete_favorite'));
		Route::any('/group_stage', array('as'       => 'api.group_stage', 'uses'       => 'APIController@group_stage'));
		Route::any('/standings', array('as'         => 'api.standings', 'uses'         => 'APIController@standings'));
		Route::any('/players_standings', array('as' => 'api.players_standings', 'uses' => 'APIController@players_standings'));
		Route::any('/top_scores', array('as'        => 'api.top_scores', 'uses'        => 'APIController@top_scores'));
	});
