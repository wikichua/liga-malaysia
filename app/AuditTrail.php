<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;

class AuditTrail extends Model
{
    use SortableTrait;
    use SearchableTrait;

    protected $table = 'audit_trails';
    protected $guarded = [];

    public function User()
    {
        return $this->belongsTo('CMS\User','user_id','id')->withTrashed();
    }
}
