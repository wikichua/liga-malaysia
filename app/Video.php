<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Http\Misc\GlobalTrait;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\AuditTrailTrait;

class Video extends Model
{
	use SoftDeletes, GlobalTrait, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'videos';
    protected $guarded = [];
    protected $dates = ['deleted_at','published_at'];

}
