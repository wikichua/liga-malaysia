<?php

namespace CMS\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        // Commands\Inspire::class,
        Commands\AutoStartMatch::class,
        Commands\AutoEndMatch::class,
        Commands\AutoAvertExpires::class,
        Commands\MatchStartOn1hr::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('football:autostartmatch')->everyMinute();
        $schedule->command('football:autoendmatch')->everyMinute();
        $schedule->command('football:autoavertexpired')->everyMinute();
        $schedule->command('football:matchstarton1hr')->everyMinute();
    }
}
