<?php

namespace CMS\Console\Commands;

use Illuminate\Console\Command;
use CMS\Advert;
use Carbon\Carbon;


class AutoAvertExpires extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'football:autoavertexpired';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $Adverts = Advert::where('expired_at','<=',Carbon::now())->where('status','Published')->get();

        foreach ($Adverts as $Advert) {
            $Advert->status = 'Draft';
            $Advert->save();
        }

    }
}
