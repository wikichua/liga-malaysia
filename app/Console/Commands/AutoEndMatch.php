<?php

namespace CMS\Console\Commands;

use Illuminate\Console\Command;
use CMS\Http\Controllers\MatchController;

class AutoEndMatch extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'football:autoendmatch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto End Match';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        (new MatchController(true))->scanMatchesToEndMatch();
    }
}
