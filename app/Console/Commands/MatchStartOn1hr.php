<?php

namespace CMS\Console\Commands;

use Carbon\Carbon;
use CMS\Http\Misc\PushNotification;
use CMS\Match;
use CMS\MatchFavorite;
use CMS\UserDevice;

use Illuminate\Console\Command;

class MatchStartOn1hr extends Command {
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'football:matchstarton1hr';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Match Start On 1hr';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct() {
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle() {
		$Matches = Match::where('state', 'Fixture')->where('start_date_time', '<>', '00-00-0000 00:00:00')->where('start_date_time', '<=', Carbon::now()->addHours(1))->where('pn_flag', '!=', 1)->get();
		foreach ($Matches as $key => $Match) {
			$emails      = MatchFavorite::where('match_id', $Match->id)->lists('email');
			$UserDevices = UserDevice::whereIn('imei', $emails)->where('push_token', '!=', '')->get();

			(new PushNotification(array(
						'title'       => 'Up Coming Match',
						'UserDevices' => $UserDevices,
						'message'     => 'Match between '.$Match->ProfileTeamHome()->first()->name.' VS '.$Match->ProfileTeamAway()->first()->name.' is about to start.',
						'env'         => env('PUSH_ENV', 'production'),
						'module'      => 'Match Fixture',
						'id'          => $Match->id,
						'custom_data' => array('tournament' => $Match->tournament),
					)))                                           ->push();

			$Match->pn_flag = 1;
			$Match->save();
		}

	}
}
