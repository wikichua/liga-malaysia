<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\AuditTrailTrait;

class AppVersion extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'app_versions';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

}
