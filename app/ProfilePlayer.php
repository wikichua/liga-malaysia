<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Http\Misc\GlobalTrait;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\AuditTrailTrait;

class ProfilePlayer extends Model
{
	use SoftDeletes, SortableTrait,GlobalTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'profile_players';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function Team()
    {
        return $this->belongsTo('CMS\ProfileTeam','profile_team_id','id')->withTrashed();
    }
}
