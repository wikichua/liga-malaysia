<?php

namespace CMS;

use CMS\Http\Misc\AuditTrailTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\SortableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchPlayerStanding extends Model {
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

	protected $table   = 'match_player_standing';
	protected $guarded = [];
	protected $dates   = ['deleted_at'];

	public function match() {
		return $this->belongsTo('CMS\Match', 'match_id', 'id')->withTrashed();
	}

	public function player() {
		return $this->belongsTo('CMS\ProfilePlayer', 'profile_player_id', 'id')->withTrashed();
	}

	public function team() {
		return $this->belongsTo('CMS\ProfileTeam', 'profile_team_id', 'id')->withTrashed();
	}

	public function home_team() {
		return $this->belongsTo('CMS\Match', 'profile_team_id', 'team_home_id')->withTrashed();
	}

	public function away_team() {
		return $this->belongsTo('CMS\Match', 'profile_team_id', 'team_away_id')->withTrashed();
	}
}
