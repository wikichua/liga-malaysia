<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PushRecord extends Model
{
    use SoftDeletes;

    protected $table = 'push_records';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function UserDevice()
    {
        return $this->belongsTo('CMS\UserDevice','device_id','id')->withTrashed();
    }
}
