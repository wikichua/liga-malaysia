<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserDevice extends Model
{
    use SoftDeletes;

    protected $table = 'user_devices';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function User()
    {
        return $this->belongsTo('CMS\User','user_id','id')->withTrashed();
    }
}
