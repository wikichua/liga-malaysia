<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\AuditTrailTrait;

class RoundRobin extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'round_robins';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

public function ProfileTeam()
    {
        return $this->belongsTo('CMS\ProfileTeam','profile_team_id','id')->withTrashed();
    }
}

