<?php

namespace CMS;

use CMS\Http\Misc\AuditTrailTrait;
use CMS\Http\Misc\GlobalTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\SortableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Match extends Model {
	use SoftDeletes, GlobalTrait, SortableTrait, SearchableTrait, AuditTrailTrait;

	protected $table   = 'match';
	protected $guarded = [];
	protected $dates   = ['deleted_at', 'start_date_time', 'end_date_time', 'postponed_at'];

	public function ProfileTeamHome() {
		return $this->belongsTo('CMS\ProfileTeam', 'team_home_id', 'id')->withTrashed();
	}
	public function ProfileTeamAway() {
		return $this->belongsTo('CMS\ProfileTeam', 'team_away_id', 'id')->withTrashed();
	}

	public function setStartDateTimeAttribute($value) {
		$this->attributes['start_date_time'] = date('Y-m-d H:i:00', strtotime($value));
	}

	public function getStartDateTimeAttribute($value) {
		return (new \Carbon\Carbon($value))->format('d-m-Y H:i:00');
	}

	public function setEndDateTimeAttribute($value) {
		$this->attributes['end_date_time'] = date('Y-m-d H:i:00', strtotime($value));
	}

	public function getEndDateTimeAttribute($value) {
		return (new \Carbon\Carbon($value))->format('d-m-Y H:i:00');
	}

	public function setPostponedAtAttribute($value) {
		$this->attributes['postponed_at'] = date('Y-m-d H:i:s', strtotime($value));
	}

	public function getPostponedAtAttribute($value) {
		return (new \Carbon\Carbon($value))->format('d-m-Y H:i:s');
	}

}
