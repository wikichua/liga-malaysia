<?php

namespace CMS;

use CMS\Http\Misc\AuditTrailTrait;
use CMS\Http\Misc\GlobalTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\SortableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProfileTeam extends Model {
	use SoftDeletes, SortableTrait, GlobalTrait, SearchableTrait, AuditTrailTrait;

	protected $table   = 'profile_teams';
	protected $guarded = [];
	protected $dates   = ['deleted_at'];

	public function ProfilePlayer() {
		return $this->HasMany('CMS\ProfilePlayer', 'profile_team_id', 'id');
	}

	public function TeamGallery() {
		return $this->HasMany('CMS\TeamGallery', 'profile_team_id', 'id');
	}

	public function Honour() {
		return $this->HasMany('CMS\Honour', 'profile_team_id', 'id');
	}

	public function setTournamentsAttribute($value) {
		if (!is_array($value)) {
			if (json_decode($value)) {
				$value = json_decode($value, true);
			} else {
				$value = array();
			}
		}

		$this->attributes['tournaments'] = json_encode($value);
	}

	public function getTournamentsAttribute($value) {
		return json_decode($value, true);
	}
}
