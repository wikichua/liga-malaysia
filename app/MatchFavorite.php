<?php

namespace CMS;

use CMS\Http\Misc\AuditTrailTrait;
use CMS\Http\Misc\GlobalTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\SortableTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MatchFavorite extends Model {
	use SoftDeletes, GlobalTrait, SortableTrait, SearchableTrait, AuditTrailTrait;

	protected $table   = 'match_favorite';
	protected $guarded = [];
	protected $dates   = ['deleted_at'];

	public function Match() {
		return $this->hasOne('CMS\Match', 'id', 'match_id');
	}

	public function DeviceUser() {
		return $this->hasOne('CMS\UserDevice', 'imei', 'email');
	}
}
