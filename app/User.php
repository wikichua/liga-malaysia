<?php

namespace CMS;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\AuditTrailTrait;

class User extends Authenticatable
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'users';
    protected $guarded = [];
    protected $hidden = ['password', 'remember_token'];
    protected $dates = ['deleted_at'];

    public function usergroup()
    {
        return $this->belongsTo('CMS\UserGroup','usergroup_id','id')->withTrashed();
    }
}
