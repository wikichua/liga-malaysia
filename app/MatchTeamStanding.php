<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\AuditTrailTrait;

class MatchTeamStanding extends Model
{
	use SoftDeletes, SortableTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'match_team_standing';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

    public function match()
    {
    	return $this->belongsTo('CMS\Match','match_id','id')->withTrashed();
    }

    public function profile_team()
    {
    	return $this->belongsTo('CMS\ProfileTeam','profile_team_id','id')->withTrashed();
    }

}
