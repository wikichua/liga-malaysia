<?php

namespace CMS;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use CMS\Http\Misc\GlobalTrait;
use CMS\Http\Misc\SortableTrait;
use CMS\Http\Misc\SearchableTrait;
use CMS\Http\Misc\AuditTrailTrait;

class TeamGallery extends Model
{
	use SoftDeletes, SortableTrait,GlobalTrait, SearchableTrait, AuditTrailTrait;

    protected $table = 'team_galleries';
    protected $guarded = [];
    protected $dates = ['deleted_at'];

}
