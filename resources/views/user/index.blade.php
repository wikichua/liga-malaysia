@extends('template')

@section('title')
	 Users
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
	    <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-user'></i> User</h1>
	        @if (ACLButtonCheck('USR_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('user.create')) !!}</span>
	        @endif
	    </div>

    	<div class="row x_content">
    		<table class="table table-striped table-responsive table-hover">
				<tr>
					<th>Picture</th>
					<th>{!! sortTableHeaderSnippet('Name ','name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Group ','usergroup-name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Email ','email') !!}</th>
					<th>{!! sortTableHeaderSnippet('Status','status') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th></th>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! searchTableHeaderSnippet('usergroup-name') !!}</th>
					<th>{!! searchTableHeaderSnippet('email') !!}</th>
					<th>{!! searchTableHeaderSnippet('status') !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($Users as $User)
				<tr>
					<td class="col-sm-1"><img src="{{ asset('uploads/'.imgTagShow($User->photo,'profile')) }}" class="img-thumbnail"></td>
					<td>{{ $User->name }}</td>
					<td>{{ $User->usergroup->name or 'N/A' }}</td>
					<td>{{ $User->email }}</td>
					<td>{{ $User->status }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('USR_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('user.edit',array($User->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (auth()->user()->isAdmin && !session()->has('original_user_id'))
							<a class="btn btn-info" href="{{ route('user.switch',array($User->id)) }}" data-toggle='tooltip' title='Switch User'><i class="fa fa-random"></i></a>
							@endif
							@if (ACLButtonCheck('USR_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('user.destroy',array($User->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
			</table>
			<div class="text-center">
			{!! str_replace('/?', '?', $Users->appends(request()->all())->render()) !!}
			</div>	

            <div class="clearfix"></div>
        </div>
    </div>
</div>
@stop