@extends('template')

@section('title')
	Users
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
    	<div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-user'></i> Create User</h1>
	    </div>

        <div class="row x_content">
            {!! Form::open(array('route' => 'user.store','name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('photo', 'Photo', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
<?php $photo = 'no-profile.gif';?>
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow('','default')) }}" class="img-thumbnail previewImg">
					{!! Form::file('photo', array('class'=>"form-control")) !!}
					<p class="text-danger">Image file allow: JPEG, JPG, PNG</p>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Full Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('name', old('name'), array('class'=>"form-control ",'placeholder'=>"Full Name")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('email', 'Email', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('email', old('email'), array('class'=>"form-control",'placeholder'=>"Email")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('password', 'Password', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::password('password', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('password_confirmation', 'Confirm Password', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::password('password_confirmation', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('usergroup_id', 'User Group', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('usergroup_id',$usergroups,old('usergroup_id'), array('class'=>"form-control",'placeholder'=>'Please select')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('status', 'Status', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('status',array('Active'=>'Active','Inactive'=>'Inactive'),old('status'), array('class'=>'form-control input-checkbox')) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('user') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>

    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#photo").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop