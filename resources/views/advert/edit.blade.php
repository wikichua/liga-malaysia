@extends('template')

@section('title')
	Advert
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit Advert</h1>
	    </div>
             <div class="row x_content">
            {!! Form::open(array('route' => array('advert.update',$Advert->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($Advert->image,'default')) }}" class="img-thumbnail previewImg">
					{!! Form::file('image', array('class'=>"form-control")) !!}
					<p class="text-danger">Image resolution: 640px x 100px<br>Image file allow: JPEG, JPG, PNG</p>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('name', old('name',$Advert->name), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('tournaments', 'Tournaments', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('tournaments[]',config('fb.tournament'), old('tournaments',json_decode($Advert->tournaments,true)), array('class'=>"form-control select2",'multiple'=>'multiple')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('published_at', 'Publish Date', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('published_at', old('published_at',date('Y-m-d',strtotime($Advert->published_at))), array('class'=>"form-control datepicker", $Advert->status == 'Published'? 'readonly':'')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('expired_at', 'Expiry Date', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('expired_at', old('expired_at',date('Y-m-d',strtotime($Advert->expired_at))), array('class'=>"form-control datepicker")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('weblink_url', 'Weblink url', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('weblink_url', $Advert->weblink_url, array('class'=>"form-control ")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('status', 'Status', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('status',$statuses ,old('status',$Advert->status), array('class'=>"form-control select2")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('advert') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#image").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop