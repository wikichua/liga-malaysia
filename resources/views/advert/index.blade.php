@extends('template')

@section('title')
	Advert
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Advert</h1>
	        @if (ACLButtonCheck('ADV_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('advert.create')) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th>{!! sortTableHeaderSnippet('Image','image') !!}</th>
					<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Publish Date','published_at') !!}</th>
					<th>{!! sortTableHeaderSnippet('Expired Date','expired_at') !!}</th>
					<th>{!! sortTableHeaderSnippet('Status','status') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th></th>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! searchTableHeaderSnippet('published_at', 'date') !!}</th>
					<th>{!! searchTableHeaderSnippet('expired_at','date') !!}</th>
					<th>{!! searchTableHeaderSnippet('status','select',$statuses) !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($Adverts as $Advert)
				<tr>
					<td class="col-sm-1"><img src="{{ asset('uploads/'.imgTagShow($Advert->image,'default')) }}" class="img-thumbnail"></td>
					<td>{{ $Advert->name }}</td>
					<td>{{ $Advert->published_at }}</td>
					<td>{{ $Advert->expired_at }}</td>
					<td>{{ $Advert->status }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('ADV_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('advert.edit',array($Advert->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('ADV_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('advert.destroy',array($Advert->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $Adverts->appends(request()->all())->render()) !!}
				</div>	
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop