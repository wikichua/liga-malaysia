@extends('template')

@section('title')
	Group Stage
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Create Group Stage</h1>
	    </div>



        <div class="row x_content">
            {!! Form::open(array('route' => 'round_robin.store','name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}


			<div class="form-group">
                {!! Form::label('year','Year',array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
                {!! Form::select('year',config('fb.year'),'',array('class'=>'form-control select2')) !!}
                </div>
            </div>

            <div class="form-group">
				{!! Form::label('name', 'Group', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::select('name',array('A'=>'A','B'=>'B','C'=>'C','D'=>'D'), old('select'), array('class'=>"form-control select2")) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('tournament', 'Tournament', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('tournament', 'Malaysia Cup', array('class'=>"form-control",'disabled')) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('team', 'Team', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('team',$Teams_name, old('team'), array('class'=>"form-control select2")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('round_robin') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop