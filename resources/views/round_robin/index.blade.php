@extends('template')

@section('title')
	Group Stage
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Group Stage</h1>
	        @if (ACLButtonCheck('GRPSTGS_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('round_robin.create')) !!}</span>
	        @endif
	        <p class="text-danger">Group Staging is compulsory to fill for Malaysia Cup.</p>
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>

					<th>{!! sortTableHeaderSnippet('Group','name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Year','year') !!}</th>
					<th>{!! sortTableHeaderSnippet('Tournament','tournament') !!}</th>
					<th>{!! sortTableHeaderSnippet('Team','ProfileTeam-name') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th>{!! searchTableHeaderSnippet('name','select',array('A'=>'A','B'=>'B','C'=>'C','D'=>'D')) !!}</th>
					<th>{!! searchTableHeaderSnippet('year','select',config('fb.year')) !!}</th>
					<th>{!! searchTableHeaderSnippet('tournament','select',config('fb.tournament')) !!}</th>
					<th>{!! searchTableHeaderSnippet('ProfileTeam-name','select',$Teams_name) !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($RoundRobins as $RoundRobin)
				<tr>

					<td>{{ $RoundRobin->name }}</td>
					<td>{{ $RoundRobin->year }}</td>
					<td>{{ config('fb.tournament')[$RoundRobin->tournament_type][$RoundRobin->tournament] }}</td>
					<td>{{ $RoundRobin->ProfileTeam->name }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('GRPSTGS_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('round_robin.edit',array($RoundRobin->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('GRPSTGS_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('round_robin.destroy',array($RoundRobin->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $RoundRobins->appends(request()->all())->render()) !!}
				</div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop