@extends('template')

@section('title')
	Honour
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h2 class="x_title"><a href="{{ route('profile_team') }}"><i class="fa fa-arrow-left"></i></a> Honour  ({{ $ProfileTeam->name }})</h2>
	        @if (ACLButtonCheck('HNRS_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('honour.create',$team_id)) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th class="col-sm-1 shift_column"></th>
					<th>{!! sortTableHeaderSnippet('year','Year') !!}</th>
					<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th class="shift_column"></th>
					<th>{!! searchTableHeaderSnippet('year') !!}</th>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($Honours as $Honour)
				<tr>
					<td class="shift_column">
						{!! shifting_buttons($Honour,route('honour.shift',array($team_id,$Honour->id,$Honour->p_id)),route('honour.shift',array($team_id,$Honour->id,$Honour->n_id))) !!}
					</td>
					<td>{{ $Honour->year }}</td>
					<td>{{ config('fb.honours')[$Honour->name] }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">

							
							@if (ACLButtonCheck('HNRS_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('honour.edit',array($team_id,$Honour->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('HNRS_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('honour.destroy',array($team_id,$Honour->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $Honours->appends(request()->all())->render()) !!}
				</div>	
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop