@extends('template')

@section('title')
	Honour
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit Honour</h1>
	    </div>

             <div class="row x_content">
            	{!! Form::open(array('route' => array('honour.update',$team_id, $Honour->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
                {!! Form::label('year','Year',array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
                {!! Form::select('year',config('fb.honour_year'),$Honour->year,array('class'=>'form-control ')) !!}
                </div>
            </div>

			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::select('name',config('fb.honours'),$Honour->name, array('class'=>"form-control",'placeholder'=>"")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('honour',$Honour->profile_team_id) }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop