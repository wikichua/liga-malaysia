@extends('template')

@section('title')
    Access Control
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
            <h1 class="x_title"><i class='fa fa-lock'></i> Update Access Control</h1>
        </div>
        <div class="row x_content">
            {!! Form::open(array('route' => array('acl.store',$usergroup_id),'name' => 'form','class' => 'form-horizontal', 'method' => 'post')) !!}
            <div class="form-group">
			@foreach ($modules as $module_key => $module_name)
				<fieldset class="col-sm-4" style="height:250px;">
					<legend style="font-size:1em; color:#7BE2AE;">{{ $module_name }}</legend>
						<div class="checkbox"><label>{!! Form::checkbox('select_all','','',array('class'=>'checkAll','data-modulekey'=>$module_key)) !!} Select All/Unselect All</label></div>
						@foreach ($roles[$module_key] as $permission)
							<div class="checkbox"><label>{!! Form::checkbox($module_key.'[]', $permission, (isset($acl[$module_key]) && in_array($permission,$acl[$module_key])? true:false),array('class'=>'checkItem-'.$module_key)) !!} {{ $permission }}</label></div>
						@endforeach
				</fieldset>
			@endforeach
			</div>
			<div class="form-group">
				<div class="col-sm-12 text-center">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('acl',$usergroup_id) }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$(".checkAll").change(function () {
		var $moduleKey = $(this).data('modulekey');
	    $(".checkItem-"+$moduleKey+":checkbox").prop('checked', $(this).prop("checked"));
	});
});
</script>
@stop