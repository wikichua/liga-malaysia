@extends('template')

@section('title')
    Access Control
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
            <h1 class="x_title"><i class='fa fa-lock'></i> Access Control</h1>
            @if (ACLButtonCheck('USR_ACL','Create'))
            <span class="pull-right">
                <a href="{{ route('usergroup') }}" class="btn btn-danger btn-sm" data-toggle='tooltip' title='Cancel'>Cancel</a>
                {!! action_update_button(route('acl.create',$usergroup_id)) !!}
            </span>
            @endif
        </div>

        <div class="row x_content">
            <table class="table table-striped table-responsive table-hover table-condensed">
			<tr>
				<th>{!! sortTableHeaderSnippet('Module','module') !!}</th>
				<th>{!! sortTableHeaderSnippet('Permission','permission') !!}</th>
			</tr>
			@foreach ($ACLs as $ACL)
			<tr>
				<td>{{ $modules[$ACL->module] }}</td>
				<td>{{ count(json_decode($ACL->permission,true))? implode(', ', json_decode($ACL->permission,true)):'No Access' }}</td>
			</tr>
			@endforeach
			</table>

			<div class="text-center">
			{!! str_replace('/?', '?', $ACLs->appends(request()->all())->render()) !!}
			</div>
        </div>
    </div>
</div>
@stop