<!doctype html>
<html><head>
    <meta charset="utf-8">
    <title>{{ config('cms.brand','CMS Starter Convep Mobilogy') }} @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Convep Mobilogy Wiki Chua">

    <!-- Le styles -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/alertify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/css/login1.css') }}" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

  <body>



<div class="login" style="padding-top: 10em; ">
  <div class="login-header">
    <h1>{{ config('cms.brand') }}</h1>
  </div>
  <div class="login-form">


        {!! Form::open(array('route' => array('login'),'name' => 'form','class' => 'form-horizontal', 'method' => 'post','files'=>false)) !!}
                        <div class="form-group">
                        <h4>Username:</h4>
                            {!! Form::text('email', old('email'), array('class' => 'input_text  input-lg','style'=>'color:black','placeholder' => 'someone@email.com')) !!}
                        </div>
                        <div class="form-group">
                            <h4>Password:</h4>
                            {!! Form::password('password', array('class' => 'input_password input-lg','style'=>'color:black','placeholder' => 'Password')) !!}
                        </div>
                        <a class="reset_pass" href={{ route('change_password_email') }}>Lost your password?</a>
                    <hr />
                        <div class="form-group">
                            <button type="submit" class="input_button login-button btn btn-success btn-lg submit" name='login'><i class="fa fa-sign-in"></i> Log In</button>

                        </div>
                    {!! Form::close() !!}

  </div>
</div>


    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/alertifyjs/alertify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
    {{-- <script type="text/javascript" src="{{ asset('assets/js/cms.js') }}"></script> --}}

    @include('alert')

</body></html>