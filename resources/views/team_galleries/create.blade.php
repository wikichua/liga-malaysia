@extends('template')

@section('title')
	Gallery
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Create Gallery</h1>
	    </div>
        <div class="row x_content">
            {!! Form::open(array('route' =>array('team_galleries.store',$team_id) ,'name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}

           	<div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow('','default')) }}" class="img-thumbnail previewImg">
					{!! Form::file('image', array('class'=>"form-control")) !!}
					<p class="text-danger">Image file allow: JPEG, JPG, PNG</p>
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('name',old('name'), array('class'=>"form-control",'placeholder'=>"")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('team_galleries',$team_id) }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#image").change(function(){
	    previewImg(this,'previewHere');
	});

});
</script>
@stop