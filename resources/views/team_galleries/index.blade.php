@extends('template')

@section('title')
	Gallery
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h2 class="x_title"><a href="{{ route('profile_team') }}"><i class="fa fa-arrow-left"></i></a> Gallery  ({{ $ProfileTeam->name }})</h2>
	        @if (ACLButtonCheck('TM_GLY_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('team_galleries.create',$team_id)) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th class="col-sm-1 shift_column"></th>
					<th>Image</th>
					<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th class="shift_column"></th>
					<th></th>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($TeamGallerys as $TeamGallery)
				<tr>
					<td class="shift_column">
						{!! shifting_buttons($TeamGallery,route('team_galleries.shift',array($team_id,$TeamGallery->id,$TeamGallery->p_id)),route('team_galleries.shift',array($team_id,$TeamGallery->id,$TeamGallery->n_id))) !!}
					</td>
					<td>
						<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($TeamGallery->image,'default')) }}" class="img-thumbnail previewImg col-sm-3">
					</td>
					<td>{{ $TeamGallery->name }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('TM_GLY_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('team_galleries.edit',array($team_id,$TeamGallery->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('TM_GLY_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('team_galleries.destroy',array($team_id,$TeamGallery->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $TeamGallerys->appends(request()->all())->render()) !!}
				</div>	
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop