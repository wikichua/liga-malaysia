@extends('template')

@section('title')
	News
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit News</h1>
	    </div>
             <div class="row x_content">
            {!! Form::open(array('route' => array('news.update',$News->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($News->image,'default')) }}" class="img-thumbnail previewImg">
					{!! Form::file('image', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('name', old('name',$News->name), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('description', old('description',$News->description), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('published_at', 'Publish Date', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('published_at', old('published_at',$News->published_at), array('class'=>"form-control datetimepicker")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('tournament', 'Tournament', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('tournament',config('fb.tournament') ,old('tournament',$News->tournament), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('news') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#image").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop