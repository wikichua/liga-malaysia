@extends('template')

@section('title')
	App Versioning
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit App Versioning</h1>
	    </div>

        <div class="row x_content">
            {!! Form::open(array('route' => array('app.version.update',$AppVersion->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('os', 'OS', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('os',$OSs , old('os',$AppVersion->os), array('class'=>"form-control",'placeholder'=>"Select One")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('version', 'Content', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('version', old('version',$AppVersion->version), array('class'=>"form-control",'placeholder'=>"e.g. 1.0.1 or X.x.x")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('type', 'Content', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('type',$types ,old('type',$AppVersion->type), array('class'=>"form-control",'placeholder'=>"Select One")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Content', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('description', old('description',$AppVersion->description), array('class'=>"form-control",'placeholder'=>"e.g. Critical update")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('app.version') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop