@extends('template')

@section('title')
	App Versioning
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> App Versioning</h1>
	        @if (ACLButtonCheck('APPVSN_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('app.version.create')) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th class="col-sm-1 shift_column"></th>
					<th>{!! sortTableHeaderSnippet('OS','os') !!}</th>
					<th>{!! sortTableHeaderSnippet('Version','version') !!}</th>
					<th>{!! sortTableHeaderSnippet('Type','type') !!}</th>
					<th>{!! sortTableHeaderSnippet('Description','description') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th>{!! searchTableHeaderSnippet('os') !!}</th>
					<th>{!! searchTableHeaderSnippet('version') !!}</th>
					<th>{!! searchTableHeaderSnippet('type') !!}</th>
					<th>{!! searchTableHeaderSnippet('description') !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($AppVersions as $AppVersion)
				<tr>
					<td>{{ $AppVersion->os }}</td>
					<td>{{ $AppVersion->version }}</td>
					<td>{{ $AppVersion->type }}</td>
					<td>{{ str_limit($AppVersion->description,20) }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('APPVSN_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('app.version.edit',array($AppVersion->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('APPVSN_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('app.version.destroy',array($AppVersion->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $AppVersions->appends(request()->all())->render()) !!}
				</div>	
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop