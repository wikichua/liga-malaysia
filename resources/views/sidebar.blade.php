        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href={{ route('dashboard') }}>{{ config('cms.brand','CMS Starter Convep Mobilogy') }}</a>


            </div>

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="{{ route('profile') }}"><i class="fa fa-user fa-fw"></i> User Profile</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            @if(Session::has('original_user_id'))
                            <a href="{{ route('user.switch',0) }}"><i class="fa fa-random"></i> Switch Back</a>
                            @else
                            <a href="{{ route('logout') }}"><i class="fa fa-sign-out"></i> Log Out</a>
                            @endif
                        </li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="{{ activeOnCurrentRoute('dashboard') }}"><a href="{{ route('dashboard') }}">Home</a></li>
                        <li>
                        @if(ACLButtonCheck('MTCHS_MGMT',$action = 'Read'))

                             <a href="#"><i class=""></i> Match<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li class='{{ activeOnCurrentRoute('match') }}'>
                                    <a href="{{ route('match') }}"></i>Overall</a>
                                </li>
                                <li class={{ activeOnCurrentRoute('match') }}>
                                    <a href="{{ route('match',array('s-state=Fixture')) }}">Fixture</a>
                                </li>
                                <li class={{ activeOnCurrentRoute('match') }}>
                                    <a href="{{ route('match',array('s-state=OnGoing')) }}">On Going</a>
                                </li>
                                <li class={{ activeOnCurrentRoute('match') }}>
                                    <a href="{{ route('match',array('s-state=Result')) }}">Result</a>
                                </li>
                                <li class={{ activeOnCurrentRoute('match') }}>
                                    <a href="{{ route('match.tbc') }}">TBC</a>
                                </li>

                            </ul>
                        @endif
                        </li>
                            @if(ACLButtonCheck('TEAMS_MGMT',$action = 'Read'))
                                <li class="{{ activeOnCurrentRoute('profile_team') }}"><a href="{{ route('profile_team') }}"> Team</a></li>
                            @endif
                            @if(ACLButtonCheck('NEWS_MGMT',$action = 'Read'))
                                <li class="{{ activeOnCurrentRoute('news') }}"><a href="{{ route('news') }}"> News</a></li>
                            @endif
                            @if(ACLButtonCheck('VIDS_MGMT',$action = 'Read'))
                                <li class="{{ activeOnCurrentRoute('videon') }}"><a href="{{ route('video') }}"> Video</a></li>
                            @endif
                            @if(ACLButtonCheck('ADV_MGMT',$action = 'Read'))
                                <li class="{{ activeOnCurrentRoute('advert') }}"><a href="{{ route('advert') }}"> Advert</a></li>
                            @endif

                             <li>
                            <a href="#"> Setting<span class="fa arrow"></span></a>
                                <ul class="nav nav-second-level">
                                    @if(ACLButtonCheck('GRPSTGS_MGMT',$action = 'Read'))
                                    <li class="{{ activeOnCurrentRoute('round_robin') }}"><a href="{{ route('round_robin') }}">Group Stage</a></li>
                                    @endif
                                    @if(ACLButtonCheck('USR_MGMT',$action = 'Read'))
                                    <li class="{{ activeOnCurrentRoute('user') }}"><a href="{{ route('user') }}"> User</a></li>
                                    @endif
                                    @if(ACLButtonCheck('USR_GRP_MGMT',$action = 'Read'))
                                    <li class="{{ activeOnCurrentRoute('acl|\busergroup') }}"><a href="{{ route('usergroup') }}"> User Group</a></li>
                                    @endif
                                    @if(ACLButtonCheck('APPVSN_MGMT',$action = 'Read'))
                                    <li class="{{ activeOnCurrentRoute('app.version') }}"><a href="{{ route('app.version') }}"> App Version</a></li>
                                    @endif
                                    @if(ACLButtonCheck('ADT_MGMT',$action = 'Read'))
                                    <li class="{{ activeOnCurrentRoute('audit_trail') }}"><a href="{{ route('audit_trail') }}"> Audit Trail</a></li>
                                    @endif
                                </ul>
                        </li>

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>