<h1>{{ route('api.'.$api) }}</h1>
{!! Form::open(array('route' => array('api.'.$api),'class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}
<div>
	{!! Form::label('data', 'Data') !!}
	<div>
		{!! Form::textarea('data', json_encode($sample)) !!}
	</div>
</div>
@foreach ($file_keys as $name)
<div>
	{!! Form::label($name, $name) !!}
	<div>
		{!! Form::file($name) !!}
	</div>
</div>
@endforeach
<div>
	{!! Form::label('debug', 'Debug') !!}
	{!! Form::checkbox('debug', true) !!}
</div>
<div>
	{!! Form::submit('Submit') !!}
</div>
{!! Form::close() !!}

<div>
<ol>
@foreach ($links as $link)
	<li><a href="{{ $link }}" title="{{ $link }}">{{ $link }}</a></li>
@endforeach
</ol>
</div>