@extends('template')

@section('title')
	Audit Trail
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-history'></i> Audit Trail</h1>
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th>{!! sortTableHeaderSnippet('Date Time','created_at') !!}</th>
					<th>{!! sortTableHeaderSnippet('User','user-name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Action','action') !!}</th>
					<th>{!! sortTableHeaderSnippet('Table/Model','model') !!}</th>
					<th style='width:15%;'>Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th>{!! searchTableHeaderSnippet('created_at') !!}</th>
					<th>{!! searchTableHeaderSnippet('user-name') !!}</th>
					<th>{!! searchTableHeaderSnippet('action') !!}</th>
					<th>{!! searchTableHeaderSnippet('model') !!}</th>
					<th class="actionButtons">{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($AuditTrails as $AuditTrail)
				<tr>
					<td>{{ $AuditTrail->created_at->format('d/M/Y g:i a') }}</td>
					<td>{{ $AuditTrail->user->name or 'System' }}</td>
					<td>{{ ucwords($AuditTrail->action) }}</td>
					<td>{{ ucwords(str_replace('mcmc\\','',$AuditTrail->model)) }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('ADT_MGMT','Read'))
							<a class="btn btn-primary" href="{{ route('audit_trail.show',array($AuditTrail->id)) }}" data-toggle='tooltip' title='Show'><i class="fa fa-eye"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
			</table>
			<div class="text-center">
			{!! str_replace('/?', '?', $AuditTrails->appends(request()->all())->render()) !!}
			</div>	
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop