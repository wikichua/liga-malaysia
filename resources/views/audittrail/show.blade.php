@extends('template')

@section('title')
	Audit Trail
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-history'></i> Audit Trail</h1>
	    </div>

        <div class="row x_content">
			<div class="form-horizontal">
				<div class="form-group">
					<label class="col-sm-2 control-label">User / Date Time</label>
					<div class="col-sm-10">
						<p class="form-control">{{ $AuditTrail->user->name }} / <strong class="text-danger">{{ $AuditTrail->created_at->format('d/M/Y g:i a') }}</strong></p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Action / Model / ID</label>
					<div class="col-sm-10">
						<p class="form-control">{{ ucwords($AuditTrail->action) }} / {{ ucwords(str_replace('mcmc\\','',$AuditTrail->model)) }} / {{ ucwords($AuditTrail->model_id) }}</p>
					</div>
				</div>
				<div class="form-group">
					<label class="col-sm-2 control-label">Changes</label>
					<div class="col-sm-5">
						<p class="text-center text-danger"><strong>New Data</strong></p>
						<pre>{!! json_encode(json_decode($AuditTrail->new_data),JSON_PRETTY_PRINT) !!}</pre>
					</div>
					<div class="col-sm-5">
						<p class="text-center text-warning"><strong>Old Data</strong></p>
						<pre>{!! json_encode(json_decode($AuditTrail->old_data),JSON_PRETTY_PRINT) !!}</pre>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-6 col-sm-12">
						<a href="{{ route('audit_trail') }}" class='btn btn-danger'>Cancel</a>
					</div>
				</div>
	        </div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
});
</script>
@stop