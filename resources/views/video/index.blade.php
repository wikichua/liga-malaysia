@extends('template')

@section('title')
	Video
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Video</h1>
	        @if (ACLButtonCheck('VIDS_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('video.create')) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th>{!! sortTableHeaderSnippet('Image','image') !!}</th>
					<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Description','description') !!}</th>
					<th>{!! sortTableHeaderSnippet('Publish Date','published_at') !!}</th>
					<th>{!! sortTableHeaderSnippet('Tournament','tournament') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th></th>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! searchTableHeaderSnippet('description') !!}</th>
					<th>{!! searchTableHeaderSnippet('published_at','date') !!}</th>
					<th>{!! searchTableHeaderSnippet('tournament','select',$tournaments) !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($Videos as $Video)
				<tr>
					<td class="col-sm-1"><img src="{{ asset('uploads/'.imgTagShow($Video->image,'default')) }}" class="img-thumbnail"></td>
					<td>{{ $Video->name }}</td>
					<td>{{ str_limit($Video->description, 80) }}</td>
					<td>{{ $Video->published_at }}</td>
					<td>{{ $tournaments[$Video->tournament] }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">
							@if (ACLButtonCheck('VIDS_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('video.edit',array($Video->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('VIDS_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('video.destroy',array($Video->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $Videos->appends(request()->all())->render()) !!}
				</div>	
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop