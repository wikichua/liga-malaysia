@extends('template')

@section('title')
	Video
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit Video</h1>
	    </div>
             <div class="row x_content">
            {!! Form::open(array('route' => array('video.update',$Video->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('image', 'Image', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($Video->image,'default')) }}" class="img-thumbnail previewImg">
					{!! Form::file('image', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('name', old('name',$Video->name), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('description', 'Description', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::textarea('description', old('description',$Video->description), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('published_at', 'Publish Date', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('published_at', old('published_at',$Video->published_at), array('class'=>"form-control datetimepicker")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('tournament', 'Tournament', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('tournament',config('fb.tournament') ,old('tournament',$Video->tournament), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('url', 'URL', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('url', old('url',$Video->url), array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('video') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#image").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop