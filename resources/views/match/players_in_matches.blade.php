@extends('template')

@section('title')
	Team
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Match ({{config('fb.tournament')[$Match->tournament_type][$Match->tournament] }})</h1>
	        <p class="text-danger">System will auto-calculate Total Appearance of each player in all of his matches. You may Deselect any unwanted player</p>
	    </div>
	    {!! Form::open(array('route' => array('match.players_in_matches_create',$Match->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}

	    <div class="col-md-6 col-sm-6 col-xs-6 ">
			<h4><img style="width: 2em" src="{{ asset('uploads/'.imgTagShow($team_home->logo,'profile')) }}"> Team Home ({{ $team_home->name }}) </h4>
		    <table class="table table-condensed table-hover">
		    <tr class="">
		    		<td>
		    			<strong>Select All/Unselect All</strong>
		    		</td>
		    		<td>
		    			{!! Form::checkbox('select_all','','',array('class'=>'home_checkAll')) !!}
		    		</td>
		    	</tr>
		    @foreach($home_Players as $key => $home_player)
		    	<tr class="tr_row">
		    		<td>
		    			{{ $home_player->name }}
		    		</td>
		    		<td>
		    			<input type="checkbox" class='ckbox home_ckbox' name='home_player[]' id={{ $home_player->id }}  value={{ $home_player->id }}
		    			{{ (in_array($home_player->id, $checked)?"checked":"") }} >
		    		</td>
		    	</tr>
		    	@endforeach
		    	<tr> </tr>
		    </table>
	    </div>
	    <div class="col-md-6 col-sm-6 col-xs-6 ">
	    	<h4><img style="width: 2em" src="{{ asset('uploads/'.imgTagShow($team_away->logo,'profile')) }}"> Team Away ({{  $team_away->name }}) </h4>
		    <table class="table table-condensed table-hover">
		    <tr class="">
		    		<td>
		    			<strong>Select All/Unselect All</strong>
		    		</td>
		    		<td>
		    			{!! Form::checkbox('select_all','','',array('class'=>'away_checkAll')) !!}
		    		</td>
		    	</tr>
		    @foreach($away_Players as $key =>  $away_player)
		    	<tr class="tr_row">
		    		<td>
		    			{{ $away_player->name }}
		    		</td>
		    		<td>
		    			<input type="checkbox" class='ckbox away_ckbox' name="away_player[]" id={{ $away_player->id }} value={{ $away_player->id }}
		    			{{ in_array($away_player->id, $checked)?"checked":"" }}>
		    		</td>
		    	</tr>
		    	@endforeach
		    </table>
	    </div>
	    <div class="col-md-12 col-sm-12 col-xs-12 text-center">
	    	{!! Form::submit('Submit', array('class'=>"btn btn-primary ",'style'=>'margin-bottom: 2em')) !!}
	    	<a style="margin-bottom: 2em" href="{{ route('match') }}" class='btn btn-danger'>Cancel</a>
	    </div>
	    {!! Form::close() !!}
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	// $('.tr_row').css({
	// 	'cursor': 'hand',
	// 	'cursor': 'pointer'
	// });
	// $('.tr_row').click(function(event) {
	// 	var $this = $(this);
	// 	var $ckbox = $this.find('.ckbox');
	// 	if ($ckbox.is(":checked") == false) {
	// 		$ckbox.prop('checked', true);
	// 	}else{
	// 		$ckbox.prop('checked', false);
	// 	}
	// });

	$(".home_checkAll").change(function () {
	    $(".home_ckbox:checkbox").prop('checked', $(this).prop("checked"));
	});
	$(".away_checkAll").change(function () {
	    $(".away_ckbox:checkbox").prop('checked', $(this).prop("checked"));
	});
});
</script>
@stop