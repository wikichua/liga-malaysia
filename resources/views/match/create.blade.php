@extends('template')

@section('title')
	Match
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Create Match</h1>

	    </div>

        <div class="row x_content">
            {!! Form::open(array('route' => 'match.store','name' => 'register_form','class' => 'form-horizontal', 'method' => 'post','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('tournament', 'Tournament', array('class'=>'col-sm-2 control-label required')) !!}
				<div class="col-sm-3">
					{!! Form::select('tournament',config('fb.tournament') ,old('tournament'), array('class'=>"form-control select2")) !!}
				</div>
				{!! Form::label('round', 'Round', array('class'=>'col-sm-2 control-label required')) !!}
				<div class="col-sm-3">
					{!! Form::select('round',config('fb.round'), old('round'), array('class'=>"form-control select2")) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-10">
					<span style="font-size: 2em;font-style: italic">VS</span>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('team_home_id', 'Team Home', array('class'=>'col-sm-2 control-label required')) !!}
				<div class="col-sm-3">
					{!! Form::select('team_home_id',$Teams_name, old('team_home_id'), array('class'=>"form-control select2")) !!}
				</div>
				{!! Form::label('team_away_id', 'Team Away', array('class'=>'col-sm-2 control-label required')) !!}
				<div class="col-sm-3">
					{!! Form::select('team_away_id',$Teams_name, old('team_away_id'), array('class'=>"form-control select2")) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('start_date_time', 'Start Date', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('start_date_time', old('start_date_time'), array('class'=>"form-control datetimepicker")) !!}
				</div>
				{!! Form::label('location', 'Location', array('class'=>'col-sm-2 control-label required')) !!}
				<div class="col-sm-3">
					{!! Form::text('location', old('location'), array('class'=>"form-control ")) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('related_match_id', 'Related Match', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-8">
					{!! Form::select('related_match_id',array(''=>'') + $related_match_list, old('related_match_id'), array('class'=>"form-control")) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-10">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('match') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::hidden('isPostpond',false) !!}
			{!! Form::hidden('state','Fixture') !!}
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(document).ready(function(){
		var arr ={!!  json_encode($all) !!};
		var j=0;
		var option='';
		// alert($('#team_home_id').html());
		for(var i=0;i<={!! count($all)-1 !!};i++){
		if(($.inArray($('#tournament').val(),arr[i]['tournaments']))!=-1 ){
			option+='<option value='+arr[i]['id']+'>'+arr[i]['name']+'</option>';
			j++;
		}else{
		}

		}
		$('#team_home_id').html(option);
		$('#team_away_id').html(option);

	$('#tournament').change(function(){
		 arr ={!!  json_encode($all) !!};
		 j=0;
		 option='';
		// alert($('#team_home_id').html());
		for(var i=0;i<={!! count($all)-1 !!};i++){
		if(($.inArray($('#tournament').val(),arr[i]['tournaments']))!=-1 ){
			option+='<option value='+arr[i]['id']+'>'+arr[i]['name']+'</option>';
			j++;
		}else{
		}

		}
		$('#team_home_id').html(option);
		$('#team_away_id').html(option);

		// alert($('#team_home_id option:eq(1)').text());
	});
});
</script>
@stop


