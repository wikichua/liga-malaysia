@extends('template')

@section('title')
	Match
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i>
	        @if($isPostpond==1)
	        Postpone
@else
	        Edit
	        @endif
	        Match</h1>
	    </div>

                <div class="row x_content">
            {!! Form::open(array('route' => array('match.update',$Match->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('tournament', 'Tournament', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('tournament',config('fb.tournament.'.$Match->tournament_type)[$Match->tournament], array('class'=>"form-control ",'readonly'=>'true')) !!}
				</div>
				{!! Form::label('round', 'Round', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('round', $Match->round, array('class'=>"form-control",'readonly'=>'true')) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-10">
					<span style="font-size: 2em;font-style: italic">VS</span>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('team_home_id', 'Team Home', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('team_home_id',$Teams_name[$Match->team_home_id], array('class'=>"form-control",'readonly'=>'true')) !!}
				</div>
				{!! Form::label('team_away_id', 'Team Away', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('team_away_id',$Teams_name[$Match->team_away_id], array('class'=>"form-control",'readonly'=>'true')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('start_date_time', 'Start Date', array('class'=>'lbl_start_date_time col-sm-2 control-label')) !!}
				<div class="col-sm-3">
@if($isPostpond==1)
					{!! Form::text('start_date_time', '', array('class'=>"form-control datetimepicker")) !!}
					@else
					{!! Form::text('start_date_time', $Match->start_date_time!='30-11--0001 00:00:00'?$Match->start_date_time:'', array('class'=>"form-control datetimepicker",($Match->state=='Fixture' || $isPostpond==1)? '':'readonly')) !!}
					@endif
				</div>
				{!! Form::label('location', 'Location', array('class'=>'lbl_location col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('location', $Match->location, array('class'=>"form-control ",($Match->state=='Fixture' || $isPostpond==1)? '':'readonly')) !!}
				</div>
			</div>
@if($isPostpond==1)
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<p class="text-danger">Empty date field if new postpone date is unknown. Go to Match TBC to edit.</p>
			</div>
			</div>
			<div class="form-group">
				{!! Form::label('remark', 'Postpone Notification Message', array('class'=>'lbl_remark col-sm-2 control-label')) !!}
				<div class="col-sm-8">
					{!! Form::textarea('remark', $Match->remark!=''?$Match->remark:'The match between <<TEAMHOME>> vs <<TEAMAWAY>> has been postponed to <<STARTDATE>>.', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<p class="text-danger">This remark will reflect in apps notification</p>
			</div>
			</div>

			<div class="form-group">
				<div class="col-sm-10 col-sm-offset-2">
					<p><strong>Short Code</strong></p>
					<code>
					&lt;&lt;TEAMHOME&gt;&gt; = Team Home<br />
					&lt;&lt;TEAMAWAY&gt;&gt; = Team Away<br />
					&lt;&lt;STARTDATE&gt;&gt; = Start Date<br />
					&lt;&lt;LOCATION&gt;&gt; = Location
					</code>
			</div>
			</div>
					@endif

						<div class="form-group">
				{!! Form::label('related_match_id', 'Related Match', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-8">
					{!! Form::select('related_match_id',array(''=>'') + $related_match_list, old('related_match_id',$Match->related_match_id), array('class'=>"form-control")) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-10">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					@if ($Match->start_date_time=='30-11--0001 00:00:00')
					<a href="{{ route('match.tbc') }}" class='btn btn-danger'>Cancel</a>
					@else
					<a href="{{ route('match',array('s-state='.$Match->state)) }}" class='btn btn-danger'>Cancel</a>
					@endif
				</div>
			</div>

			{!! Form::hidden('isPostpond',$isPostpond) !!}
			{!! Form::hidden('state',$Match->state) !!}
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	@if ($Match->state=='Fixture' || $isPostpond==1)
	// $('.lbl_start_date_time,.lbl_location').addClass('required').promise().done(function(){
	// 	init_required_label();
	// });
	@endif
	@if ($Match->state!='Fixture' || $isPostpond==1)
	// $('.lbl_remark').addClass('required').promise().done(function(){
	// 	$('.lbl_start_date_time,.lbl_location').removeClass('required').promise().done(function(){
	// 		init_required_label();
	// 	});
	// });
	@endif
});
</script>
@stop