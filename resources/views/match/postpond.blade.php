@extends('template')

@section('title')
	Match
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit Match</h1>
	    </div>

                <div class="row x_content">
            {!! Form::open(array('route' => array('match.update',$Match->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('tournament', 'Tournament', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('tournament',config('fb.tournament.'.$Match->tournament_type)[$Match->tournament], array('class'=>"form-control ",'readonly'=>'true')) !!}
				</div>
				{!! Form::label('round', 'Round', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('round', $Match->round, array('class'=>"form-control",'readonly'=>'true')) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-10">
					<span style="font-size: 2em;font-style: italic">VS</span>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('team_home_id', 'Team Home', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('team_home_id',$Teams_name[$Match->team_home_id], array('class'=>"form-control",'readonly'=>'true')) !!}
				</div>
				{!! Form::label('team_away_id', 'Team Away', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('team_away_id',$Teams_name[$Match->team_away_id], array('class'=>"form-control",'readonly'=>'true')) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('start_date_time', 'Start Date', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('start_date_time', old('start_date_time'), array('class'=>"form-control")) !!}
				</div>
				{!! Form::label('location', 'Location', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-3">
					{!! Form::text('location', old('location'), array('class'=>"form-control ")) !!}
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('remark', 'Remark', array('class'=>'col-sm-2 control-label')) !!}
				<div class="col-sm-8">
					{!! Form::textarea('remark', $Match->remark, array('class'=>"form-control")) !!}
				</div>
			</div>

			<div class="form-group">
				<div class="col-sm-offset-5 col-sm-10">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('match') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	
});
</script>
@stop