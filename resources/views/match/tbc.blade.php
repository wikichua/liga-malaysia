@extends('template')

@section('title')
	Match
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">

        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Match TBC</h1>
	        @if (ACLButtonCheck('MTCHS_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('match.create')) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th>{!! sortTableHeaderSnippet('Tournament','tournament') !!}</th>
					<th>{!! sortTableHeaderSnippet('Round','round') !!}</th>
					<th>{!! sortTableHeaderSnippet('Team Home','ProfileTeamHome-name') !!}</th>
					<th>Score</th>
					<th>{!! sortTableHeaderSnippet('Team Away','ProfileTeamAway-name') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th>{!! searchTableHeaderSnippet('tournament','select',config('fb.tournament')) !!}</th>
					<th>{!! searchTableHeaderSnippet('round','select',config('fb.round')) !!}</th>
					<th>{!! searchTableHeaderSnippet('ProfileTeamHome-name','select',$Teams_name) !!}</th>
					<th></th>
					<th>{!! searchTableHeaderSnippet('ProfileTeamAway-name','select',$Teams_name) !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($Matches as $Match)
				<tr>

					<td>{{ config('fb.tournament.'.$Match->tournament_type)[$Match->tournament] }}</td>
					<td>{{ $Match->round }}</td>
					<td>{{ $Match->ProfileTeamHome->name }}</td>
					<td>
					@if ($Match->state == 'Result')
					{{ $Match->team_home_goal }}({{ $Match->team_home_result }}):{{ $Match->team_away_goal }}({{ $Match->team_away_result }})
					@else
					NIL
					@endif
					</td>
					<td>{{ $Match->ProfileTeamAway->name }}</td>

					<td class="actionButtons">
						<div class="btn-group btn-group-xs">

							@if (ACLButtonCheck('MTCHS_MGMT','Read'))
							<a class="btn btn-default dropdown-toggle" href="" data-toggle='dropdown' title=''><span class="fa fa-flag-checkered "></span></a>
								<ul class="dropdown-menu dropdown-menu-right">
		      						@if (ACLButtonCheck('MTCHS_MGMT','Postpone'))
								    	<li><a href="{{ route('match.postpond',array($Match->id)) }}">Postpone</a></li>
								    @endif
								    @if (ACLButtonCheck('PIM_MGMT','Manage'))
								    	<li><a href="{{ route('match.players_in_matches',array($Match->id)) }}">Players In Matches</a></li>
								    @endif
							    </ul>
							@endif
							@if (ACLButtonCheck('MTCHS_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('match.edit',array($Match->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('MTCHS_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('match.destroy',array($Match->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $Matches->appends(request()->all())->render()) !!}
				</div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop