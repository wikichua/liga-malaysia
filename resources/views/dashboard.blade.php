@extends('template')

@section('title')
    Dashboard
@stop

@section('body')
<div class="row">
	<div class="col-md-12">
	<div class="panel-heading">
		<div class="panel-heading-left"><h4>Match of the day</h4></div>
	</div>
	<table class="table table-striped table-responsive table-hover">
		<tr>
			<th>{!! sortTableHeaderSnippet('Start Datetime','start_date_time') !!}</th>
			<th>{!! sortTableHeaderSnippet('Tournament','tournament') !!}</th>
			<th>{!! sortTableHeaderSnippet('Round','round') !!}</th>
			<th>{!! sortTableHeaderSnippet('Team Home','ProfileTeamHome-name') !!}</th>
			<th>Score</th>
			<th>{!! sortTableHeaderSnippet('Team Away','ProfileTeamAway-name') !!}</th>
			<th>{!! sortTableHeaderSnippet('State','state') !!}</th>
			<th class="col-sm-1">Action</th>
		</tr>
		{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
		<tr>
			<th>{!! searchTableHeaderSnippet('start_date_time','date') !!}</th>
			<th>{!! searchTableHeaderSnippet('tournament','select',config('fb.tournament')) !!}</th>
			<th>{!! searchTableHeaderSnippet('round','select',config('fb.round')) !!}</th>
			<th>{!! searchTableHeaderSnippet('ProfileTeamHome-name','select',$Teams_name) !!}</th>
			<th></th>
			<th>{!! searchTableHeaderSnippet('ProfileTeamAway-name','select',$Teams_name) !!}</th>
			<th>{!! searchTableHeaderSnippet('state','select',config('fb.state')) !!}</th>
			<th>{!! search_reset_buttons() !!}</th>
		</tr>
		{!! Form::close() !!}
		@foreach ($TodayMatchs as $Match)
		<tr>
			<td>{{ $Match->start_date_time }}</td>
			<td>{{ config('fb.tournament.'.$Match->tournament_type)[$Match->tournament] }}</td>
			<td>{{ $Match->round }}</td>
			<td>{{ $Match->ProfileTeamHome->name }}</td>
			<td>
			@if ($Match->state == 'Result')
			{{ $Match->team_home_goal }}({{ $Match->team_home_result }}) : {{ $Match->team_away_goal }}({{ $Match->team_away_result }})
			@else
			NIL
			@endif
			</td>
			<td>{{ $Match->ProfileTeamAway->name }}</td>
			<td>{{ $Match->state }}</td>
			<td class="actionButtons">
				<div class="btn-group btn-group-xs">

					@if (ACLButtonCheck('MTCHS_MGMT','Update'))
					<a class="btn btn-default dropdown-toggle" href="" data-toggle='dropdown' title=''><span class="fa fa-flag-checkered "></span></a>
								<ul class="dropdown-menu dropdown-menu-right">
		      						@if (ACLButtonCheck('MTCHS_MGMT','Postpone'))
								    	<li><a href="{{ route('match.postpond',array($Match->id)) }}">Postpone</a></li>
								    @endif
		      						@if (ACLButtonCheck('SCORING','Manage')&&$Match->state!='Fixture')
								    	<li><a href="{{ route('standing.edit',array($Match->id)) }}">Score</a></li>
								    	<li><a href="{{ route('standing.penalty_edit',array($Match->id)) }}">Penalty</a></li>
								    @endif
								    @if (ACLButtonCheck('PIM_MGMT','Manage')&&$Match->state!='Fixture')
								    	<li><a href="{{ route('match.players_in_matches',array($Match->id)) }}">Players In Matches</a></li>
								    @endif
							    </ul>
					@endif

				</div>
			</td>
		</tr>
		@endforeach
	</table>
	<hr>

	</div>
	{{-- <div class="col-md-12"> --}}
			{{-- @include('analytic.access_rate',compact('sessions')) --}}
	{{-- </div> --}}
</div>
@stop

@section('style')
	{{-- expr --}}
@stop

@section('scripts')
	{{-- expr --}}
@stop