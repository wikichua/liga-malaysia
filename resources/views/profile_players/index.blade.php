@extends('template')

@section('title')
	Player
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h2 class="x_title"><a href="{{ route('profile_team') }}"><i class="fa fa-arrow-left"></i></a> Player ({{ $ProfileTeam->name }})</h2>
	        @if (ACLButtonCheck('PLYERS_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('profile_players.create',$team_id)) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th class="col-sm-2">Picture</th>
					<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Position','position') !!}</th>
					<th>{!! sortTableHeaderSnippet('Nationality','nationality') !!}</th>
					<th>{!! sortTableHeaderSnippet('Squad Number','squad_number') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>

					<th></th>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! searchTableHeaderSnippet('position','select',config('fb.position')) !!}</th>
					<th>{!! searchTableHeaderSnippet('nationality','select',config('fb.countries')) !!}</th>
					<th>{!! searchTableHeaderSnippet('squad_number') !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($ProfilePlayers as $ProfilePlayer)
				<tr>
					<td>
						<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($ProfilePlayer->profile_picture,'default')) }}" class="img-thumbnail previewImg col-sm-12">
					</td>
					<td>{{ $ProfilePlayer->name }}</td>
					<td>{{ isset(config('fb.position')[$ProfilePlayer->position])? config('fb.position')[$ProfilePlayer->position]:''}}</td>
					<td>{{ (is_numeric($ProfilePlayer->nationality))?config('fb.countries')[$ProfilePlayer->nationality]:$ProfilePlayer->nationality }}</td>
					<td>{{ $ProfilePlayer->squad_number }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">

							@if (ACLButtonCheck('PLYERS_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('profile_players.edit',array($team_id,$ProfilePlayer->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('PLYERS_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('profile_players.destroy',array($team_id,$ProfilePlayer->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $ProfilePlayers->appends(request()->all())->render()) !!}
				</div>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
	@if(isSearchOrSortExecuted() === 'false')
		$('.shift_column').remove();
	@endif
});
</script>
@stop