@extends('template')

@section('title')
	Player
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit Player</h1>
	    </div>
        <div class="row x_content">
        {!! Form::open(array('route' => array('profile_players.update',$team_id,$ProfilePlayer->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('image', 'Picture', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($ProfilePlayer->profile_picture,'default')) }}" class="img-thumbnail previewImg">
					{!! Form::file('image', array('class'=>"form-control")) !!}
					<p class="text-danger">Image file allow: JPEG, JPG, PNG</p>

					    <label>Remove Image
					      {!! Form::checkbox('remove_image','true') !!}
					    </label>
				</div>
			</div>

			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('name',$ProfilePlayer->name, array('class'=>"form-control",'placeholder'=>"")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('position', 'Position', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('position',config('fb.position'),$ProfilePlayer->position, array('class'=>"form-control",'placeholder'=>"")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('nationality', 'Nationality', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('nationality',config('fb.countries'),$ProfilePlayer->nationality, array('class'=>"form-control",'placeholder'=>"")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('squad_number', 'Squad Number', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('squad_number',$ProfilePlayer->squad_number, array('class'=>"form-control",'placeholder'=>"")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('appearance', 'Total Appearance', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('appearance',$ProfilePlayer->appearance, array('class'=>"form-control",'placeholder'=>"")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('profile_players',$ProfilePlayer->profile_team_id) }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
		{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#image").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop