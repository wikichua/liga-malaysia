<!doctype html>
<html><head>
    <meta charset="utf-8">
    <title>{{ config('cms.brand','') }} @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Convep Mobilogy">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/select2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/jquery.datetimepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/alertify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/flexslider.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/Trumbowyg/ui/trumbowyg.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/sb-admin-2.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/metisMenu.css') }}" rel="stylesheet">

    @yield('style')

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link rel="shortcut icon" href="assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="assets/ico/apple-touch-icon-57-precomposed.png">
    <link href="http://fonts.googleapis.com/css?family=Raleway:400,300" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
  </head>
  <body>
    <div id="wrapper">
            @include('sidebar')

            <div id="page-wrapper">
                @yield('body')
            </div>
    </div>

    <script type="text/javascript" src="//code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="//momentjs.com/downloads/moment.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.datetimepicker.full.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/select2.full.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/alertifyjs/alertify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/autosize.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/Trumbowyg/trumbowyg.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/cms.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/sb-admin-2.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/metisMenu.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/chartjs-2.1.6/dist/Chart.min.js') }}"></script>

    @include('alert')

    <script type="text/javascript">
    function init_required_label() {
        $('.required').prepend('<span class="fa fa-asterisk text-danger"></span> ');
    }
    $(document).ready(function(){
        alertify.defaults.glossary.title = '{{ config('cms.brand') }}';
        init_required_label();
    });
    </script>

    @yield('scripts')

    <script src="https://www.gstatic.com/firebasejs/live/3.0/firebase.js"></script>
    <script>
      // Initialize Firebase
      var config = {
        apiKey: "AIzaSyCrqVNiWibHCgdkyUYdOo8MdCu_ZsvrMnM",
        authDomain: "cms-stater.firebaseapp.com",
        databaseURL: "https://cms-stater.firebaseio.com",
        storageBucket: "",
      };
      firebase.initializeApp(config);
    </script>

</body></html>