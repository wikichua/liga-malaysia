<div class="panel-container">
	<div class="panel-heading">
		<div class="col-md-12">
		    <div class="page-title" style="width: 100%;">
		        <h1 style="display: inline-block;">Chart</h1>
		        <div class="pull-right well well-sm">
		            {!! Form::open(array(route('dashboard') ,'method'=>'get','name'=>'theform','files'=>true,'class'=>'form-inline','style'=>'display:inline-block;')) !!}
		            <div class="form-group">
		                {!! Form::label('ga_from','From') !!}
		                {!! Form::text('ga_from',$GA_CHART_START_DATE,array('class'=>'form-control datepicker')) !!}
		            </div>
		            <div class="form-group">
		                {!! Form::label('ga_to','To') !!}
		                {!! Form::text('ga_to',$GA_CHART_END_DATE,array('class'=>'form-control datepicker')) !!}
		            </div>
		            <button type="submit" class="btn btn-default"><i class="fa fa-search-plus"></i></button>
		            {!! Form::close() !!}
		            <div class="btn-group">
		                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		                Charts <span class="caret"></span>
		                </button>
		                <ul class="dropdown-menu">
		                <li><a href="#">Chart1</a></li>
		                {{-- <li><a href="{{  route('home.chartjs',$CustomerIdLists->first()->id) }}">Chart2</a></li> --}}
		                </ul>
		            </div>
		        </div>
		    </div>
		</div>


		<div class="panel-heading-left"><h4>Access Rate</h4></div>
	</div>
	<div class="panel-body">
		<canvas id="session"></canvas>
	</div>
{{-- 	<div class=" col-md-8 panel-body">
		<div class="panel-heading-left"><h4>Access Rate</h4></div>
		<canvas id="session1"></canvas>
	</div> --}}
{{-- 	<div class=" col-md-6 panel-body">
		<div class="panel-heading-left"><h4>Access Rate</h4></div>
		<canvas id="session2"></canvas>
	</div> --}}
</div>

@section('scripts')
<script>
$(function(){

	var sessionCount = [];
	var chartData = {
	    labels: [],
	    datasets: [
	        {
	            label: "Page View",
	            fill: true,
	            lineTension: 0.1,
	            backgroundColor: "rgba(75,192,192,0.1)",
	            borderColor: "rgba(75,192,192,1)",
	            borderCapStyle: 'butt',
	            borderDash: [],
	            borderDashOffset: 0.0,
	            borderJoinStyle: 'miter',
	            pointBorderColor: "rgba(75,192,192,1)",
	            pointBackgroundColor: "#fff",
	            pointBorderWidth: 1,
	            pointHoverRadius: 5,
	            pointHoverBackgroundColor: "rgba(75,192,192,1)",
	            pointHoverBorderColor: "rgba(220,220,220,1)",
	            pointHoverBorderWidth: 2,
	            pointRadius: 1,
	            pointHitRadius: 10,
	            data: sessionCount,
	        }
	    ]
	};

	var chartData1 = {
	    labels: [],
	    datasets: [
	        {
	            label: "Access Rate",
	            fill: true,
	            lineTension: 0.1,
	            backgroundColor: "rgba(75,192,192,0.1)",
	            borderColor: "rgba(75,192,192,1)",
	            borderCapStyle: 'butt',
	            borderDash: [],
	            borderDashOffset: 0.0,
	            borderJoinStyle: 'miter',
	            pointBorderColor: "rgba(75,192,192,1)",
	            pointBackgroundColor: "#fff",
	            pointBorderWidth: 1,
	            pointHoverRadius: 5,
	            pointHoverBackgroundColor: "rgba(75,192,192,1)",
	            pointHoverBorderColor: "rgba(220,220,220,1)",
	            pointHoverBorderWidth: 2,
	            pointRadius: 1,
	            pointHitRadius: 10,
	            data: sessionCount,
	        }
	    ]
	};
	@foreach( json_decode($pv['sessions']) as $sessions )
	chartData.labels.push('{{ $sessions->date }}'); 
	sessionCount.push({{ $sessions->visitors }}); 

	chartData1.labels.push('{{ $sessions->date }}'); 
	sessionCount.push({{ $sessions->visitors }}); 
	@endforeach
	

	ctx = $("#session");
	var myLineChart = new Chart(ctx, {
	    type: 'line',
	    data: chartData,
	    options: {
	        responsive: true
	    }
	});

	ctx1 = $("#session1");
	var myLineChart = new Chart(ctx1, {
	    type: 'pie',
	    data: chartData1,
	    options: {
	        responsive: true
	    }
	});

	ctx2 = $("#session2");
	var myLineChart = new Chart(ctx2, {
	    type: 'line',
	    data: chartData,
	    options: {
	        responsive: true
	    }
	});

});
</script>
@stop