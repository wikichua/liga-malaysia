@extends('template')

@section('title')
	Score
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Update Score (Match {{ $Match->status }})</h1>
	    </div>

        <div class="row x_content">
            {!! Form::open(array('route' =>array('standing.update',$match_id) ,'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}

			<fieldset>
				<legend>TEAM HOME ({{ $Match->ProfileTeamHome->name }})</legend>
				@for ($i = 1; $i <= 3; $i++)
					<div class="row">
						<div class="form-group col-sm-6">
							{!! Form::label('profile_player_id_'.$i, 'Player '.$i, array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::select('profile_player_id_'.$i,array(''=>'None') + $TeamHome,'', array('class'=>"form-control select2")) !!}
							</div>
						</div>
						<div class="form-group col-sm-2">
							{!! Form::label('action_'.$i, 'Action', array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::select('action_'.$i,config('fb.score_action') ,'', array('class'=>"form-control select2")) !!}
							</div>
						</div>
						<div class="form-group col-sm-2">
							{!! Form::label('time_'.$i, 'Minutes', array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::select('time_'.$i,config('fb.score_minutes') ,'', array('class'=>"form-control select2")) !!}
							</div>
						</div>
						<div class="form-group col-sm-2">
							{!! Form::label('remark_'.$i, 'Remark', array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::textarea('remark_'.$i,'', array('class'=>"form-control",'rows'=>1)) !!}
							</div>
						</div>
						{!! Form::hidden('profile_team_id_'.$i, $Match->team_home_id) !!}
						{!! Form::hidden('match_position_'.$i, 'HOME') !!}
					</div>
				@endfor
			</fieldset>

			<fieldset>
				<legend>TEAM AWAY ({{ $Match->ProfileTeamAway->name }})</legend>
				@for ($i = 4; $i <= 6; $i++)
					<div class="row">
						<div class="form-group col-sm-6">
							{!! Form::label('profile_player_id_'.$i, 'Player '.$i, array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::select('profile_player_id_'.$i,array(''=>'None') + $TeamAway,'', array('class'=>"form-control select2")) !!}
							</div>
						</div>
						<div class="form-group col-sm-2">
							{!! Form::label('action_'.$i, 'Action', array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::select('action_'.$i,config('fb.score_action') ,'', array('class'=>"form-control select2")) !!}
							</div>
						</div>
						<div class="form-group col-sm-2">
							{!! Form::label('time_'.$i, 'Minutes', array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::select('time_'.$i,config('fb.score_minutes') ,'', array('class'=>"form-control select2")) !!}
							</div>
						</div>
						<div class="form-group col-sm-2">
							{!! Form::label('remark_'.$i, 'Remark', array('class'=>'col-sm-2 control-label')) !!}
							<div class="col-sm-12">
								{!! Form::textarea('remark_'.$i,'', array('class'=>"form-control",'rows'=>1)) !!}
							</div>
						</div>
						{!! Form::hidden('profile_team_id_'.$i, $Match->team_away_id) !!}
						{!! Form::hidden('match_position_'.$i, 'AWAY') !!}
					</div>
				@endfor
			</fieldset>

			<div class="form-group">
				<div class="text-center">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					@if ($Match->state == 'OnGoing')
					<a class="btn btn-warning end_match" data-href="{{ route('standing.end',array($Match->id)) }}" data-toggle='tooltip' title='End this match'>End Match</a>
					@endif
					<a href="{{ route('match','s-id='.$match_id) }}" class='btn btn-danger'>Back</a>
					<a href="{{ route('standing.penalty_edit',array($match_id)) }}" class='btn btn-warning'>Penalty</a>
				</div>
			</div>
			{!! Form::close() !!}

			<fieldset>
				<legend>Match Details</legend>
					<table class="table table-striped table-responsive table-hover">
						<tr>
							<th class="col-sm-2">Picture</th>
							<th>{!! sortTableHeaderSnippet('Name','player-name') !!}</th>
							<th>{!! sortTableHeaderSnippet('Team','team-name') !!}</th>
							<th>{!! sortTableHeaderSnippet('Action','action') !!}</th>
							<th>{!! sortTableHeaderSnippet('Minutes','time') !!}</th>
							<th>{!! sortTableHeaderSnippet('Remark','remark') !!}</th>
							<th class="col-sm-1">Action</th>
						</tr>
						{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
						<tr>
							<th></th>
							<th>{!! searchTableHeaderSnippet('player-name') !!}</th>
							<th>{!! searchTableHeaderSnippet('team-name') !!}</th>
							<th>{!! searchTableHeaderSnippet('action','select',config('fb.score_action')) !!}</th>
							<th>{!! searchTableHeaderSnippet('time','select',config('fb.score_minutes')) !!}</th>
							<th>{!! searchTableHeaderSnippet('remark') !!}</th>
							<th>{!! search_reset_buttons() !!}</th>
						</tr>
						{!! Form::close() !!}
						@foreach ($Standings as $Standing)
						<tr>
							<td>
								<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($Standing->player->profile_picture,'profile')) }}" class="img-thumbnail previewImg col-sm-12">
							</td>
							<td>{{ $Standing->player->name }}</td>
							<td>{{ $Standing->team->name }}</td>
							<td>{{ config('fb.score_action.'.$Standing->action) }}</td>
							<td>{{ $Standing->time }}</td>
							<td>{{ $Standing->remark }}</td>
							<td class="actionButtons">
								<div class="btn-group btn-group-xs">
									<a class="btn btn-danger delete" data-href="{{ route('standing.destroy',array($Standing->match_id,$Standing->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
								</div>
							</td>
						</tr>
						@endforeach
					</table>
					<div class="text-center">
					{!! str_replace('/?', '?', $Standings->appends(request()->all())->render()) !!}
					</div>
			</fieldset>
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$(document).on('click','.end_match',function(event) {
        event.preventDefault();
        var $self = $(this);
        alertify.confirm('Are you sure to end this match?').set('onok', function(closeEvent){
            if($self.attr('href') != '#')
            {
                $.ajax({
                    url: $self.data('href'),
                    type: 'PUT',
                    success: function(result){
                        window.location.href = result;
                    }
                });

            }
        } );
    });
});
</script>
@stop