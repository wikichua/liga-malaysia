@extends('template')

@section('title')
	Score
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Update Penalty Score (Match {{ $Match->status }})</h1>
	    </div>

        <div class="row x_content">
            {!! Form::open(array('route' =>array('standing.penalty_update',$match_id) ,'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}

			<fieldset>
			<hr>
					<div class="row">
						<div class="form-group col-sm-12">
							{!! Form::label('team_home_penalty', 'TEAM HOME ('. $Match->ProfileTeamHome->name.')', array('class'=>'col-sm-5 control-label')) !!}
							<div class="col-sm-3">
								{!! Form::text('team_home_penalty',$Match->team_home_penalty, array('class'=>"form-control ")) !!}
							</div>
						</div>
						<div class="form-group col-sm-12">
							{!! Form::label('team_away_penalty', 'TEAM AWAY ('. $Match->ProfileTeamAway->name.')', array('class'=>'col-sm-5 control-label')) !!}
							<div class="col-sm-3">
								{!! Form::text('team_away_penalty',$Match->team_away_penalty, array('class'=>"form-control ")) !!}
							</div>
						</div>


					</div>
			</fieldset>

			
			<div class="form-group">
				<div class="text-center">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					@if ($Match->state == 'OnGoing')
					<a class="btn btn-warning end_match" data-href="{{ route('standing.end',array($Match->id)) }}" data-toggle='tooltip' title='End this match'>End Match</a>
					@endif
					<a href="{{ route('match','s-id='.$match_id) }}" class='btn btn-danger'>Back</a>
				</div>
			</div>
			{!! Form::close() !!}

@stop

@section('scripts')
<script>
$(function(){
	$(document).on('click','.end_match',function(event) {
        event.preventDefault();
        var $self = $(this);
        alertify.confirm('Are you sure to end this match?').set('onok', function(closeEvent){
            if($self.attr('href') != '#')
            {
                $.ajax({
                    url: $self.data('href'),
                    type: 'PUT',
                    success: function(result){
                        window.location.href = result;
                    }
                });

            }
        } );
    });
});
</script>
@stop