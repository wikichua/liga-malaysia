@extends('template')

@section('title')
	Team
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Edit Team</h1>
	    </div>

                <div class="row x_content">
            {!! Form::open(array('route' => array('profile_team.update',$ProfileTeam->id),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('logo', 'Logo', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($ProfileTeam->logo,'default')) }}" class="img-thumbnail previewImg col-sm-6">
					{!! Form::file('logo', array('class'=>"form-control")) !!}
					<p class="text-danger">Image file allow: JPEG, JPG, PNG</p>
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('tournaments', 'Tournaments', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::select('tournaments[]',config('fb.tournament'), old('tournaments',$ProfileTeam->tournaments), array('class'=>"form-control select2",'multiple'=>'multiple')) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Name', array('class'=>'col-sm-3 control-label required')) !!}
				<div class="col-sm-7">
					{!! Form::text('name', $ProfileTeam->name, array('class'=>"form-control",'placeholder'=>"Name")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('nickname', 'Nickname', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('nickname',$ProfileTeam->nickname, array('class'=>"form-control",'placeholder'=>"Nickname")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('head_coach', 'Head Coach', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('head_coach',$ProfileTeam->head_coach, array('class'=>"form-control",'placeholder'=>"Head Coach")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('home_ground', 'Home ground', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('home_ground',$ProfileTeam->home_ground, array('class'=>"form-control",'placeholder'=>"Home ground")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('website_url', 'Website URL', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('website_url',$ProfileTeam->website_url, array('class'=>"form-control",'placeholder'=>"Website URL")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('fb_url', 'Facebook URL', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('fb_url',$ProfileTeam->fb_url, array('class'=>"form-control",'placeholder'=>"Facebook URL")) !!}
				</div>
			</div>			<div class="form-group">
				{!! Form::label('twitter_url', 'Twitter URL', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('twitter_url',$ProfileTeam->twitter_url, array('class'=>"form-control",'placeholder'=>"Twitter URL")) !!}
				</div>
			</div>			<div class="form-group">
				{!! Form::label('instagram_url', 'Instagram URL', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('instagram_url',$ProfileTeam->instagram_url, array('class'=>"form-control",'placeholder'=>"Instagram URL")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('profile_team') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#logo").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop