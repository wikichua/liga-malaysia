@extends('template')

@section('title')
	Team
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-archive'></i> Team</h1>
	        @if (ACLButtonCheck('TEAMS_MGMT','Create'))
	       	<span class="pull-right">{!! action_add_button(route('profile_team.create')) !!}</span>
	        @endif
	    </div>

        <div class="row x_content">
        	<table class="table table-striped table-responsive table-hover">
				<tr>
					<th class="col-sm-2">Logo</th>
					<th>{!! sortTableHeaderSnippet('Name','name') !!}</th>
					<th>{!! sortTableHeaderSnippet('Nickname','nickname') !!}</th>
					<th class="col-sm-1">Action</th>
				</tr>
				{!! Form::open(array('url' => Request::url(), 'method' => 'get')) !!}
				<tr>
					<th></th>
					<th>{!! searchTableHeaderSnippet('name') !!}</th>
					<th>{!! searchTableHeaderSnippet('nickname') !!}</th>
					<th>{!! search_reset_buttons() !!}</th>
				</tr>
				{!! Form::close() !!}
				@foreach ($ProfileTeams as $ProfileTeam)
				<tr>
					<td>
						<img id="previewHere" src="{{ asset('uploads/'.imgTagShow($ProfileTeam->logo,'default')) }}" class="img-thumbnail previewImg col-sm-12">
					</td>
					<td>{{ $ProfileTeam->name }}</td>
					<td>{{ $ProfileTeam->nickname }}</td>
					<td class="actionButtons">
						<div class="btn-group btn-group-xs">


							@if (ACLButtonCheck('HNRS_MGMT','Read'))
							<a class="btn btn-default dropdown-toggle" href="" data-toggle='dropdown' title=''><span class="caret"></span></a>
								<ul class="dropdown-menu dropdown-menu-right">
		      						@if (ACLButtonCheck('PLYERS_MGMT','Read'))
								    	<li><a href="{{ route('profile_players',array($ProfileTeam->id)) }}">Player</a></li>
								    @endif
		      						@if (ACLButtonCheck('TM_GLY_MGMT','Read'))
								    	<li><a href="{{ route('team_galleries',array($ProfileTeam->id)) }}">Gallery</a></li>
								    @endif
									@if (ACLButtonCheck('HNRS_MGMT','Read'))
		      							<li><a href="{{ route('honour',array($ProfileTeam->id)) }}">Honour</a></li>
		      						@endif
							    </ul>	
							@endif
							@if (ACLButtonCheck('TEAMS_MGMT','Update'))
							<a class="btn btn-warning" href="{{ route('profile_team.edit',array($ProfileTeam->id)) }}" data-toggle='tooltip' title='Edit'><i class="fa fa-pencil"></i></a>
							@endif
							@if (ACLButtonCheck('TEAMS_MGMT','Delete'))
							<a class="btn btn-danger delete" data-href="{{ route('profile_team.destroy',array($ProfileTeam->id)) }}" data-toggle='tooltip' title='Delete'><i class="fa fa-trash-o"></i></a>
							@endif
						</div>
					</td>
				</tr>
				@endforeach
				</table>
				<div class="text-center">
				{!! str_replace('/?', '?', $ProfileTeams->appends(request()->all())->render()) !!}
				</div>	
        </div>
    </div>
</div>
@stop

@section('scripts')
<script type="text/javascript">
$(document).ready(function(){
});
</script>
@stop