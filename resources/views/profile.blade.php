@extends('template')

@section('title')
	My Profile
@stop

@section('body')
<div class="row">
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="row x_panel">
	        <h1 class="x_title"><i class='fa fa-user'></i> My Profile</h1>
	    </div>

        <div class="row x_content">
            {!! Form::open(array('route' => array('profile.update'),'name' => 'register_form','class' => 'form-horizontal', 'method' => 'put','files'=>true)) !!}
			<div class="form-group">
				{!! Form::label('photo', 'Photo', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					<img id="previewHere" src="{{ asset('uploads/'.imgTagShow(auth()->user()->photo,'profile')) }}" class="img-thumbnail previewImg">
					{!! Form::file('photo', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('name', 'Full Name', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('name', old('name',auth()->user()->name), array('class'=>"form-control",'placeholder'=>"Full Name")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('email', 'Email', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::text('email', old('email',auth()->user()->email), array('class'=>"form-control",'placeholder'=>"Email")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('password', 'Password', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::password('password', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				{!! Form::label('password_confirmation', 'Confirm Password', array('class'=>'col-sm-3 control-label')) !!}
				<div class="col-sm-7">
					{!! Form::password('password_confirmation', array('class'=>"form-control")) !!}
				</div>
			</div>
			<div class="form-group">
				<div class="col-sm-offset-3 col-sm-7">
					{!! Form::submit('Submit', array('class'=>"btn btn-primary")) !!}
					<a href="{{ route('dashboard') }}" class='btn btn-danger'>Cancel</a>
				</div>
			</div>
			{!! Form::close() !!}
        </div>
    </div>
</div>
@stop

@section('scripts')
<script>
$(function(){
	$("#photo").change(function(){
	    previewImg(this,'previewHere');
	});
});
</script>
@stop