<!doctype html>
<html><head>
    <meta charset="utf-8">
    <title>{{ config('cms.brand','CMS Starter Convep Mobilogy') }} @yield('title')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Convep Mobilogy Wiki Chua">

    <!-- Le styles -->
    <link href="{{ asset('assets/css/bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/font-style.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/alertify.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/alertifyjs/css/themes/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/css/main.css') }}" rel="stylesheet">

    <link href="{{ asset('assets/css/login1.css') }}" rel="stylesheet">

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

  </head>

  <body>



<div class="login" style="padding-top: 10em">
  <div class="login-header">
    <h1>Forgot your password?</h1>
    <hr>
    <h6>In order to receive your temporary password by email, please enter the email address used for your account.</h6>
  </div>
  <div class="login-form">


        {!! Form::open(array('route' => array('send_email'),'name' => 'form','class' => 'form-horizontal', 'method' => 'put','files'=>false)) !!}
                        <div class="form-group">
                        <h4>Email:</h4>
                            {!! Form::text('email', old('email'), array('class' => 'input_text  input-lg','placeholder' => 'someone@email.com')) !!}
                        </div>
                       
                        <div class="form-group">
                            <button type="submit" class="input_button login-button btn btn-success btn-lg submit" name='login'> submit</button>
                            <a href={{ route('auth') }} class="input_button login-button btn btn-danger btn-lg submit" > back</a>
                            
                        </div>  
                    {!! Form::close() !!}

  </div>
</div>


    <script src="js/index.js"></script>
    <script type="text/javascript" src="http://code.jquery.com/jquery-latest.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/bootstrap.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/alertifyjs/alertify.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/jquery.flexslider.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/cms.js') }}"></script>

    @include('alert')

    
  
</body></html>