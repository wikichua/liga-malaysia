<?php

return array(
	'brand' => 'Liga Malaysia App',

	'modules'       => array(
		'ADT_MGMT'     => 'Audit Trails',
		'USR_MGMT'     => 'Users Management',
		'USR_GRP_MGMT' => 'User Groups Management',
		'USR_ACL'      => 'Access Control List',
		'APPVSN_MGMT'  => 'App Versioning',

		'DASHBOARD'    => 'Dashboard',
		'NEWS_MGMT'    => 'News Management',
		'VIDS_MGMT'    => 'Videos Management',
		'ADV_MGMT'     => 'Adverts Management',
		'TEAMS_MGMT'   => 'Teams Management',
		'PLYERS_MGMT'  => 'Players Management',
		'HNRS_MGMT'    => 'Honours Management',
		'TM_GLY_MGMT'  => 'Teams Gallery Management',
		'GRPSTGS_MGMT' => 'Group Stages (Round Robins) Management',
		'MTCHS_MGMT'   => 'Matches Management',
		'SCORING'      => 'Scoring Management',
		'PIM_MGMT'     => 'Player In Match Management',
	),

	'ACL_USER'      => array(
		'ADT_MGMT'     => array('Read'),
		'USR_MGMT'     => array('Create', 'Read', 'Update', 'Delete'),
		'USR_GRP_MGMT' => array('Create', 'Read', 'Update', 'Delete'),
		'USR_ACL'      => array('Create', 'Read'),
		'APPVSN_MGMT'  => array('Create', 'Read', 'Update', 'Delete'),

		'DASHBOARD'    => array('Read'),
		'NEWS_MGMT'    => array('Create', 'Read', 'Update', 'Delete'),
		'VIDS_MGMT'    => array('Create', 'Read', 'Update', 'Delete'),
		'ADV_MGMT'     => array('Create', 'Read', 'Update', 'Delete'),
		'TEAMS_MGMT'   => array('Create', 'Read', 'Update', 'Delete'),
		'PLYERS_MGMT'  => array('Create', 'Read', 'Update', 'Delete'),
		'HNRS_MGMT'    => array('Create', 'Read', 'Update', 'Delete'),
		'TM_GLY_MGMT'  => array('Create', 'Read', 'Update', 'Delete'),
		'GRPSTGS_MGMT' => array('Create', 'Read', 'Update', 'Delete'),
		'MTCHS_MGMT'   => array('Create', 'Read', 'Update', 'Delete', 'Postpone'),
		'SCORING'      => array('Manage'),
		'PIM_MGMT'     => array('Manage'),
	),
);