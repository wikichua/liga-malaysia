<?php

return array(

	'tournament' => array(
		'Cup'       => array(
			'MYC'      => 'Malaysia Cup',
			'FAC'      => 'FA Cup',
		),
		'League' => array(
			'SPL'   => 'Super League',
			'PML'   => 'Premier League',
		),
	),
	'position' => array(
		'FW'      => 'Forward',
		'MID'     => 'Midfielder',
		'DEF'     => 'Defender',
		'GK'      => 'Goalkeeper',
		'SB'      => 'Standby',
	),
	'state'    => array(
		'Fixture' => 'Fixture',
		'OnGoing' => 'On Going',
		'Result'  => 'Result',
	),
	'round' => array_combine(
		array_merge(array(
				'01'                      => '01',
				'02'                      => '02',
				'03'                      => '03',
				'04'                      => '04',
				'05'                      => '05',
				'06'                      => '06',
				'07'                      => '07',
				'08'                      => '08',
				'09'                      => '09',
				'10'                      => '10',
				'11'                      => '11',
				'12'                      => '12',
				'13'                      => '13',
				'14'                      => '14',
				'15'                      => '15',
				'16'                      => '16',
				'17'                      => '17',
				'18'                      => '18',
				'19'                      => '19',
				'20'                      => '20',
				'21'                      => '21',
				'22'                      => '22',
				'23'                      => '23',
				'24'                      => '24',
				'25'                      => '25',
				'26'                      => '26',
				'27'                      => '27',
				'28'                      => '28',
				'29'                      => '29',
				'30'                      => '30',
				'31'                      => '31',
				'32'                      => '32',
			), array('Quarter-Final-1' => 'Quarter-Final-1', 'Quarter-Final-2' => 'Quarter-Final-2', 'Semi-Final-1' => 'Semi-Final-1', 'Semi-Final-2' => 'Semi-Final-2', 'Final' => 'Final')),
		array_merge(array(
				'01'                      => '01',
				'02'                      => '02',
				'03'                      => '03',
				'04'                      => '04',
				'05'                      => '05',
				'06'                      => '06',
				'07'                      => '07',
				'08'                      => '08',
				'09'                      => '09',
				'10'                      => '10',
				'11'                      => '11',
				'12'                      => '12',
				'13'                      => '13',
				'14'                      => '14',
				'15'                      => '15',
				'16'                      => '16',
				'17'                      => '17',
				'18'                      => '18',
				'19'                      => '19',
				'20'                      => '20',
				'21'                      => '21',
				'22'                      => '22',
				'23'                      => '23',
				'24'                      => '24',
				'25'                      => '25',
				'26'                      => '26',
				'27'                      => '27',
				'28'                      => '28',
				'29'                      => '29',
				'30'                      => '30',
				'31'                      => '31',
				'32'                      => '32',
			), array('Quarter-Final-1' => 'Quarter-Final-1', 'Quarter-Final-2' => 'Quarter-Final-2', 'Semi-Final-1' => 'Semi-Final-1', 'Semi-Final-2' => 'Semi-Final-2', 'Final' => 'Final'))),

	'advert'      => array(
		'status'     => array(
			'Draft'     => 'Draft',
			'Published' => 'Published',
			'Expired'   => 'Expired',
		)
	),

	'year' => array_combine(range(2016, date('Y')+13), range(2016, date('Y')+13)),

	'honours' => array(
		'FD'     => 'First Division',
		'SD'     => 'Second Division',
		'CS'     => 'Charity Shield',
		'MYC'    => 'Malaysia Cup',
		'FAC'    => 'FA Cup',
	),
	'honour_year' => array_combine(range(1920, date('Y')+13), range(1920, date('Y')+13)),

	'score_minutes' => array_combine(
		array_merge(array('00', '01', '02', '03', '04', '05', '06', '07', '08', '09', ), range(10, 120)),
		array_merge(array('00', '01', '02', '03', '04', '05', '06', '07', '08', '09', ), range(10, 120))
	),

	'score_action' => array(
		'GL'          => 'Goal',
		'YC'          => 'Yellow Card',
		'RC'          => 'Red Card',
		'OG'          => 'Own Goal',
	),
	'score_action_penalty' => array(
		'PNT'                 => 'Penalty',
	),

	'countries' => array(
		"Afghanistan", "Albania", "Algeria", "American Samoa", "Andorra", "Angola", "Anguilla", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Aruba", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegowina", "Botswana", "Bouvet Island", "Brazil", "British Indian Ocean Territory", "Brunei Darussalam", "Bulgaria", "Burkina Faso", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Cayman Islands", "Central African Republic", "Chad", "Chile", "China", "Christmas Island", "Cocos (Keeling) Islands", "Colombia", "Comoros", "Congo", "Congo, the Democratic Republic of the", "Cook Islands", "Costa Rica", "Cote d'Ivoire", "Croatia (Hrvatska)", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Falkland Islands (Malvinas)", "Faroe Islands", "Fiji", "Finland", "France", "France Metropolitan", "French Guiana", "French Polynesia", "French Southern Territories", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Gibraltar", "Greece", "Greenland", "Grenada", "Guadeloupe", "Guam", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Heard and Mc Donald Islands", "Holy See (Vatican City State)", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran (Islamic Republic of)", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, Democratic People's Republic of", "Korea, Republic of", "Kuwait", "Kyrgyzstan", "Lao, People's Democratic Republic", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libyan Arab Jamahiriya", "Liechtenstein", "Lithuania", "Luxembourg", "Macau", "Macedonia, The Former Yugoslav Republic of", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Martinique", "Mauritania", "Mauritius", "Mayotte", "Mexico", "Micronesia, Federated States of", "Moldova, Republic of", "Monaco", "Mongolia", "Montserrat", "Morocco", "Mozambique", "Myanmar", "Namibia", "Nauru", "Nepal", "Netherlands", "Netherlands Antilles", "New Caledonia", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Niue", "Norfolk Island", "Northern Mariana Islands", "Norway", "Oman", "Pakistan", "Palau", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Pitcairn", "Poland", "Portugal", "Puerto Rico", "Qatar", "Reunion", "Romania", "Russian Federation", "Rwanda", "Saint Kitts and Nevis", "Saint Lucia", "Saint Vincent and the Grenadines", "Samoa", "San Marino", "Sao Tome and Principe", "Saudi Arabia", "Senegal", "Seychelles", "Sierra Leone", "Singapore", "Slovakia (Slovak Republic)", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "South Georgia and the South Sandwich Islands", "Spain", "Sri Lanka", "St. Helena", "St. Pierre and Miquelon", "Sudan", "Suriname", "Svalbard and Jan Mayen Islands", "Swaziland", "Sweden", "Switzerland", "Syrian Arab Republic", "Taiwan, Province of China", "Tajikistan", "Tanzania, United Republic of", "Thailand", "Togo", "Tokelau", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Turks and Caicos Islands", "Tuvalu", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "United States Minor Outlying Islands", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Virgin Islands (British)", "Virgin Islands (U.S.)", "Wallis and Futuna Islands", "Western Sahara", "Yemen", "Yugoslavia", "Zambia", "Zimbabwe",
	),

	'postpone_message_placeholder' => array(
		'<<TEAMHOME>>',
		'<<TEAMAWAY>>',
		'<<STARTDATE>>',
		'<<LOCATION>>',
	),

);