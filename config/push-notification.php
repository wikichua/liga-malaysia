<?php

return array(
	'appNameFirebase' => array(
		'environment'    => env('PUSH_ENV'),
		'apiKey'         => env('API_KEY'),
		'service'        => 'fcm',
	)

);